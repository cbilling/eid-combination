import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
from .tools.rootToArray import *
from .tools.plotting import *
import os
import numpy as np
from .dataclass import MethodSF

working_dir = os.environ["WORKING_DIR"]
data_dir = working_dir + "/data"


def combineSysLabels(method1, method2):
    # we need to end up with the same labels
    # compare index by index, if they are the same, do nothing
    # if one is zero, take the other, if they don't agree and are non-zero,
    # make a new label

    systlabel1 = method1.syst_labels
    systlabel2 = method2.syst_labels
    systToIter = systlabel1
    if len(systlabel2) > len(systlabel1):
        systToIter = systlabel2

    combinedLabel = []
    for i in range(len(systToIter)):
        if systlabel1[i] == systlabel2[i]:
            combinedLabel.append(systlabel1[i])
        elif systlabel1[i] == "0":
            combinedLabel.append(systlabel2[i])
        elif systlabel2[i] == "0":
            combinedLabel.append(systlabel1[i])
        else:
            combinedLabel.append(systlabel1[i] + systlabel2[i])
    return combinedLabel


def combineZmethods(method1, method2):
    SF_meth1, stat_meth1, syst_meth1 = method1.sf, method1.stat, method1.syst
    SF_meth2, stat_meth2, syst_meth2 = method2.sf, method2.stat, method2.syst
    # print("input: ")
    # print(SF_meth1)
    # print(SF_meth2)
    # the full blown chi2
    SF_all = np.array((SF_meth1, SF_meth2))  # it's a 2-d array
    # FROM NOW ON WE DECORRELATE THE INPUT WITH A DEDICATED SCRIPT
    # syst_meth1, syst_meth2 = decorrelate( syst_meth1, syst_meth2, list(range(220+2)) ) # in practise we want to keep columns 0 and 1 correlated, so there's a exception for columns [0,1]

    stat_all = np.array((stat_meth1, stat_meth2))
    syst_all = np.array((syst_meth1, syst_meth2))

    def getAverage(SFs, stats, systs):
        SFav = np.zeros(SFs.shape[1])
        statav = np.zeros(SFs.shape[1])
        systav = np.zeros(systs.shape[1:])

        for ientry in range(0, SFs.shape[1], 1):
            SFpair = SFs[:, ientry : ientry + 1]
            statpair = stats[:, ientry : ientry + 1]
            systpair = systs[:, ientry : ientry + 1, :]

            if SFpair[0] == 0 or SFpair[1] == 0:
                updn = np.array([SFpair[0], SFpair[1]])
                index = np.where(updn == np.amax(updn))[0][0]

                SFav[ientry] = SFpair[index]
                statav[ientry] = statpair[index]
                systav[ientry] = systpair[index]
                continue

            cov = get_covariance(
                np.concatenate(
                    (
                        np.column_stack(
                            (
                                statpair[0, :].reshape((1, 1)),
                                np.zeros(statpair[0, :].reshape((1, 1)).shape),
                                np.reshape(systpair[0, :, :], (1, systpair.shape[2])),
                            )
                        ),
                        np.column_stack(
                            (
                                np.zeros(statpair[1, :].reshape((1, 1)).shape),
                                statpair[1, :].reshape((1, 1)),
                                np.reshape(systpair[1, :, :], (1, systpair.shape[2])),
                            )
                        ),
                    ),
                    axis=0,
                )
            )
            invcov = np.linalg.pinv(cov, hermitian=True)

            # ------------------------------------
            # normal weighted average
            #        SFmean = np.sum( invcov.dot(SFpair) ) / np.sum( invcov )
            #        statmean = np.sum( invcov.dot(statpair) ) / np.sum( invcov )

            # BLUE method which yields identical results but has an easier to understand notation
            l0 = np.sum(invcov[:, 0]) / np.sum(invcov)
            l1 = np.sum(invcov[:, 1]) / np.sum(invcov)
            weights = np.array([l0, l1])

            SFmean = weights.dot(SFpair)
            statmean = weights.dot(statpair)
            # ------------------------------------

            SFav[ientry : ientry + 1] = SFmean
            statav[ientry : ientry + 1] = statmean

            for ivar in range(0, systpair.shape[2]):
                if not systpair[:, :, ivar].any():
                    continue

                # ------------------------------------
                # weighted average
                systav[ientry : ientry + 1, ivar] = np.sum(
                    invcov.dot(systpair[:, :, ivar])
                ) / np.sum(invcov)

                # BLUE method, doesn't preserve the sign (!!!)
                #            cov_ivar  = get_covariance( np.concatenate( (
                #                np.column_stack( ( np.reshape(systpair[0,:,ivar],(1,1)) ) ),
                #                np.column_stack( ( np.reshape(systpair[1,:,ivar],(1,1)) ) )), axis=0) )
                #            systav[ientry:ientry+1,ivar] = np.sqrt(weights.dot(cov_ivar).dot(weights.T))
                # ------------------------------------

        return SFav, statav, systav

    average, averstat, aversyst = getAverage(
        SF_all, stat_all, syst_all
    )  # len(x_meth1))
    averstat = np.sqrt(averstat**2)

    # ensure_directory( outdir )
    # writeArray_SF_stat_syst( outdir, average, averstat, aversyst, x_meth1, y_meth1 )

    chi2 = get_chi2(average, 0, SF_all, stat_all, syst_all)
    ndof = len(SF_meth1)
    # np.savetxt( outdir+'/chi2_philip.txt'       ,  np.array([chi2,ndof]), fmt="%1.6f" )
    # print '    chi2 (chi2 incl all correlations): ', chi2/ndof

    method = MethodSF(average, averstat, aversyst, method1.x_bins, method1.y_bins)

    method.syst_labels = combineSysLabels(method1, method2)
    return method


def combineHera(indir_meth1, indir_meth2, x, y, wp, year):
    ZcombPath = f"{data_dir}/{wp}/{year}/Zcomb_final_loPt_rebin"
    JpsiPath = f"{data_dir}/{wp}/{year}/Jpsi"
    ZJcombPath = f"{data_dir}/{wp}/{year}/ZJcomb"

    os.makedirs(ZcombPath, exist_ok=True)
    os.makedirs(JpsiPath, exist_ok=True)
    os.makedirs(ZJcombPath, exist_ok=True)

    # need to add the number of rows of zeros that jpsi has
    zero_row = np.zeros(
        (abs(indir_meth1.shape[0] - indir_meth2.shape[0]), indir_meth1.shape[1])
    )

    # Prepending the row of zeros to the existing array
    indir_meth1 = np.vstack((zero_row, indir_meth1))

    writeArray(ZcombPath, indir_meth1, x, y)
    writeArray(JpsiPath, indir_meth2, x, y)
    os.system(f"python/averagerpy27/combineHera.sh {ZcombPath} {JpsiPath} {ZJcombPath}")

    ZJcomb, x, y = readArray(ZJcombPath)

    return MethodSF(ZJcomb[:, 0], ZJcomb[:, 1], ZJcomb[:, 2:], x, y)
