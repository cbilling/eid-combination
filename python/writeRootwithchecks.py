import sys, operator
from .tools.arrayToEgamma import *
from .tools.statTools import *
import numpy as np
import ROOT
import matplotlib.pyplot as plt


def cov_to_cor(cov):
    sigma_inv = 1.0 / np.sqrt(np.diag(cov))
    sigmaij_inv = np.outer(sigma_inv, sigma_inv)
    cor = cov * sigmaij_inv
    return cor


def get_et(arr_unc, eta=8):

    return arr_unc[eta:200:20]


def is_diagonalizable(A):
    eigenvalues, eigenvectors = np.linalg.eigh(A)
    rank = np.linalg.matrix_rank(eigenvectors)
    if rank == A.shape[0]:
        return True
    else:
        return False


def diagonalize_matrix(A, DecreaseOrder=True, normalize=True, trim=True):
    if not is_diagonalizable(A):
        return "Matrix A is not diagonalizable"

    # Step 2: Calculate eigenvalues
    eigenvalues, P = np.linalg.eigh(A)
    if DecreaseOrder:
        eigenvalues = np.flip(eigenvalues)
        P = np.flip(P, axis=1)

    D = np.diag(eigenvalues)

    print(np.min(D), "smallest eigenvalues")
    if trim:
        print("trimming")
        tol = 5e-17
        D[abs(D) < tol] = tol
    if normalize:
        P = P @ np.diag(1.0 / np.sqrt(np.diag(D)))
    np.savetxt("P.txt", P)
    print("NANS", np.argwhere(np.isnan(P)))
    return [P, D]


def writeRootFiles_decomp(indirs=[], years=[], outfname=""):
    # the old doFifteen variable makes sure we always fill 15 corr systs, regardless of the size of the eigenvalues
    doFifteen = 15
    sflabel = "FullSim"
    # now the input
    # if "," not in sys.argv[-1] and "/" not in sys.argv[-1]:
    #    # then it should be the label for AF2 and FullSim
    #    sflabel = sys.argv[-1]
    #    del sys.argv[
    #        -1
    #    ]  # delete it right away so I don't have to rewrite the code below
    indirs_good = []
    years_good = []
    for i in range(len(indirs)):
        if type(indirs[i]["SF"]) != type(None):
            indirs_good.append(indirs[i])
            years_good.append(years[i])
    indirs = indirs_good
    years = years_good

    print(years)
    # read the values from the previous steps
    SF = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    # dictionaries with these keys: SF, x, y
    for indir in indirs:
        SF_meth, stat_meth, syst_meth, x_meth, y_meth = readArray_SF_stat_syst(
            indir["SF"], indir["x"], indir["y"]
        )
        print(SF_meth.shape, "SF meth shape")
        print(stat_meth.shape, "stat meth shape")
        print(syst_meth.shape, "syst meth shape")
        SF.append(SF_meth)
        stat.append(stat_meth)
        syst.append(syst_meth)
        xbin.append(x_meth)
        ybin.append(y_meth)

    # if years and len(years) != len(SF) :
    #    print ' in writeRootFiles.py: you didn\'t provide enough years '
    #    exit()
    print(1)
    nSys = syst[0].shape[1]
    for i in range(1, len(SF)):

        # for Jpsi we also need to pad the syst, so let's store nSys (for the length we don't care)
        nSys_i = syst[i].shape[1]
        nSys = max(nSys, nSys_i)

    # stack the systs together so we can decompose
    syst_all = np.zeros((syst[0].shape[0], nSys))
    syst_all[: syst[0].shape[0], : syst[0].shape[1]] = syst[0]
    print(syst_all.shape, "syst_all shape")
    for i in range(1, len(SF)):
        syst_i = np.zeros((syst[i].shape[0], nSys))
        syst_i[: syst[i].shape[0], : syst[i].shape[1]] = syst[i]
        syst_all = np.concatenate((syst_all, syst_i), axis=0)
        print(syst_all.shape, "syst_all shape")

    # zero_idx = np.argwhere(np.all(syst_all[..., :] == 0, axis=0))
    # syst_all = np.delete(syst_all, zero_idx, axis=1)
    # print("Columns to drop ", zero_idx)
    print(syst_all.shape, "syst_all shape after dropping zeros")
    print(2)

    # syst_all = np.array([-1.561946,1.087936,-0.417566,-2.547219,1.630727,-2.343305,-0.659660,0,-1.312506,-0.820935,1.210568,-1.432693,-1.529273,-2.273519,2.351643,2.391382])
    # get the covariance matrixsoju gang

    #########################
    syst_todecomp = syst_all.T
    corr = get_correlation(syst_todecomp)
    print(corr.shape, "corr shape")

    cov = get_covariance(syst_todecomp)
    A = cov.copy()
    print(cov.shape, "cov shape")

    # inner = (syst_todecomp.T @ A).T @ syst_todecomp.T
    # print(inner.shape)

    # unc_tot = np.sqrt(np.abs(np.sum(inner, axis=1)))
    # print(unc_tot)
    # sig_tot = unc_tot

    vec_eig, val_eig = diagonalize_matrix(cov, normalize=False)

    cov_norm = get_covariance(syst_todecomp, add_norm=True)
    vec_eig_norm, val_eig_norm = diagonalize_matrix(cov_norm, normalize=True, trim=True)

    # vec_eig = vec_eig @ (1/val_eig)
    # print("eigenvalues",val_eig.diagonal())
    # np.savetxt("eig.txt",val_eig.diagonal())
    # cov_eig = vec_eig @ np.diag(val_eig) @ np.linalg.inv(vec_eig)

    for i in range(len(vec_eig)):
        print("vec eig shape ", vec_eig.shape, "val_eig shape ", val_eig.shape)
        T = vec_eig @ np.sqrt(val_eig)
        cov_in_eig = T @ T.T
        print(cov.shape, "cov shape")
        corr_in_eig = cov_to_cor(cov_in_eig)
        fig, ax = plt.subplots(1, 2)
        ax[0].imshow(corr)

        ax[1].imshow(corr_in_eig)
        plt.savefig(f"np_red_plots/_statcorrcompare{i}.png", dpi=800)
        plt.close()

        # print(corr[0,:],corr_in_eig[0,:])
        diff_in_corr_space = np.divide(corr, corr_in_eig).flatten()
        fig, ax = plt.subplots()
        ax.hist(diff_in_corr_space, bins=500, range=(0.90, 1.1))
        plt.savefig(f"np_red_plots_stat/diff_in_corr_space{i}.png")
        plt.close()

        # already in kinematic space
        regular_unc = get_standarddev(syst_all)

        eig_unc = get_standarddev(vec_eig @ np.sqrt(val_eig))
        fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [4, 1]})
        ax[0].plot(regular_unc, "b")
        ax[0].plot(eig_unc, "r*")
        ax[1].plot(regular_unc / eig_unc)
        ax[1].set_ylim([0.9, 1.1])
        plt.savefig(f"np_red_plots_stat/compunc{i}.png")
        plt.close()

        vec_eig = vec_eig[:, : len(vec_eig) - 1 - i]
        vec_eig_norm = vec_eig_norm[:, : len(vec_eig_norm) - 1 - i]
        val_eig = np.diag(val_eig.diagonal()[: vec_eig.shape[1]])
        val_eig_norm = np.diag(val_eig_norm.diagonal()[: vec_eig_norm.shape[1]])

    return
    vec_eig, val_eig = diagonalize_matrix(cov, normalize=False)
    vec_eig_norm, val_eig_norm = diagonalize_matrix(cov, normalize=True, trim=True)

    fig, ax = plt.subplots()
    for i in range(6):
        ax.plot(get_et(syst_all[:, i]))
        ax.plot(get_et(vec_eig[:, i]))
    plt.savefig("plots/et_unc.png")
    plt.close()

    # cov_eig = vec_eig @ val_eig
    # print(cov_eig)
    # cov_eig = cov_eig  @ np.linalg.inv(vec_eig)
    # print(cov_eig)
    # print(cov_eig.shape,"cov shape")
    # print(np.allclose(cov,cov_eig))

    # get the covariance matrix
    # corr_eig = get_correlation(cov_eig,dejacov=True)
    # print(corr_eig.shape,"corr shape")
    # print(np.allclose(corr,corr_eig))

    # print(np.diag(val_eig).shape)
    # divisor = np.sqrt(np.diag(val_eig))
    # print(divisor.shape)
    # vec_eig = vec_eig * np.diag(1.0 / divisor)
    # print(vec_eig.shape)
    # inner = vec_eig @ sig_tot @ np.linalg.inv(vec_eig)
    # inner = ((np.linalg.inv(vec_eig).T @ syst_todecomp).T @ cov_eig).T @ (np.linalg.inv(vec_eig).T @ syst_todecomp).T
    print("syst_all shape:", syst_all.T.shape, "vec_eig shape:", vec_eig.shape)
    # inner = syst_all @ vec_eig
    # inner = np.sqrt(np.abs(np.sum(inner @ inner.T, axis=1)))
    # print("inner shape",inner.shape)
    # vec_eig = inner
    # vec_eig = vec_eig
    # unc_tot = np.sqrt(np.abs(np.sum(inner,axis = 1)))
    # print(unc_tot.shape)

    fig, ax = plt.subplots()
    im = ax.imshow(vec_eig)
    plt.savefig("plots/testvec.png", dpi=800)
    plt.close()
    #############################

    """
    #########################
    syst_todecomp = syst_all
    corr = get_correlation(syst_todecomp)
    print(corr.shape,"corr shape")
    
    cov = get_covariance(syst_todecomp)
    A = cov.copy()
    print(cov.shape,"cov shape")
    
    inner = (syst_todecomp.T @ A).T @ syst_todecomp.T
    print(inner.shape)
    
    unc_tot = np.sqrt(np.abs(np.sum(inner, axis=1)))
    print(unc_tot)
    sig_tot = unc_tot
    
    vec_eig_kin, val_eig_kin = diagonalize_matrix(cov,normalize=True)
    
    print("eigenvalues",val_eig_kin.diagonal())
    np.savetxt("eig.txt",val_eig_kin.diagonal())
    #cov_eig = vec_eig @ np.diag(val_eig_kin) @ np.linalg.inv(vec_eig)

    cov_eig = vec_eig_kin @ val_eig_kin
    print(cov_eig)
    cov_eig = cov_eig  @ np.linalg.inv(vec_eig_kin)
    print(cov_eig)
    print(cov_eig.shape,"cov shape")
    print(np.allclose(cov,cov_eig))
    
    
    # get the covariance matrix
    corr_eig = get_correlation(cov_eig,dejacov=True)
    print(corr_eig.shape,"corr shape")
    print(np.allclose(corr,corr_eig))
    
    #print(np.diag(val_eig).shape)
    #divisor = np.sqrt(np.diag(val_eig))
    #print(divisor.shape)
    #vec_eig_kin = vec_eig_kin * np.diag(1.0 / divisor)
    #print(vec_eig_kin.shape)
    #inner = vec_eig_kin @ sig_tot @ np.linalg.inv(vec_eig_kin)
    #inner = ((np.linalg.inv(vec_eig_kin).T @ syst_todecomp).T @ cov_eig).T @ (np.linalg.inv(vec_eig_kin).T @ syst_todecomp).T
    print("syst_all shape:", syst_all.T.shape, "vec_eig_kin shape:",vec_eig_kin.shape)
    
    #############################
    
    #############################
    num_eig = val_eig.shape[0]
    eig_val_diff = np.subtract(val_eig,val_eig_kin[:num_eig,:num_eig])
    print("Total difference in eigenvalues: ",np.sum(eig_val_diff))
    #############################
    """
    # eigenvalues are already sorted by size, let's find the largest 95%
    val_eig = val_eig.diagonal()
    counteig = 0
    currentsum = 0
    for eig in val_eig:
        print("eigenvalue", counteig, eig)
        currentsum += eig
        counteig += 1
        if currentsum / np.sum(val_eig) > 0.99:
            print("got the most")
            break
        if counteig > doFifteen:
            print(" we have more than ", doFifteen, " eig values ")
            break
    print("counted eigenvalues", counteig)

    # print ' checking vec eig ', vec_eig.shape
    # for i in range(0,vec_eig.shape[0]) :
    #    print ' checking vec eig ', np.sum(vec_eig[:,i]**2)
    print(
        "vec_eig",
        vec_eig[:, :counteig].shape,
        vec_eig[:, :counteig].dot(np.diag(np.sqrt(val_eig[:counteig]))).shape,
    )
    gamma = vec_eig[:, :counteig].dot(np.diag(np.sqrt(val_eig[:counteig])))
    rest = vec_eig[:, counteig:].dot(np.diag(np.sqrt(val_eig[counteig:])))
    print("gamma shape: ", gamma.shape, "rest shape: ", rest.shape)
    rest = np.sqrt(np.sum(rest**2, axis=1))
    print(np.sqrt(np.sum(rest**2)))
    if counteig < doFifteen:
        gamma = np.column_stack(
            (gamma, np.zeros((gamma.shape[0], doFifteen - counteig)))
        )
    print(gamma.shape, rest.shape)
    print("decomped")
    uncorr = []
    for i in range(0, len(syst)):
        currentsyst = gamma[: syst[i].shape[0], :]
        remainingsyst = gamma[syst[i].shape[0] :, :]

        syst[i] = currentsyst
        gamma = remainingsyst

        currentrest = rest[: syst[i].shape[0]]
        remainingrest = rest[syst[i].shape[0] :]
        #    print ' current rest  ', currentrest.shape, ' remaining ', remainingrest.shape
        uncorr.append(currentrest)
        rest = remainingrest
    print("len syst 1", len(syst[0]))
    print("len syst 2", len(syst[1]))

    outfile = ROOT.TFile(outfname, "RECREATE")

    for i in range(0, len(SF)):
        outfile.cd()
        runrange = lookupRunRange(years if years == 0 else int(years[i]))

        prefix = sflabel + "_"
        if not outfile.GetDirectory(runrange):
            outfile.mkdir(runrange)
        else:  # the second get's the LowPt prefix, didn't have a better idea how to handle this
            prefix = prefix + "LowPt_"
        print("writing to egamma file")
        outfile.cd(runrange)
        print(
            len(SF[i]),
            len(stat[i]),
            len(syst[i]),
            len(uncorr[i]),
            len(xbin[i]),
            len(ybin[i]),
        )
        print("xbins", xbin[i])
        writeToEgammaFile(
            SF[i], stat[i], syst[i], uncorr[i], xbin[i], ybin[i], prefix, False
        )

        h_eig = ROOT.TH1D(
            prefix + "eig", prefix + "eig", doFifteen + 1, 0, doFifteen + 1
        )
        h_eig.SetDirectory(0)
        #    print ' PHILIP eig ', val_eig[:counteig], ' sum ', np.sum(val_eig), ' fraction ', np.sum(val_eig[:counteig])
        for eig in range(0, counteig - 1):
            h_eig.SetBinContent(doFifteen - eig + 1, val_eig[eig])
        h_eig.SetBinContent(1, np.sum(val_eig[counteig - 1 :]))
        #    print ' PHILIP integral ', h_eig.Integral()
        h_eig.Scale(1.0 / h_eig.Integral())
        h_eig.Write()
    print("done")
    outfile.Write()
    outfile.Close()
