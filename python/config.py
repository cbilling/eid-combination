ziso_data_path = {
    "TightLLH_d0z0": {
        2018: "/eos/user/c/cbilling/eid-comb-inputs/ziso/inclusive/scalefactors_ziso_for_comb_2018.root"
    }
}

zisoPrebin_data_path = {
    "TightLLH_d0z0": {
        2018: "/eos/user/c/cbilling/eid-comb-inputs/ziso/lowPtExtraBin/scalefactors_ziso_for_comb_2018.root"
    }
}

zmass_data_path = {
    "TightLLH_d0z0": {
        2018: "/eos/user/c/cbilling/eid-comb-inputs/zmass/inclusive/scalefactors_zmass_for_comb_2018.root"
    }
}

zmassPrebin_data_path = {
    "TightLLH_d0z0": {
        2018: "/eos/user/c/cbilling/eid-comb-inputs/zmass/lowPtExtraBin/scalefactors_zmass_for_comb_2018.root"
    }
}

jpsi_data_path = {
    "TightLLH_d0z0": {
        2018: "/eos/user/c/cbilling/eid-comb-inputs/jpsi/lowPtExtraBin/scalefactors_jpsi_for_comb_2018.root"
    }
}
