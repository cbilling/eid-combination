from functools import total_ordering
from .tools.arrayToEgamma import *
from .tools.statTools import *
import numpy as np
import ROOT
from .logger import logger
import mplhep
import matplotlib.pyplot as plt
import seaborn as sns

mplhep.style.use("ATLAS")

# the old doFifteen variable makes sure we always fill 15 corr systs, regardless of the size of the eigenvalues

sflabel = "FullSim"


def stackForDecomp(indirs=[], years=[], stat_in_decomp=False):

    # read the values from the previous steps
    SF = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    # dictionaries with these keys: SF, x, y
    for indir in indirs:
        SF.append(indir.sf)
        stat.append(indir.stat)
        syst.append(indir.syst)
        xbin.append(indir.x_bins)
        ybin.append(indir.y_bins)

    # Come up with better padding
    nSys = syst[0].shape[1]
    for i in range(1, len(SF)):

        # for Jpsi we also need to pad the syst, so let's store nSys (for the length we don't care)
        nSys_i = syst[i].shape[1]
        nSys = max(nSys, nSys_i)

    # stack the systs together so we can decompose
    syst_all = np.zeros((syst[0].shape[0], nSys))
    syst_all[: syst[0].shape[0], : syst[0].shape[1]] = syst[0]
    stat_all = stat[0]
    for i in range(1, len(SF)):
        syst_i = np.zeros((syst[i].shape[0], nSys))
        syst_i[: syst[i].shape[0], : syst[i].shape[1]] = syst[i]
        syst_all = np.concatenate((syst_all, syst_i), axis=0)
        stat_all = np.concatenate((stat_all, stat[i]))
    if stat_in_decomp:

        logger.info("Stacking stat at the front of syst for decomp")

        print("Before", syst_all.shape)
        syst_all = np.column_stack((np.diag(stat_all), syst_all))
        print("After", syst_all.shape)
    return SF, stat, syst, xbin, ybin, syst_all


def eigendecomposition(syst_all, normalize=False):
    # get the covariance matrix
    cov = get_covariance(syst_all)

    val_eig, vec_eig = np.linalg.eigh(cov)

    # !!! the stupid thing is upside down compared to linalg.eig !!!
    val_eig = np.flipud(val_eig)
    vec_eig = np.fliplr(vec_eig)

    val_eig = np.abs(val_eig)
    print("vec_eig shape: ", vec_eig.shape)

    if normalize:
        # small_number = 1e-10
        # val_eig[val_eig < small_number] = small_number
        norm_factor = np.diag(1.0 / np.sqrt(val_eig))
        print("norm factor: ", norm_factor.shape)
        vec_eig = vec_eig @ norm_factor
    return vec_eig, val_eig


def countSignificantEig(
    val_eig: np.ndarray, threshold: float = 0.99, max_sig_eig: int = 15
) -> int:
    # Study showed that we always want to keep 15

    numsignifcanteig = 0
    currentsum = 0
    for eig in val_eig:
        currentsum += eig
        numsignifcanteig += 1
        if currentsum / np.sum(val_eig) > threshold:
            break
        if numsignifcanteig > max_sig_eig:
            print(" we have more than ", max_sig_eig, " eig values ")
            break
    return numsignifcanteig


def splitVectors(syst, vec_eig, val_eig, numsignifcanteig):
    # Split the eigenvectors into high and low pt

    gamma = vec_eig[:, :numsignifcanteig].dot(
        np.diag(np.sqrt(val_eig[:numsignifcanteig]))
    )
    rest = vec_eig[:, numsignifcanteig:].dot(
        np.diag(np.sqrt(val_eig[numsignifcanteig:]))
    )
    rest = np.sqrt(np.sum(rest**2, axis=1))
    # need to resolve this logic, now that we don't have a hardcoded NUMCORRSYST
    # if numsignifcanteig < NUMCORRSYST:
    #     gamma = np.column_stack(
    #         (gamma, np.zeros((gamma.shape[0], NUMCORRSYST - numsignifcanteig)))
    #     )
    print("decomped")
    # Split the eigenvectors into significant eigenvectors
    # and the uncorrelated variable (all the insignificant ones)
    uncorr = []
    for i in range(0, len(syst)):
        currentsyst = gamma[: syst[i].shape[0], :]
        remainingsyst = gamma[syst[i].shape[0] :, :]

        syst[i] = currentsyst
        gamma = remainingsyst

        currentrest = rest[: syst[i].shape[0]]
        remainingrest = rest[syst[i].shape[0] :]
        #    print ' current rest  ', currentrest.shape, ' remaining ', remainingrest.shape
        uncorr.append(currentrest)
        rest = remainingrest
    return syst, uncorr


def checkDecompUncertainty(syst_all, vec_eig, val_eig):

    regular_unc = get_standarddev(syst_all)

    eig_unc = get_standarddev(vec_eig @ np.sqrt(np.diag(val_eig)))

    total_err = np.sum(np.abs(regular_unc - eig_unc))
    print("Total summed error after decomposition, ", total_err)
    return total_err


def checkDecompCorr(syst_all, vec_eig, val_eig):
    corr = get_correlation(syst_all)
    print(corr)
    T = vec_eig @ np.sqrt(np.diag(val_eig))
    corr_in_eig = get_correlation(T)
    print(corr_in_eig)
    total_err = np.sum(np.abs(np.subtract(corr, corr_in_eig).flatten()))

    print("Correlation is different from decomposition by: ", total_err)
    return total_err


def npreduction(indirs=[], years=[]):

    SF, stat, syst, xbin, ybin, syst_all = stackForDecomp(indirs=indirs, years=years)

    vec_eig, val_eig = eigendecomposition(syst_all)

    total_corr_err = []
    total_kin_err = []
    for i in range(len(vec_eig)):
        print(f"removed {i} eigenvectors")
        corr_err = checkDecompCorr(syst_all, vec_eig, val_eig)
        total_corr_err.append(corr_err)
        kin_err = checkDecompUncertainty(syst_all, vec_eig, val_eig)
        total_kin_err.append(kin_err)
        vec_eig = vec_eig[:, : len(vec_eig) - 1 - i]
        val_eig = val_eig[: vec_eig.shape[1]]


def plot_unc_by_kinematic_bin(syst_all):
    fig, ax = plt.subplots(figsize=(10, 5))

    mplhep.atlas.label(rlabel="")
    low = get_standarddev(np.flipud(syst_all))[0:28][::-1]
    hi = get_standarddev(np.flipud(syst_all))[28:228]
    ax.plot(low)
    ax.set_xlim(0, 28)
    ax.set_ylim(0, 1.1)
    ax.set_xlabel("Kinematic bin #")
    ax.set_ylabel("Uncertainty")
    plt.title("Total Combination Uncertainty")
    plt.savefig("log_plots/unc_by_bin_low_v2.png")
    plt.close()

    fig, ax = plt.subplots(figsize=(10, 5))

    mplhep.atlas.label(rlabel="")
    ax.plot(get_standarddev(np.flipud(syst_all))[28:228])
    ax.set_xlim(0, 200)
    ax.set_ylim(0, 2.2e-2)
    ax.set_xlabel("Kinematic bin #")
    ax.set_ylabel("Uncertainty")
    plt.title("Total Combination Uncertainty")
    plt.savefig("log_plots/unc_by_bin_hi_v2.png")
    plt.close()


def plot_heatmap(matrix, xticklabels=False, yticklabels=False, size=(10, 10), path=""):
    plt.figure(figsize=size)
    ax = sns.heatmap(
        matrix,
        annot=False,
        xticklabels=xticklabels,
        yticklabels=yticklabels,
        cmap="YlGnBu",
    )
    # ax.xaxis.tick_top()
    size = 15
    plt.yticks(fontsize=size)
    plt.xticks(fontsize=size)

    ax.use_sticky_edges
    plt.savefig(f"{path}.pdf", bbox_inches="tight", dpi=800)
    plt.close()


### Star of the show! ###
def writeRootFiles_decomp(
    indirs=[],
    years=[],
    outfname="",
    eig_threshold=0.99,
    max_sig_eig=15,
    use_exact_eig_num=False,
    stat_in_decomp=False,
    sflabel="FullSim",
):

    # syst is a vector of the vectors of systematics, syst_all is just s 2d-array that can be acted on by the different functions
    # Put all the scalefactors and stats and systs together so we can decompose
    SF, stat, syst, xbin, ybin, syst_all = stackForDecomp(
        indirs=indirs, years=years, stat_in_decomp=stat_in_decomp
    )

    plot_unc_by_kinematic_bin(syst_all)

    # Perform the eigen decomposition
    vec_eig, val_eig = eigendecomposition(syst_all)

    # Simply prints the difference in total uncertainty before and after you decompose
    # Will not fail if this difference is high
    print("decomposition in writeRoot")
    checkDecompUncertainty(syst_all, vec_eig, val_eig)

    # Prints the sum of the difference of a bin by bin comparison of the correlation
    # matrices before and after decompositoin
    checkDecompCorr(syst_all, vec_eig, val_eig)

    if use_exact_eig_num:
        numsignifcanteig = max_sig_eig
    else:
        numsignifcanteig = countSignificantEig(
            val_eig, threshold=eig_threshold, max_sig_eig=max_sig_eig
        )

    # Splits into the eigenvectors that were significant (syst) and not significant
    # enough (leftovers->uncorr)
    syst, uncorr = splitVectors(syst, vec_eig, val_eig, numsignifcanteig)
    print("Number of significant Eigenvalues: ", numsignifcanteig)
    # Put this into a file
    writeToEgammaFile(
        SF,
        stat,
        syst,
        uncorr,
        xbin,
        ybin,
        val_eig,
        numsignifcanteig,
        years,
        outfname,
        sflabel=sflabel,
    )


def studyEigenMapping(indirs=[], years=[], stat_in_decomp=False, plots_dir=""):

    os.makedirs(plots_dir, exist_ok=True)
    # Put all the scalefactors and stats and systs together so we can decompose
    SF, stat, syst, xbin, ybin, syst_all = stackForDecomp(
        indirs=indirs, years=years, stat_in_decomp=stat_in_decomp
    )
    systlabels = indirs[0].syst_labels
    plot_heatmap(
        syst_all, xticklabels=systlabels, path=f"{plots_dir}/syst_all", size=(4, 16)
    )

    # Perform the eigen decomposition
    evlabels = [f"EV {i}" for i in range(len(syst_all.T))]

    vec_eig, val_eig = eigendecomposition(syst_all.T, normalize=False)

    plot_heatmap(
        vec_eig,
        xticklabels=evlabels,
        yticklabels=systlabels,
        path=f"{plots_dir}/eig_vec",
    )


def cov_to_cor(cov):
    sigma_inv = 1.0 / np.sqrt(np.diag(cov))
    sigmaij_inv = np.outer(sigma_inv, sigma_inv)
    cor = cov * sigmaij_inv
    return cor


def writeRootFiles_decomp_stat(indirs=[], years=[], outfname="", stat_in_decomp=False):

    ## Test function used for studying the effects of statistics in the decomposition
    ## also for studying what happens when you drop n number of eigenvectors
    if stat_in_decomp:
        print("WRITEROOT")
    SF, stat, syst, xbin, ybin, syst_all = stackForDecomp(
        indirs=indirs, years=years, stat_in_decomp=False
    )
    SF_stat, stat_stat, syst_stat, xbin_stat, ybin_stat, syst_all_stat = stackForDecomp(
        indirs=indirs, years=years, stat_in_decomp=True
    )

    fig, ax = plt.subplots(figsize=(10, 30))
    ax.imshow(syst_all)
    plt.savefig(f"syst_all.pdf")
    plt.close()
    fig, ax = plt.subplots(figsize=(10, 5))

    mplhep.atlas.label(rlabel="")
    ax.plot(get_standarddev(np.flipud(syst_all)))
    ax.set_xlim(0, 228)
    ax.set_ylim(-0.02, 0.4)
    ax.set_xlabel("Kinematic bin #")
    ax.set_ylabel("Uncertainty")
    plt.title("Total Combination Uncertainty")
    plt.savefig("test.png")
    plt.close()
    vec_eig, val_eig = eigendecomposition(syst_all)
    vec_eig_stat, val_eig_stat = eigendecomposition(syst_all_stat)

    plt.plot(val_eig[:20])
    plt.savefig("val.png")
    plt.close()
    checkDecompUncertainty(syst_all, vec_eig, val_eig)

    checkDecompCorr(syst_all, vec_eig, val_eig)

    numsignifcanteig = countSignificantEig(val_eig)

    corr = get_correlation(syst_all)
    cov = get_covariance(syst_all)
    val_eig = np.diag(val_eig)

    corr_stat = get_correlation(syst_all_stat)
    cov_stat = get_covariance(syst_all_stat)
    val_eig_stat = np.diag(val_eig_stat)
    vec_eig_static = vec_eig
    val_eig_static = val_eig
    for i in range(len(vec_eig)):
        print("vec eig shape ", vec_eig.shape, "val_eig shape ", val_eig.shape)
        if len(val_eig) > 1:
            T = vec_eig @ np.sqrt(val_eig)
        else:
            T = vec_eig * np.sqrt(val_eig)
        cov_in_eig = T @ T.T
        print(cov.shape, "cov shape")
        corr_in_eig = cov_to_cor(cov_in_eig)

        T_stat = vec_eig_stat @ np.sqrt(val_eig_stat)
        cov_in_eig_stat = T_stat @ T_stat.T
        print(cov.shape, "cov shape")
        corr_in_eig_stat = cov_to_cor(cov_in_eig_stat)
        fig, ax = plt.subplots(1, 3)
        ax[0].imshow(corr_in_eig)
        ax[1].imshow(corr_in_eig_stat)
        ax[2].imshow(corr_in_eig / corr_in_eig_stat)
        plt.savefig(f"np_red_plots/corrcompare{i}.png", dpi=800)
        plt.close()

        # print(corr[0,:],corr_in_eig[0,:])
        diff_in_corr_space = np.divide(corr_in_eig_stat, corr_in_eig).flatten()
        fig, ax = plt.subplots()
        ax.hist(diff_in_corr_space, bins=500, range=(0.90, 1.1))
        plt.savefig(f"np_red_plots/diff_in_corr_space{i}.png")
        plt.close()

        # already in kinematic space
        regular_unc = get_standarddev(syst_all)

        eig_unc = get_standarddev(vec_eig @ np.sqrt(val_eig))
        """fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [4, 1]})
        print(len(ax))
        ax[0].plot(regular_unc, "b")
        ax[0].plot(eig_unc, "r*")
        ax[1].plot(regular_unc / eig_unc)
        # ax[1].set_ylim([0.9, 1.1])
        plt.savefig(f"np_red_plots/compunc{i}.png")
        plt.close()
        """
        # vec_eig = vec_eig_static[:, i]
        # val_eig = np.array(val_eig_static[i])
        # vec_eig = vec_eig[:, : len(vec_eig) - 1 - i]
        # vec_eig_norm = vec_eig_norm[:, : len(vec_eig_norm) - 1 - i]
        # val_eig = np.diag(val_eig.diagonal()[: vec_eig.shape[1]])
        # val_eig_norm = np.diag(val_eig_norm.diagonal()[: vec_eig_norm.shape[1]])
        break
    # The syst variable is now the decomposed vectors
    val_eig = val_eig.diagonal()
    syst, uncorr = splitVectors(syst, vec_eig, val_eig, numsignifcanteig)

    outfile = ROOT.TFile(outfname, "RECREATE")

    for i in range(0, len(SF)):
        outfile.cd()
        runrange = lookupRunRange(years if years == 0 else int(years[i]))

        prefix = sflabel + "_"
        if not outfile.GetDirectory(runrange):
            outfile.mkdir(runrange)
        else:  # the second get's the LowPt prefix, didn't have a better idea how to handle this
            prefix = prefix + "LowPt_"

        outfile.cd(runrange)

        writeToEgammaFile(
            SF[i], stat[i], syst[i], uncorr[i], xbin[i], ybin[i], prefix, False
        )

        h_eig = ROOT.TH1D(
            prefix + "eig", prefix + "eig", NUMCORRSYST + 1, 0, NUMCORRSYST + 1
        )
        h_eig.SetDirectory(0)

        for eig in range(0, numsignifcanteig - 1):
            h_eig.SetBinContent(NUMCORRSYST - eig + 1, val_eig[eig])
        h_eig.SetBinContent(1, np.sum(val_eig[numsignifcanteig - 1 :]))

        h_eig.Scale(1.0 / h_eig.Integral())
        h_eig.Write()

    outfile.Write()
    outfile.Close()


def writeRoot(years, indirs, outfname, sflabel="FullSim", doCorr=False):

    # read the values from the previous steps
    SF = []
    stat = []
    syst = []
    xbin = []
    ybin = []
    for indir in indirs:

        SF_meth, stat_meth, syst_meth, x_meth, y_meth = (
            indir.sf,
            indir.stat,
            indir.syst,
            indir.x_bins,
            indir.y_bins,
        )
        SF.append(SF_meth)
        stat.append(stat_meth)
        syst.append(syst_meth)
        xbin.append(x_meth)
        ybin.append(y_meth)

    if years and len(years) != len(SF):
        print(" in writeRootFiles.py: you didn't provide enough years ")
        exit()

    outfile = TFile(outfname, "RECREATE")

    for i in range(0, len(SF)):
        print(xbin)
        outfile.cd()
        outfile.mkdir(lookupRunRange(years if years == 0 else int(years[i])))
        outfile.cd(lookupRunRange(years if years == 0 else int(years[i])))
        writeHistToEgammaFile(
            SF[i],
            stat[i],
            syst[i],
            np.zeros(SF[i].shape),
            xbin[i],
            ybin[i],
            sflabel + "_",
            doCorr=doCorr,
        )

    outfile.Write()
    outfile.Close()
