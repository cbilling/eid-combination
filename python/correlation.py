import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
from .tools.rootToArray import *
from .tools.plotting import *
from . import MethodSF


def decorrelateStat(indir_meth1, indir_meth2, x, y):
    SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst(
        indir_meth1, x, y
    )
    SF_meth2, stat_meth2, syst_meth2, x_meth2, y_meth2 = readArray_SF_stat_syst(
        indir_meth2, x, y
    )
    print(SF_meth1.shape, SF_meth2.shape)
    # consistent = True
    # consistent = consistent and consistency(x_meth1,x_meth2)
    # consistent = consistent and consistency(y_meth1,y_meth2)

    SF_out = np.copy(SF_meth1)
    uncorr_out = np.zeros(SF_meth1.shape)
    corr_out = np.zeros(SF_meth1.shape)  # the syst here is the correlated stat

    for bin in range(0, len(SF_out)):
        stat_ref_rel = stat_meth1[bin] / SF_meth1[bin]
        uncorr_new_rel = stat_meth2[bin] / SF_meth2[bin]
        corr_new_rel = syst_meth2[bin] / SF_meth2[bin]

        rescale = stat_ref_rel / np.sqrt(uncorr_new_rel**2 + corr_new_rel**2)
        if uncorr_new_rel == 0.0 or np.isnan(uncorr_new_rel):
            rescale = 1.0
            stat_ref_rel = 0.0
            uncorr_new_rel = 0.0
            corr_new_rel = 0.0
            SF_out[bin] = 0.0
            syst_meth1[bin, :] = np.zeros(syst_meth1.shape[1])

        uncorr_out[bin] = uncorr_new_rel * rescale * SF_meth1[bin]
        corr_out[bin] = corr_new_rel * rescale * SF_meth1[bin]

    corr_out = np.diag(corr_out)
    # corr_out = np.zeros(SF_out.shape)

    # writing where the correlated stats go at the beginning of systs
    # writeArray( outdir, np.column_stack( (SF_out, uncorr_out, corr_out, syst_meth1) ), x_meth1, y_meth2 )

    arr_out = np.column_stack((SF_out, uncorr_out, corr_out, syst_meth1))
    return arr_out


# nCorr = 220 because when you diagonalize stats, you get one for each kinematic bin
def decorrelateSyst(method1, method2, inplace=True, nCorr=220):

    meth1 = MethodSF()
    meth2 = MethodSF()
    meth1.copy(method1)
    meth2.copy(method2)
    print(" correlating the first ", nCorr, " sources of syst uncertainty ")
    if nCorr == 2:
        print(meth1.syst)
        print(meth2.syst)
    print("print shape before decorr: ", meth1.syst.shape)
    # print("syst labels before", meth1.syst_labels)
    meth1.syst, meth2.syst = decorrelate(meth1.syst, meth2.syst, list(range(nCorr)))
    meth1.syst_labels, meth2.syst_labels = decorrelate_syst_labels(
        meth1.syst_labels, meth2.syst_labels, list(range(nCorr))
    )
    print("syst labels after", meth1.syst_labels)
    # print("print shape after decorr: ", meth1.syst.shape)
    if nCorr == 2:
        print(meth1.syst)
        print(meth2.syst)

    return meth1, meth2


def undecorrelateStat(indir_meth1, x, y, nCorr=-1):
    SF_meth1, stat_meth1, syst_meth1, x_meth1, y_meth1 = readArray_SF_stat_syst(
        indir_meth1, x, y
    )

    print(SF_meth1.shape, syst_meth1.shape, stat_meth1.shape, "BEFORE")
    nStat = nCorr
    if nCorr < 0:
        nStat = SF_meth1.shape[0]
    print(nStat, "nSTAT")
    SF_out = np.copy(SF_meth1)
    print(np.sum(syst_meth1[:, :nStat] ** 2))
    stat_out = np.sqrt(
        np.copy(stat_meth1) ** 2 + np.sum(syst_meth1[:, :nStat] ** 2, axis=1)
    )
    syst_out = np.copy(syst_meth1[:, nStat:])
    print(syst_out[0])
    print(syst_out[-1])
    print(SF_out.shape, "STATOUT")
    print(stat_out.shape, "STATOUT")
    print(syst_out.shape, "SYSTOUT")
    arr_out1 = np.column_stack((SF_out, stat_out, syst_out))
    return arr_out1
