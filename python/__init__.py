"""
.. module:: SFCombination
"""

__version__ = "1.0.0"

from .dataclass import (
    ZisoData,
    ZisoDataV2,
    ZisoStatData,
    ZisoStatDataV2,
    ZmassData,
    ZmassStatData,
    JpsiData,
    ZrecoData,
    SimpleTPData,
    MethodSF,
)
from . import config, test_config
from .correlation import decorrelateSyst
from .combinator import combineHera, combinemethods_BLUE, combineSysLabels
from .writeRoot import writeRootFiles_decomp, npreduction, studyEigenMapping, writeRoot
from .plotting import plot_scale_factors_at_pt, plot_all_scale_factors
from .logger import logger
from .tools import getArray_SF_stat_syst_refactored
