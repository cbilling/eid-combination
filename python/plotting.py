from cProfile import label
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
from .tools.statTools import get_standarddev
import os
import math

hep.style.use("ATLAS")

### Add pt region that we are plotting in the title
### make the plots tighter together

from collections import defaultdict


def group_dataclasses_by_y_bins(dataclasses: list, labels: list, colors: list):
    grouped = defaultdict(lambda: ([], [], []))

    for dataclass, label, color in zip(dataclasses, labels, colors):
        grouped[tuple(dataclass.y_bins)][0].append(dataclass)
        grouped[tuple(dataclass.y_bins)][1].append(label)
        grouped[tuple(dataclass.y_bins)][2].append(color)

    return list(grouped.values())


def plot_scale_factors_at_pt(methods: list, labels: list, name="test"):
    colors = ["black", "red", "blue"]
    shift = [0, 0.025, -0.025]
    for pT in range(len(methods[0].x_bins)):

        # Data points and error bars for the plot (based on the image)

        # Creating figure and subplots
        fig, (ax1, ax2) = plt.subplots(
            2, 1, figsize=(8, 8), sharex=True, gridspec_kw={"height_ratios": [3.5, 1]}
        )

        for i, method in enumerate(methods):
            eta = list(method.y_bins)
            eta.append(method.last_eta_bound)
            center_of_eta_bins = []
            xerr = []
            for j in range(len(eta) - 1):
                center = (eta[j + 1] + eta[j]) / 2
                size = (eta[j + 1] - eta[j]) / 2
                center_of_eta_bins.append(center + shift[i])
                xerr.append(size)
            eta = center_of_eta_bins

            yerr = method.get_totunc(ptBin=pT)[0]

            # Top plot

            ax1.errorbar(
                eta,
                method.getSF(ptBin=pT)[0],
                yerr=yerr,
                xerr=xerr,
                fmt=".",
                color=colors[i],
                label=labels[i],
                capsize=3,
                capthick=1.5,
            )

            # Ratio is always set to first method
            ratio = methods[0].getSF(ptBin=pT)[0] / method.getSF(ptBin=pT)[0]
            ax2.errorbar(
                eta,
                ratio,
                yerr=yerr,
                xerr=xerr,
                fmt=".",
                color=colors[i],
                label=labels[i],
                capsize=3,
                capthick=1.5,
            )
            if i == 0:
                for k in range(len(eta)):
                    ax2.fill_between(
                        [eta[k] - xerr[k], eta[k] + xerr[k]],
                        ratio[k] - yerr[k],
                        ratio[k] + yerr[k],
                        color="yellow",
                        alpha=0.5,
                    )
        print(min(yerr), max(yerr))
        pts = list(methods[0].x_bins) + [methods[0].last_pt_bound]
        ax1.set_ylabel("Scale factor")
        ax1.set_xlim(eta[0] - xerr[k] - 0.1, eta[-1] + xerr[k] + 0.1)
        ax1.set_ylim(
            min(methods[0].getSF(ptBin=pT)[0]) - max(yerr) - 0.2,
            max(methods[0].getSF(ptBin=pT)[0]) + max(yerr) + 0.2,
        )
        ax1.legend(loc="upper right")
        ax1.text(
            0.05,
            0.85,
            f"{pts[pT]} < $E_T$ < {pts[pT+1]} GeV",
            ha="left",
            va="top",
            transform=ax1.transAxes,
        )
        ax2.set_ylabel("Ratio")
        ax2.set_ylim(0.95, 1.05)
        ax2.axhline(1, color="black", linestyle="--")

        ax2.set_xlabel(r"$\eta$")

        # Style settings
        hep.style.use("ATLAS")  # Applying ATLAS style
        hep.atlas.label(label="Work in Progrees", loc=4, ax=ax1)
        plt.tight_layout()
        os.makedirs("kinematic_plots", exist_ok=True)
        plt.savefig(f"kinematic_plots/{name}_pt{pT}.png")
        plt.close()


def plot_all_scale_factors(
    methods: list, labels: list, colors=["black", "red", "blue"], name="test"
):

    shift = []
    for i in range(len(methods)):
        shift.append(i * 0.025)
    grouped = group_dataclasses_by_y_bins(methods, labels, colors)
    print("groups ", len(grouped))
    for g in grouped:
        print("group ", len(g))
    # Creating figure and subplots
    total_pt_bins = set()
    total_eta_bins = set()
    for method in methods:
        total_pt_bins.update(method.x_bins)
        total_eta_bins.update(method.y_bins)
    total_pt_bins = sorted(total_pt_bins)
    total_eta_bins = sorted(total_eta_bins)

    fig, ax = plt.subplots(
        2,
        len(total_pt_bins),
        figsize=(max(len(total_pt_bins) * 3.5, 8), 8),
        sharex=True,
        sharey="row",
        gridspec_kw={"height_ratios": [3.5, 1]},
    )
    # enforce 2 dimensions in the case that you only have one pt bin
    if ax.ndim == 1:

        ax = ax.reshape(2, -1)  # Reshape to (2, ndim)
    print(ax.shape)
    print(total_pt_bins)
    print(labels)
    for pTBin, pT in enumerate(total_pt_bins):
        for groups in grouped:
            methods, labels, colors = groups
            ratioed = False
            for i, method in enumerate(methods):
                if pT not in method.x_bins:
                    print("skipping ", pT)
                    continue
                print("method ", labels[i])
                top_axs = ax[0, pTBin]
                ratio_axs = ax[1, pTBin]
                eta = list(method.y_bins)
                eta.append(method.last_eta_bound)
                center_of_eta_bins = []
                xerr = []
                for j in range(len(eta) - 1):
                    center = (eta[j + 1] + eta[j]) / 2
                    size = (eta[j + 1] - eta[j]) / 2
                    center_of_eta_bins.append(center + shift[i])
                    xerr.append(size)
                eta = center_of_eta_bins

                yerr = method.get_totunc(pt=pT)[0]

                # Top plot

                top_axs.errorbar(
                    eta,
                    method.getSF(pt=pT)[0],
                    yerr=yerr,
                    xerr=xerr,
                    fmt=".",
                    color=colors[i],
                    label=labels[i],
                    capsize=3,
                    capthick=1.5,
                )

                # Ratio is always set to first method
                ratio = methods[0].getSF(pt=pT)[0] / method.getSF(pt=pT)[0]
                ratio_axs.errorbar(
                    eta,
                    ratio,
                    yerr=yerr,
                    xerr=xerr,
                    fmt=".",
                    color=colors[i],
                    label=labels[i],
                    capsize=3,
                    capthick=1.5,
                )
                if (not ratioed) and (not math.isnan(ratio[0])):
                    for k in range(len(eta)):
                        ratio_axs.fill_between(
                            [eta[k] - xerr[k], eta[k] + xerr[k]],
                            ratio[k] - yerr[k],
                            ratio[k] + yerr[k],
                            color="yellow",
                            alpha=0.5,
                        )
                    ratioed = True

                top_axs.set_xlim(eta[0] - xerr[0] - 0.1, eta[-1] + xerr[0] + 0.1)
                if pTBin == len(total_pt_bins) - 1:
                    print("at the edge")
                    top_axs.text(
                        0.05,
                        0.8,
                        f"{total_pt_bins[pTBin]/1000} < $E_T$ < {method.last_pt_bound/1000} GeV",
                        ha="left",
                        va="top",
                        transform=top_axs.transAxes,
                    )
                else:
                    top_axs.text(
                        0.05,
                        0.8,
                        f"{total_pt_bins[pTBin]/1000} < $E_T$ < {total_pt_bins[pTBin+1]/1000} GeV",
                        ha="left",
                        va="top",
                        transform=top_axs.transAxes,
                    )

                ratio_axs.set_ylim(0.95, 1.05)
                ratio_axs.axhline(1, color="black", linestyle="--")
    first_method = grouped[0][0][0]
    yerr = first_method.get_totunc(ptBin=0)[0]
    print("final yerr ", yerr)
    top_axs.set_ylim(
        min(first_method.sf) - max(yerr) - 0.2,
        max(first_method.sf) + max(yerr) + 0.2,
    )
    ax[0, 0].set_ylabel("Scale factor")
    ax[1, 0].set_ylabel("Ratio")
    ax[0, -1].legend(loc="upper right")
    ax[1, -1].set_xlabel(r"$\eta$")
    # Style settings
    hep.style.use("ATLAS")  # Applying ATLAS style
    hep.atlas.label(label="Work in Progrees", loc=4, ax=ax[0, 0])
    fig.tight_layout()
    os.makedirs("kinematic_plots", exist_ok=True)
    fig.subplots_adjust(wspace=0, hspace=0)
    fig.savefig(f"kinematic_plots/{name}.png")
    plt.close(fig)
