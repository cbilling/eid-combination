import sys, operator
from .tools.arrayToText import *
from .tools.statTools import *
from .tools.rootToArray import *
from .tools.plotting import *
import os
import numpy as np
from .dataclass import MethodSF

working_dir = os.environ["WORKING_DIR"]
data_dir = working_dir + "/data"


def combineSysLabels(method1, method2):
    # we need to end up with the same labels
    # compare index by index, if they are the same, do nothing
    # if one is zero, take the other, if they don't agree and are non-zero,
    # make a new label

    systlabel1 = method1.syst_labels
    systlabel2 = method2.syst_labels
    systToIter = systlabel1
    if len(systlabel2) > len(systlabel1):
        systToIter = systlabel2

    combinedLabel = []
    for i in range(len(systToIter)):
        if systlabel1[i] == systlabel2[i]:
            combinedLabel.append(systlabel1[i])
        elif systlabel1[i] == "0":
            combinedLabel.append(systlabel2[i])
        elif systlabel2[i] == "0":
            combinedLabel.append(systlabel1[i])
        else:
            combinedLabel.append(str(systlabel1[i]) + str(systlabel2[i]))
    return combinedLabel


def combinemethods_BLUE(method1, method2):
    SF_meth1, stat_meth1, syst_meth1 = method1.sf, method1.stat, method1.syst
    SF_meth2, stat_meth2, syst_meth2 = method2.sf, method2.stat, method2.syst
    # print("input: ")
    # print(SF_meth1)
    # print(SF_meth2)
    # the full blown chi2
    SF_all = np.array((SF_meth1, SF_meth2), dtype=object)  # it's a 2-d array
    # FROM NOW ON WE DECORRELATE THE INPUT WITH A DEDICATED SCRIPT
    # syst_meth1, syst_meth2 = decorrelate( syst_meth1, syst_meth2, list(range(220+2)) ) # in practise we want to keep columns 0 and 1 correlated, so there's a exception for columns [0,1]

    stat_all = np.array((stat_meth1, stat_meth2), dtype=object)
    syst_all = np.array((syst_meth1, syst_meth2), dtype=object)

    average, averstat, aversyst = getAverage_BLUE(
        SF_all, stat_all, syst_all
    )  # len(x_meth1))
    averstat = np.sqrt(averstat**2)

    # ensure_directory( outdir )
    # writeArray_SF_stat_syst( outdir, average, averstat, aversyst, x_meth1, y_meth1 )

    chi2 = get_chi2(average, 0, SF_all, stat_all, syst_all)
    ndof = len(SF_meth1)
    # np.savetxt( outdir+'/chi2_philip.txt'       ,  np.array([chi2,ndof]), fmt="%1.6f" )
    # print '    chi2 (chi2 incl all correlations): ', chi2/ndof

    method = MethodSF(average, averstat, aversyst, method1.x_bins, method1.y_bins)

    method.syst_labels = combineSysLabels(method1, method2)
    return method


def combineHera(indir_meth1, indir_meth2, x, y, wp, year):
    ZcombPath = f"{data_dir}/{wp}/{year}/Zcomb_final_loPt_rebin"
    JpsiPath = f"{data_dir}/{wp}/{year}/Jpsi"
    ZJcombPath = f"{data_dir}/{wp}/{year}/ZJcomb"

    os.makedirs(ZcombPath, exist_ok=True)
    os.makedirs(JpsiPath, exist_ok=True)
    os.makedirs(ZJcombPath, exist_ok=True)

    # need to add the number of rows of zeros that jpsi has
    zero_row = np.zeros(
        (abs(indir_meth1.shape[0] - indir_meth2.shape[0]), indir_meth1.shape[1])
    )

    # Prepending the row of zeros to the existing array
    indir_meth1 = np.vstack((zero_row, indir_meth1))

    writeArray(ZcombPath, indir_meth1, x, y)
    writeArray(JpsiPath, indir_meth2, x, y)
    os.system(f"python/averagerpy27/combineHera.sh {ZcombPath} {JpsiPath} {ZJcombPath}")

    ZJcomb, x, y = readArray(ZJcombPath)

    return MethodSF(ZJcomb[:, 0], ZJcomb[:, 1], ZJcomb[:, 2:], x, y)
