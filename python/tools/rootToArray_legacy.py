#!/usr/bin/env python

import array, re
import numpy as np
from ROOT import gDirectory, TFile, gROOT, TH2F, TH1F

def getArray_SF_stat_syst( filename, path, match, pTmin = 0., pTmax = 99999999. ) :
    # ------------------------------------------------------------------------------
    # this is the most important function for us
    #
    # it will return 5 np.arrays:
    # - the central values
    # - the stat uncertainty
    # - an array of systematic uncertainties, with columns of systematic sources
    # - the x-bins
    # - the y-bins
    #
    # it takes as input: 
    # - a root file name
    # - the path in the root file
    # - a string and all histograms matching the name will be considered to get the mean and rms
    # ------------------------------------------------------------------------------

    # here is what we're returning
    SF   = np.array([])
    stat = np.array([])
    syst = np.array([])
    xbins= np.array([])
    ybins= np.array([])

    infile = TFile.Open( filename )
    if not infile.IsOpen() :
        print( ' in rootToArray: problem opening file ',filename)
        return SF,stat,syst,xbins,ybins

    listOfCentralValues = []
    listOfStatErrors    = []

    directory = infile
    if len(path) : 
        directory = infile.Get(path)

    for hist in directory.GetListOfKeys() :

        hname = hist.GetName()
        reg = re.compile(match)

        if not bool(reg.match(hname)) :
#            print( ' skipping in hname ', hname , ' when looking for ', match)
            continue
            
        if 'CentralValue' in hname :
            print( ' adding hist central value ', hname)
            listOfCentralValues.append( directory.Get(hname) )
        elif 'StatError' in hname :
            print( ' adding hist stat error ', hname)
            listOfStatErrors.append( directory.Get(hname) )

    if not len(listOfCentralValues) or len(listOfCentralValues) != len(listOfStatErrors) :
        print( ' in rootToArray: found no or inconsistent number of histograms ',len(listOfCentralValues), ' ', len(listOfStatErrors))
    else :
        print( ' added ',len(listOfCentralValues), ' histograms ')

    # get the binning from the first one
    xbins,ybins = getArray_binning( listOfCentralValues[0] )

    # we need to cut some of the variations that are invalid
    cutoff_SF = 1.2
    cutoff_stat = 0.5
    cutoff_SF = 99999.
    cutoff_stat = 1.5

    # now getting central values and stuff
    allSFs  = [] #np.zeros( (xbins.size*ybins.size,0) )
    allStat = [] #np.zeros( (xbins.size*ybins.size,0) )
    for x in range(0,xbins.size) :
        for y in range(0,ybins.size) :
            var_val = []
            var_err = []
            histo = 0
            for cv in range(0,len(listOfCentralValues)) :
                val     = listOfCentralValues[cv].GetBinContent(x+1,y+1)
                err     = listOfStatErrors   [cv].GetBinContent(x+1,y+1)
                var_val.append( val )
                var_err.append( err )

            allSFs .append( np.array( var_val ))
            allStat.append( np.array( var_err ))
        
    
    SF   = np.zeros( len(allSFs ) )
    stat = np.zeros( len(allStat) )
    cov  = np.zeros( (len(allSFs),len(allSFs)) )
    for i in range(0,len(allSFs)) :
        mean = 0.
        mean_stat = 0.
        counter = 0.
        for var in range(0,len(allSFs[i])) :
            val = allSFs [i][var]
            err = allStat[i][var]

            if val>20. and val<100. : # it's Andrey's damn Jpsi efficiency that is in percent
                val = val/100.
                err = err/100.

            if val>0. and abs(val-1)<cutoff_SF and err<cutoff_stat : # skip a bunch of SF variations 
                mean += val
                mean_stat += err
                counter += 1
#                if i==185 :
#                    print( ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' adding mean ', val, ' stat ', err)
#                    print( ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' mean ', mean, ' stat ', mean_stat, ' counter ', counter)
            elif mean != 0 :
                print( ' skipping ', val, ' +- ', err, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]))
        if counter > 0. :
            SF  [i] = mean/counter
            stat[i] = mean_stat/counter
    for i in range(0,len(allSFs)) :
        for j in range(0,len(allSFs)) :
            corr = 0.
            counter = 0.
            for var in range(0,len(allSFs[i])) :
                vali = allSFs [i][var]
                valj = allSFs [j][var]
                erri = allStat[i][var]
                errj = allStat[j][var]
                if  vali>0. and valj>0. and abs(vali-1)<cutoff_SF and erri<cutoff_stat and abs(valj-1)<cutoff_SF and errj<cutoff_stat : # skip a bunch of SF variations 
                    corr += (vali-SF[i])*(valj-SF[j])
                    counter += 1.
#                elif vali != 0 and valj != 0 and erri<99999999999. and errj<99999999999. :
#                    print( ' skipping 2 ', vali, ' +- ', erri, '  ', valj, ' +- ', errj, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]), ' in bins ', xbins[int(j/len(ybins))], ' / ', ('%1.2f' % ybins[j%len(ybins)]))
            if counter > 0. :
                cov  [i,j] = corr/counter
#            else :
#                print( ' problem counting cov in bins ' ,i, ' ',j, ' ', counter)

    SF, stat, cov, xbins, ybins = stripFromArrayX    ( pTmin, SF, stat, cov, xbins, ybins )
    SF, stat, cov, xbins, ybins = stripFromArrayX_max( pTmax, SF, stat, cov, xbins, ybins )


    #--------------------------------------
    # let's run an eigenvalue decomposition
    #--------------------------------------
    val_eig,vec_eig = np.linalg.eigh(cov)

    # remove complex entries, in general all this can be complex but for positive definite matrix is real
    val_eig = np.flipud(val_eig)
    vec_eig = np.fliplr(vec_eig)
    for iv in range(0,len(val_eig)) : # if there are neg eigenvalues, set them to 0
        if val_eig[iv] < 0. :
            val_eig[iv] = 0

    eigsum = np.sum(val_eig)
    if eigsum == 0 :
        print( ' all eigenvalues are zero so the syst uncertainty must be zero, too ')
        return SF,stat,np.zeros(stat.shape),xbins,ybins
    
    # eigenvalues are already sorted by size, let's find the largest 99%
    counteig = 0
    currentsum = 0
    for eig in val_eig :

        currentsum += eig
        counteig += 1
        if currentsum/eigsum > 0.99 :
            break

    print( ' number of eig ', counteig, val_eig[:counteig])
    # the gamma matrix contains the sqrt( eigenvalue ) * eigenvectors
    # --> these are the uncorrelated sources of uncertainty
    gamma = vec_eig[:,:counteig].dot( np.diag( np.sqrt( val_eig[:counteig]) ) )
    rest  = vec_eig[:,counteig:].dot( np.diag( np.sqrt( val_eig[counteig:]) ) )
    rest  = np.sqrt( np.sum( rest**2, axis=1 ) )

    # copy the syst and ignore for now the uncorr
    syst = gamma
    # add the uncorr to stats
    stat = np.sqrt( np.sum( np.column_stack( (stat**2,rest**2) ), axis=1 ) )

    infile.Close()
    return SF,stat,syst,xbins,ybins


def getArray_SF_stat_syst_2( filename1, path1, match1, filename2, path2, match2, pTmin = 0., pTmax = 99999999. ) :
    # ------------------------------------------------------------------------------
    # this is the most important function for us
    #
    # it will return 5 np.arrays:
    # - the central values
    # - the stat uncertainty
    # - an array of systematic uncertainties, with columns of systematic sources
    # - the x-bins
    # - the y-bins
    #
    # it takes as input: 
    # - a root file name
    # - the path in the root file
    # - a string and all histograms matching the name will be considered to get the mean and rms
    # ------------------------------------------------------------------------------

    # here is what we're returning
    SF   = np.array([])
    stat = np.array([])
    syst = np.array([])
    xbins= np.array([])
    ybins= np.array([])

    infile1 = TFile.Open( filename1 )
    if not infile1.IsOpen() :
        print( ' in rootToArray: problem opening file ',filename1)
        return SF,stat,syst,xbins,ybins
    infile2 = TFile.Open( filename2 )
    if not infile2.IsOpen() :
        print( ' in rootToArray: problem opening file ',filename2)
        return SF,stat,syst,xbins,ybins

    # method 1
    listOfCentralValues1 = []
    listOfStatErrors1    = []

    directory1 = infile1
    if len(path1) : 
        directory1 = infile1.Get(path1)

    for hist in directory1.GetListOfKeys() :

        hname = hist.GetName()
        reg = re.compile(match1)
        
        if not bool(reg.match(hname)) :
            continue
            
        if 'CentralValue' in hname :
#            print( ' adding hist central value ', hname)
            listOfCentralValues1.append( directory1.Get(hname) )
        elif 'StatError' in hname :
#            print( ' adding hist stat error ', hname)
            listOfStatErrors1.append( directory1.Get(hname) )

    if not len(listOfCentralValues1) or len(listOfCentralValues1) != len(listOfStatErrors1) :
        print( ' in rootToArray: found no or inconsistent number of histograms ',len(listOfCentralValues1), ' ', len(listOfStatErrors1))
    else :
        print( ' added ',len(listOfCentralValues1), ' histograms for meth 1')



    # method 2
    listOfCentralValues2 = []
    listOfStatErrors2    = []

    directory2 = infile2
    if len(path2) : 
        directory2 = infile2.Get(path2)

    for hist in directory2.GetListOfKeys() :

        hname = hist.GetName()
        reg = re.compile(match2)
        if not bool(reg.match(hname)) :
            continue
            
        if 'CentralValue' in hname :
#            print( ' adding hist central value ', hname)
            listOfCentralValues2.append( directory2.Get(hname) )
        elif 'StatError' in hname :
#            print( ' adding hist stat error ', hname)
            listOfStatErrors2.append( directory2.Get(hname) )

    if not len(listOfCentralValues2) or len(listOfCentralValues2) != len(listOfStatErrors2) :
        print( ' in rootToArray: found no or inconsistent number of histograms ',len(listOfCentralValues2), ' ', len(listOfStatErrors2))
    else :
        print( ' added ',len(listOfCentralValues2), ' histograms for meth 2')

    listOfCentralValues = []
    listOfStatErrors    = []

    mult1 = int( len(listOfCentralValues2)/len(listOfCentralValues1) )
    mult2 = int( len(listOfCentralValues1)/len(listOfCentralValues2) )
    if mult1<1 :
        mult1 = 1
    if mult2<1 :
        mult2 = 1
    listOfCentralValues.extend( listOfCentralValues1*mult1 )
    listOfStatErrors   .extend( listOfStatErrors1   *mult1 )
    listOfCentralValues.extend( listOfCentralValues2*mult2 )
    listOfStatErrors   .extend( listOfStatErrors2   *mult2 )
    print( ' Weighting histograms by ', mult1, ' ', mult2)

    # get the binning from the first one
    xbins,ybins = getArray_binning( listOfCentralValues[0] )

    # we need to cut some of the variations that are invalid
    cutoff_SF = 1.2
    cutoff_stat = .5
    cutoff_SF = 99999.
    cutoff_stat = 1.5

    # now getting central values and stuff
    allSFs  = [] #np.zeros( (xbins.size*ybins.size,0) )
    allStat = [] #np.zeros( (xbins.size*ybins.size,0) )

    for x in range(0,xbins.size) :
        for y in range(0,ybins.size) :
            var_val = []
            var_err = []
            for cv in range(0,len(listOfCentralValues)) :
                val     = listOfCentralValues[cv].GetBinContent(x+1,y+1)
                err     = listOfStatErrors   [cv].GetBinContent(x+1,y+1)
                var_val.append( val )
                var_err.append( err )

            allSFs .append( np.array( var_val ))
            allStat.append( np.array( var_err ))


    SF   = np.zeros( len(allSFs ) )
    stat = np.zeros( len(allStat) )
    cov  = np.zeros( (len(allSFs),len(allSFs)) )
    for i in range(0,len(allSFs)) :
        mean = 0.
        mean_stat = 0.
        counter = 0.
        for var in range(0,len(allSFs[i])) :
            val = allSFs [i][var]
            err = allStat[i][var]
#            if i%len(ybins) == 3 : 
#                print( ' bins i j ', i , ' val ', val, ' err ', err)
            if val>0. and abs(val-1)<cutoff_SF and err<cutoff_stat : # skip a bunch of SF variations 
                mean += val
                mean_stat += err
                counter += 1
#                if i==185 :
#                    print( ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' adding mean ', val, ' stat ', err)
#                    print( ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' mean ', mean, ' stat ', mean_stat, ' counter ', counter)
            elif mean != 0 :
                print( ' skipping ', val, ' +- ', err, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]))
        if counter > 0. :
            SF  [i] = mean/counter
            stat[i] = mean_stat/counter
    for i in range(0,len(allSFs)) :
        for j in range(0,len(allSFs)) :
            corr = 0.
            counter = 0.
            for var in range(0,len(allSFs[i])) :
                vali = allSFs [i][var]
                valj = allSFs [j][var]
                erri = allStat[i][var]
                errj = allStat[j][var]
                if vali>0. and valj>0. and abs(vali-1)<cutoff_SF and erri<cutoff_stat and abs(valj-1)<cutoff_SF and errj<cutoff_stat : # skip a bunch of SF variations 
                    corr += (vali-SF[i])*(valj-SF[j])
                    counter += 1.
#                elif vali != 0 and valj != 0 and erri<99999999999. and errj<99999999999. :
#                    print( ' skipping 2 ', vali, ' +- ', erri, '  ', valj, ' +- ', errj, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]), ' in bins ', xbins[int(j/len(ybins))], ' / ', ('%1.2f' % ybins[j%len(ybins)]))
            if counter > 0. :
                cov  [i,j] = corr/counter
#            else :
#                print( ' problem counting cov in bins ' ,i, ' ',j, ' ', counter)

    SF, stat, cov, xbins, ybins = stripFromArrayX( pTmin, SF, stat, cov, xbins, ybins )
    SF, stat, cov, xbins, ybins = stripFromArrayX_max( pTmax, SF, stat, cov, xbins, ybins )

    #--------------------------------------
    # let's run an eigenvalue decomposition
    #--------------------------------------
    val_eig,vec_eig = np.linalg.eigh(cov)

    # remove complex entries, in general all this can be complex but for positive definite matrix is real
    val_eig = np.flipud(val_eig)
    vec_eig = np.fliplr(vec_eig)
    for iv in range(0,len(val_eig)) : # if there are neg eigenvalues, set them to 0
        if val_eig[iv] < 0. :
            val_eig[iv] = 0

    eigsum = np.sum(val_eig)
    if eigsum == 0 :
        print( ' all eigenvalues are zero so the syst uncertainty must be zero, too ')
        return SF,stat,np.zeros(stat.shape),xbins,ybins
    
    # eigenvalues are already sorted by size, let's find the largest 95%
    counteig = 0
    currentsum = 0
    for eig in val_eig :

        currentsum += eig
        counteig += 1
        if currentsum/np.sum(val_eig) > 0.99 :
            break

    print( ' number of eig ', counteig, val_eig[:counteig])
    # the gamma matrix contains the sqrt( eigenvalue ) * eigenvectors
    # --> these are the uncorrelated sources of uncertainty
    gamma = vec_eig[:,:counteig].dot( np.diag( np.sqrt( val_eig[:counteig] ) ) )
    rest  = vec_eig[:,counteig:].dot( np.diag( np.sqrt( val_eig[counteig:] ) ) )
    rest  = np.sqrt( np.sum( rest**2, axis=1 ) )

    # copy the syst and ignore for now the uncorr
    syst = gamma
    # add the uncorr to stats
    stat = np.sqrt( np.sum( np.column_stack( (stat**2,rest**2) ), axis=1 ) )

    infile1.Close()
    infile2.Close()
    return SF,stat,syst,xbins,ybins


def getArray_binning( histogram ) : 

    # returns arrays with bin boundaries in x and y, excluding the upper edge of the last bin
    if not histogram :
        print( ' in rootToArray:  no histogram ',histogram)
        return 0
    
    nxbins = histogram.GetXaxis().GetNbins()
    nybins = histogram.GetYaxis().GetNbins()

    xbins = np.array( [histogram.GetXaxis().GetBinLowEdge(x+1) for x in range(0,nxbins)] ) # I don't want the upper edge for now
    ybins = np.array( [histogram.GetYaxis().GetBinLowEdge(y+1) for y in range(0,nybins)] )

    return xbins,ybins

def stripFromArrayX( threshold, SF, stat, cov, xbins, ybins ) :

    # not all our methods cover the full pT range, so let's strip some of the zeros
    bin = 0
    for x in xbins :
        if x<threshold :
            bin = bin+1

    return SF[(bin*ybins.size):],stat[(bin*ybins.size):],cov[(bin*ybins.size):,(bin*ybins.size):],xbins[bin:],ybins

def stripFromArrayX_max( threshold, SF, stat, cov, xbins, ybins ) :

    # not all our methods cover the full pT range, so let's strip some of the zeros
    bin = 0
    for x in xbins :
        if x<threshold :
            bin = bin+1

    return SF[:(bin*ybins.size)],stat[:(bin*ybins.size)],cov[:(bin*ybins.size),:(bin*ybins.size)],xbins[:bin],ybins
