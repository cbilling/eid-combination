#!/usr/bin/env python

import numpy as np
import sys


def consistency(arr1, arr2):
    return np.array_equal(arr1, arr2)


def decorrelate_syst_labels(arr_in1, arr_in2, exceptions=[]):
    # we'll often feed two arrays where columns are treated correlated between them
    # in this function we'll fill blanks in both arrays such that no column is filled in both arrays
    # if we want to treat columns correlated, we'll specify their number in exceptions

    len_arr1 = len(arr_in1)
    len_arr2 = len(arr_in2)

    newcolumns = 0

    arr_out1 = np.copy(arr_in1)
    arr_out2 = np.copy(arr_in2)

    for c in range(
        0, min(len_arr1, len_arr2)
    ):  # if one array is longer we don't need to decorrelate the columns
        # print( ' looping column ', c, ' of ', min(len_arr1,len_arr2), ' with newcol ', newcolumns)
        if (c + newcolumns) in exceptions:
            # print( ' --> column ', c , ' is an execption')
            continue
        arr_out1 = np.insert(arr_out1, min(len_arr1, c) + newcolumns, 0)
        arr_out2 = np.insert(arr_out2, min(len_arr2, c) + newcolumns + 1, 0)
        #        print( ' *** new column! inserting ', min(len_arr1,c)+newcolumns, ' and ', min(len_arr2,c)+newcolumns+1)
        newcolumns = newcolumns + 1

    len_arr1 = len(arr_out1)
    len_arr2 = len(arr_out2)

    for c in range(
        min(len_arr1, len_arr2), max(len_arr1, len_arr2)
    ):  # now fill some more blanks so both arrays will have the same size
        if len_arr1 > len_arr2:
            arr_out2 = np.insert(arr_out2, len_arr2, 0)
        elif len_arr1 < len_arr2:
            arr_out1 = np.insert(arr_out1, len_arr1, 0)
    #        print( ' !!! filling blanks ', c)

    return arr_out1, arr_out2


def decorrelate(arr_in1, arr_in2, exceptions=[]):
    # we'll often feed two arrays where columns are treated correlated between them
    # in this function we'll fill blanks in both arrays such that no column is filled in both arrays
    # if we want to treat columns correlated, we'll specify their number in exceptions

    len_arr1 = arr_in1.shape[1]
    len_arr2 = arr_in2.shape[1]

    newcolumns = 0

    arr_out1 = np.copy(arr_in1)
    arr_out2 = np.copy(arr_in2)

    for c in range(
        0, min(len_arr1, len_arr2)
    ):  # if one array is longer we don't need to decorrelate the columns
        # print( ' looping column ', c, ' of ', min(len_arr1,len_arr2), ' with newcol ', newcolumns)
        if (c + newcolumns) in exceptions:
            # print( ' --> column ', c , ' is an execption')
            continue

        arr_out1 = np.insert(arr_out1, min(len_arr1, c) + newcolumns, 0, axis=1)
        arr_out2 = np.insert(arr_out2, min(len_arr2, c) + newcolumns + 1, 0, axis=1)
        #        print( ' *** new column! inserting ', min(len_arr1,c)+newcolumns, ' and ', min(len_arr2,c)+newcolumns+1)
        newcolumns = newcolumns + 1

    len_arr1 = arr_out1.shape[1]
    len_arr2 = arr_out2.shape[1]

    for c in range(
        min(len_arr1, len_arr2), max(len_arr1, len_arr2)
    ):  # now fill some more blanks so both arrays will have the same size
        if len_arr1 > len_arr2:
            arr_out2 = np.insert(arr_out2, len_arr2, 0, axis=1)
        elif len_arr1 < len_arr2:
            arr_out1 = np.insert(arr_out1, len_arr1, 0, axis=1)
    #        print( ' !!! filling blanks ', c)

    return arr_out1, arr_out2


def get_covariance(arr_unc):
    # usually we work with arrays, where source of uncertainties are stored in columns and are fully correlated between rows
    # we can of course calculate the covariance matrix from this
    # --> the output is a covariance matrix where both the rows and the columns are bins of the measurement!
    len = arr_unc.shape[0]
    matrix = np.zeros((len, len), dtype=np.float64)

    for i in range(0, len):
        for j in range(0, len):
            matrix[i, j] = np.sum(np.multiply(arr_unc[i], arr_unc[j]))
    return matrix


def get_correlation(arr_unc):
    # the correlation coefficients, i.e. covar(i,j)/var(i)/var(j)
    covar = get_covariance(arr_unc)
    corre = covar.copy()
    print("Jere")
    for i in range(0, corre.shape[0]):
        for j in range(0, corre.shape[0]):
            #print(i,j,covar[i,i],covar[j,j])
            if (covar[i,i]==0.0 or covar[j,j]==0.0):
                corre[i, j] = 0
            else:
                corre[i, j] = covar[i, j] / np.sqrt(covar[i, i] * covar[j, j])

    return corre


def get_variance(arr_unc):
    # usually we work with arrays, where source of uncertainties are stored in columns and are fully correlated between rows
    # sometimes it's helpful to get just the variance (i.e. quadratic sum of columns)
    return np.sum(arr_unc**2, axis=1)


def get_standarddev(arr_unc):
    # same as variance, but holding the standard deviation
    return np.sqrt(get_variance(arr_unc))


def stripBins(SF, stat, syst, x, y, ptmin=0, ptmax=-1, etamin=0, etamax=-1):
    if ptmax < 0:
        ptmax = len(x)
    if etamax < 0:
        etamax = len(y)

    #    print( ' PHILIP OLD before ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)
    SF = SF.reshape((len(x), len(y)))
    stat = stat.reshape((len(x), len(y)))
    syst = syst.reshape((len(x), len(y), syst.shape[1]))
    #    print( ' PHILIP OLD after ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)

    SF = SF[ptmin:ptmax, etamin:etamax]
    stat = stat[ptmin:ptmax, etamin:etamax]
    syst = syst[ptmin:ptmax, etamin:etamax, :]
    x = x[ptmin:ptmax]
    y = y[etamin:etamax]

    #    print( ' PHILIP NEW before ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)
    SF = SF.reshape((len(x) * len(y)))
    stat = stat.reshape((len(x) * len(y)))
    syst = syst.reshape((len(x) * len(y), syst.shape[2]))
    #    print( ' PHILIP NEW after ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)

    return SF, stat, syst, x, y


def stripBinsTest(SF, stat, syst, x, y, ptmin=0, ptmax=-1, etamin=0, etamax=-1):
    if ptmax < 0:
        ptmax = len(x)
    if etamax < 0:
        etamax = len(y)

    #    print( ' PHILIP OLD before ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)
    SF = SF.reshape((len(x), len(y)))
    stat = stat.reshape((len(x), len(y)))
    syst = syst.reshape((len(x), len(y), syst.shape[1]))
    #    print( ' PHILIP OLD after ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)

    SF = SF[ptmin:ptmax, etamin:etamax]
    stat = stat[ptmin:ptmax, etamin:etamax]
    syst_statstriplow = syst[ptmin:ptmax, etamin:etamax, : (ptmin * len(y))]
    syst_statretain = syst[
        ptmin:ptmax, etamin:etamax, (ptmin * len(y)) : (ptmax * len(y))
    ]
    syst_statstriphigh = syst[
        ptmin:ptmax, etamin:etamax, (ptmax * len(y)) : (len(x) * len(y))
    ]
    syst_syst = syst[ptmin:ptmax, etamin:etamax, (len(x) * len(y)) :]
    syst = np.concatenate((syst_statretain, syst_syst), axis=2)
    print(
        " PHILIP shapes SF ",
        SF.shape,
        " stat ",
        stat.shape,
        " syst_statstriplow ",
        syst_statstriplow.shape,
        " syst_statretain ",
        syst_statretain.shape,
        " syst_statstriphigh ",
        syst_statstriphigh.shape,
        " syst_syst ",
        syst_syst.shape,
        " syst ",
        syst.shape,
    )
    x = x[ptmin:ptmax]
    y = y[etamin:etamax]

    #    print( ' PHILIP NEW before ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)
    SF = SF.reshape((len(x) * len(y)))
    stat = stat.reshape((len(x) * len(y)))
    syst = syst.reshape((len(x) * len(y), syst.shape[2]))
    #    print( ' PHILIP NEW after ', SF  .shape,    stat.shape,    syst.shape, x.shape, y.shape)

    return SF, stat, syst, x, y


def addBlanks(SF, stat, syst, x, y):
    SF = np.append(SF, np.zeros(len(y)))
    stat = np.append(stat, np.zeros(len(y)))
    syst = np.concatenate((syst, np.zeros((len(y), syst.shape[1]))))
    x = np.append(x, 250000.0)
    y = y
    return SF, stat, syst, x, y


def get_chi2(average, averror, SFs, stats, systs):
    # calculate the chi2 with respect to the average
    # the stat per bin will be decorrelated by the function
    # the syst requires already a decorrelated array

    SF_all = np.array(SFs)  # it's a 2-d array
    errors = np.array([])
    for imeth in range(len(SFs)):
        ierr = np.array([])
        for jmeth in range(len(SFs)):
            istat = np.diag(stats[imeth])
            if imeth != jmeth:
                istat = np.zeros(istat.shape)
            ierr = np.column_stack((ierr, istat)) if ierr.size else istat
        ierr = np.column_stack((ierr, systs[imeth]))
        #        print( ' PHILIP concatenating ', errors.shape, ' ', ierr.shape)
        errors = np.concatenate((errors, ierr), axis=0) if errors.size else ierr

    cov_all = get_covariance(
        errors
    )  # but the cov matrix is (2*nbins)*(2*nbins) always! we have to be careful later

    invcov_all = 0
    #    if np.linalg.cond(cov_all)<1/sys.float_info.epsilon :
    #        invcov_all = np.linalg.inv( cov_all )
    #    else :
    #        print( ' problem inverting matrix ',np.linalg.cond(cov_all),'<',1/sys.float_info.epsilon)
    #        exit()

    invcov_all = np.linalg.pinv(cov_all, hermitian=True)
    #    np.savetxt( 'debuginvcov.txt' , (invcov_all), fmt='%1.2f' )

    chi2 = 0
    for i in range(0, SF_all.shape[0]):
        for j in range(0, SF_all.shape[0]):
            if i != j:
                continue
            shape = SF_all.shape[1]
            chi2 = chi2 + (SF_all[i] - average).T.dot(
                invcov_all[i * shape : (i + 1) * shape, j * shape : (j + 1) * shape]
            ).dot(SF_all[j] - average)

    return chi2


def getAverage_BLUE(SFs, stats, systs):
    print(SFs.shape)
    N_meas = SFs.shape[0]
    N_entries = SFs.shape[1]
    N_syst = systs.shape[2] if systs.ndim == 3 else 0

    # Output arrays
    SFav = np.zeros(N_entries)
    statav = np.zeros(N_entries)
    systav = np.zeros((N_entries, N_syst))

    for ientry in range(N_entries):
        # Extract the values for this entry from all measurements
        SFset = SFs[:, ientry]
        statset = stats[:, ientry]
        systset = systs[:, ientry, :] if N_syst > 0 else np.zeros((N_meas, 0))

        # Handle zeros: if any measurement is zero, we fall back to nonzero ones
        # If all are zero, the result is zero.
        nonzero_indices = np.where(SFset != 0)[0]
        if len(nonzero_indices) == 0:
            # All zero: just set everything to zero
            SFav[ientry] = 0.0
            statav[ientry] = 0.0
            if N_syst > 0:
                systav[ientry, :] = 0.0
            continue
        elif len(nonzero_indices) == 1:
            # Only one nonzero measurement: just take that one
            idx = nonzero_indices[0]
            SFav[ientry] = SFset[idx]
            statav[ientry] = statset[idx]
            if N_syst > 0:
                systav[ientry, :] = systset[idx, :]
            continue
        # If multiple nonzero measurements, we proceed with BLUE

        # Build the input array for covariance calculation
        # The array should have shape (N_meas, N_meas + N_syst)
        # First N_meas columns: each row has stat error in the diagonal position, 0 elsewhere
        # Next N_syst columns: syst errors for that measurement
        input_array = np.zeros((N_meas, N_meas + N_syst))

        # Fill in the stat errors diagonally in the first N_meas columns
        for im in range(N_meas):
            input_array[im, im] = statset[im]

        # Fill in the systematic uncertainties
        if N_syst > 0:
            input_array[:, N_meas:] = systset

        # Compute covariance
        cov = get_covariance(input_array)
        invcov = np.linalg.pinv(cov, hermitian=True)

        # Compute BLUE weights
        # weights_i = (sum of column i of invcov) / (sum of all elements in invcov)
        # This generalizes the l0, l1 formula to N measurements.
        col_sums = np.sum(invcov, axis=0)
        denom = np.sum(invcov)
        weights = col_sums / denom

        # Weighted average for SF and stat
        SFmean = weights.dot(SFset)
        statmean = weights.dot(statset)

        SFav[ientry] = SFmean
        statav[ientry] = statmean

        # For systematics, we do a weighted average similarly
        for ivar in range(N_syst):
            # syst array shape: (N_meas, N_syst)
            # Weighted average: sum(invcov.dot(column)) / sum(invcov)
            # Extract the column for this systematic variable
            syst_values = systset[:, ivar]
            if not np.any(syst_values):
                # If all zero, just continue with zero
                systav[ientry, ivar] = 0.0
                continue

            systav[ientry, ivar] = (invcov.dot(syst_values).sum()) / denom

    return SFav, statav, systav


# def get_chi2( average, averror, SF_meth1, stat_meth1, syst_meth1, SF_meth2, stat_meth2, syst_meth2 ) :
#
#    # use the above function but pass individual methods
#    SFs   = [SF_meth1,SF_meth2]
#    stats = [stat_meth1,stat_meth2]
#    systs = [syst_meth1,syst_meth2]
#    return get_chi2( average, averror, SFs, stats, systs )
