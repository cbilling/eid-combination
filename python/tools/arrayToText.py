#!/usr/bin/env python

import os
import numpy as np

def writeArray( path, array, xbins, ybins ) :

    # ------------------------------------------------------------------------------
    # - create a new folder under path
    # - write a text file with binning information
    # - write a text file with the array
    # ------------------------------------------------------------------------------
    
    ensure_directory( path )
    np.savetxt( path + '/SF_stat_syst.txt' , (array), fmt='%+1.9f' )

    np.savetxt( path + '/binningX.txt' , (xbins.T), fmt='%1.2f' )
    np.savetxt( path + '/binningY.txt' , (ybins.T), fmt='%1.2f' )


def readArray( path ) :

    # ------------------------------------------------------------------------------
    # the opposite from above! we'll read the files created above back in
    # ------------------------------------------------------------------------------
    
    array = np.loadtxt( path + '/SF_stat_syst.txt' )

    xbins = np.atleast_1d(np.loadtxt( path + '/binningX.txt' ))
    ybins = np.atleast_1d(np.loadtxt( path + '/binningY.txt' ))
    
    return array, xbins, ybins


def writeArray_SF_stat_syst( path, SF, stat, syst, xbins, ybins ) :
    arr_out = np.column_stack((SF,stat,syst))
    return writeArray( path, arr_out, xbins, ybins )


def readArray_SF_stat_syst( array, xbins, ybins ) :

    return array[:,0],array[:,1],array[:,2:],xbins,ybins


def readArray_SF_stat_syst_leg( path ) :

    array, xbins, ybins = readArray( path )
    return array[:,0],array[:,1],array[:,2:],xbins,ybins

# Make output directory if it does not exists
def ensure_directory( path ) :

    if not os.path.isdir( path ) :
        os.makedirs( path )

