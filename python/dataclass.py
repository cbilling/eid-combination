from dataclasses import dataclass
from fileinput import filename
import numpy as np
import pandas as pd
import sys
from .tools import (
    getArray_SF_stat_syst,
    stripFromArrayX,
    makeEtaAbs,
    check_SF_stat_syst,
    getArray_SF_stat_syst_refactored,
)
import ROOT
from .logger import logger
import os
import matplotlib.pyplot as plt
from .tools.statTools import get_standarddev, getAverage_BLUE
import re
from seaborn import heatmap
from pandas import pivot_table
import matplotlib.pyplot as plt
from matplotlib import rc_context


# should convert syst to dataframe so we can keep track of column names (syst names)
# and we would be able to index by pt and eta bins

log_plot_dir = os.environ["LOG_PLOT_DIR"]


@dataclass
class MethodSF:
    sf: np.ndarray = np.zeros((1, 1))
    stat: np.ndarray = np.zeros((1, 1))
    syst: np.ndarray = np.zeros((1, 1))
    x_bins: np.ndarray = np.zeros((1, 1))
    y_bins: np.ndarray = np.zeros((1, 1))
    eta_bins: np.ndarray = np.zeros((1, 1))
    pt_bins: np.ndarray = np.zeros((1, 1))
    prebinned: bool = False
    energy_units: str = "GeV"
    data_path: str = ""
    working_point: str = "TightLLH_d0z0"
    last_eta_bound: float = 2.47
    last_pt_bound: float = 250000
    inverted: bool = False
    year: int = 1998
    iso: str = "NonIso"
    include_wp_names: bool = False

    def __post_init__(self):
        self.syst_labels = []
        if self.prebinned:
            self.read_data(removeNeg="yes")
        else:
            self.read_data()
        self.eta_bins = self.x_bins
        self.pt_bins = self.y_bins

    def stacked_array(self, last_nrows=0):
        print(self.syst.shape)
        arr = arr = np.column_stack((self.sf, self.stat))
        if self.syst.shape[0] > 0:
            arr = np.column_stack((self.sf, self.stat, self.syst))
        if last_nrows > 0:
            arr = np.column_stack(
                (
                    self.sf[-last_nrows:],
                    self.stat[-last_nrows:],
                    self.syst[-last_nrows:, :],
                )
            )
        return arr

    def get_totunc(self, ptBin=None, etaBin=None, pt=None, eta=None):
        x = list(self.x_bins)
        x.append(self.last_pt_bound)
        y = list(self.y_bins)
        y.append(self.last_eta_bound)

        # Convert to bin index if pt or eta is provided
        if ptBin is None:
            for i in range(len(self.x_bins)):
                if pt == self.x_bins[i]:
                    ptBin = i
                    break
        if etaBin is None:
            for i in range(len(self.y_bins)):
                if eta == self.y_bins[i]:
                    etaBin = i
                    break
        if ptBin is None and etaBin is None:
            print("Non existing pt or eta provided, returning float('nan')")
            arr = np.empty(
                (
                    len(self.x_bins),
                    len(
                        self.y_bins,
                    ),
                )
            )
            arr[:] = np.nan
            if pt is not None and eta is not None:
                arr = np.array([np.nan])
            if pt is not None:
                arr = arr[0, :]
            if eta is not None:
                arr = arr[:, 0]
            return arr, tuple(x), tuple(y)
        # Reshape the scale factor array
        arr = np.column_stack((self.stat, self.syst))
        arr = get_standarddev(arr)
        arr = arr.reshape(len(self.x_bins), len(self.y_bins))

        # Ensure either ptBin or etaBin is provided
        if ptBin is None and etaBin is None:
            raise ValueError("Either ptBin or etaBin must be provided.")
        elif ptBin is not None and etaBin is not None:
            # Handle the case where both ptBin and etaBin are provided
            arr = arr[ptBin, etaBin]
            x = (x[ptBin], x[ptBin + 1])
            y = (y[etaBin], y[etaBin + 1])
        elif etaBin is not None:
            # If etaBin is provided, slice the scale factor accordingly
            arr = arr[:, etaBin]
            y = (y[etaBin], y[etaBin + 1])
        elif ptBin is not None:
            # If ptBin is provided, slice the scale factor accordingly
            arr = arr[ptBin, :]
            x = (x[ptBin], x[ptBin + 1])

        return np.array(arr), tuple(x), tuple(y)

    def prepend(self, method):
        print(len(method.sf), len(self.sf))
        self.x_bins = method.x_bins
        self.y_bins = method.y_bins
        self.sf = np.concatenate((method.sf[: -len(self.sf)], self.sf))
        self.stat = np.concatenate((method.stat[: -len(self.stat)], self.stat))
        self.syst = np.concatenate((method.syst[: -len(self.syst), :], self.syst))
        print(len(self.sf))

    def getSF(self, ptBin=None, etaBin=None, pt=None, eta=None):
        x = list(self.x_bins)
        x.append(self.last_pt_bound)
        y = list(self.y_bins)
        y.append(self.last_eta_bound)
        # Convert to bin index if pt or eta is provided
        if ptBin is None:
            for i in range(len(self.x_bins)):
                if pt == self.x_bins[i]:
                    ptBin = i
                    break
        if etaBin is None:
            for i in range(len(self.y_bins)):
                if eta == self.y_bins[i]:
                    etaBin = i
                    break
        if ptBin is None and etaBin is None:
            print("Non existing pt or eta provided, returning float('nan')")
            arr = np.empty(
                (
                    len(self.x_bins),
                    len(
                        self.y_bins,
                    ),
                )
            )
            arr[:] = np.nan
            if pt is not None and eta is not None:
                arr = np.array([np.nan])
            if pt is not None:
                arr = arr[0, :]
            if eta is not None:
                arr = arr[:, 0]
            return arr, tuple(x), tuple(y)
        # Reshape the scale factor array
        sf = self.sf.reshape(len(self.x_bins), len(self.y_bins))

        # Ensure either ptBin or etaBin is provided
        if ptBin is None and etaBin is None:
            raise ValueError("Either ptBin or etaBin must be provided.")
        elif ptBin is not None and etaBin is not None:
            # Handle the case where both ptBin and etaBin are provided
            sf = sf[ptBin, etaBin]
            x = (x[ptBin], x[ptBin + 1])
            y = (y[etaBin], y[etaBin + 1])
        elif etaBin is not None:
            # If etaBin is provided, slice the scale factor accordingly
            sf = sf[:, etaBin]
            y = (y[etaBin], y[etaBin + 1])

        elif ptBin is not None:
            # If ptBin is provided, slice the scale factor accordingly
            sf = sf[ptBin, :]
            x = (x[ptBin], x[ptBin + 1])

        return sf, tuple(x), tuple(y)

    def print_shapes(self):

        print(
            f"Length of SF's {self.sf.shape[0]}, Number of systematics {self.syst.shape[1]}, Number of x (pT) bins {self.x_bins.shape[0]}, Number of y (eta) bins {self.y_bins.shape[0]}"
        )

    def read_data(self, removeNeg="yes"):
        pass

    def convertGeVtoMeV(self):

        # Would be good to check what the units are before you do the computation somehow
        if self.energy_units == "GeV":
            scalefactor = 1000
            self.x_bins *= scalefactor
            self.energy_units = "MeV"
        else:
            print("already in MeV")

    def converteVtoMev(self):
        scalefactor = 1e4
        self.x_bins /= scalefactor
        self.energy_units = "MeV"

    def to_df(self):
        # print(self.y_bins,self.x_bins)
        y_index = list(self.y_bins) * len(self.x_bins)
        x_index = []
        for x in self.x_bins:
            for i in range(len(self.y_bins)):
                x_index.append(x)
        # print(self.stacked_array())
        data = np.column_stack((x_index, y_index, self.stacked_array()))
        labels = [str(i) for i in self.syst_labels]
        print("Length of stat", len(self.stat))
        cols = [
            "Energy [GeV]",
            "eta",
            "SF",
            "Stat",
        ] + labels
        # print(cols)
        # print(data)
        df = pd.DataFrame(data)  # , columns=cols)
        df.columns = cols
        df.set_index(["Energy [GeV]", "eta"], inplace=True)
        return df

    def log_syst(self, step_name, obj_name):
        os.makedirs(f"{log_plot_dir}/{step_name}", exist_ok=True)
        # Plot scalefactors

        fig, ax1 = plt.subplots(figsize=(20, 40))

        df = self.to_df()
        print(df.head())
        print(df.columns)
        df.drop(labels=["SF", "Stat"], inplace=True, axis=1)
        ax1 = heatmap(
            df,
            annot=True,
            cmap="Spectral",
            # vmin=np.min(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
            # vmax=np.max(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
            # fmt=(".2f"),
            annot_kws=({"fontsize": "x-small"}),
            ax=ax1,
        )
        ax1.invert_yaxis()
        ax1.set_title("Systs")
        # ax1.imshow(self.syst)
        # print(np.arange(0, self.syst.shape[1]))
        # print(len(self.syst_labels), self.syst_labels)
        # ax1.set_xticks(np.arange(0, self.syst.shape[1]))
        # ax1.set_xticklabels(self.syst_labels, rotation=90)
        # ax1.set_yticks(np.arange(-0.5, self.syst.shape[0], 5))
        # ax1.tick_params(axis="both", which="major", labelsize=20)
        # ax1.tick_params(axis="both", which="minor", labelsize=0)
        # ax1.tick_params(which="both", direction="out")
        plt.savefig(f"{log_plot_dir}/{step_name}/{obj_name}_syst.pdf")
        plt.close()

    def log(self, step_name, obj_name):
        os.makedirs(f"{log_plot_dir}/{step_name}", exist_ok=True)

        # Set local matplotlib parameters
        rc_params = {"font.family": "Nimbus Sans", "font.size": 12}

        with rc_context(rc_params):

            # Create a 2D heatmap plot from the pandas dataframe
            fig, (ax1, ax2) = plt.subplots(
                ncols=2, figsize=(10, 6), dpi=100, layout="tight"
            )
            df = self.to_df()
            heatmap_data = pivot_table(
                df,
                values="SF",
                index=["eta"],
                columns="Energy [GeV]",
            )
            ax1 = heatmap(
                heatmap_data,
                annot=True,
                cmap="Spectral",
                # vmin=np.min(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
                # vmax=np.max(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
                fmt=(".2g"),
                annot_kws=({"fontsize": "x-small"}),
                ax=ax1,
            )
            ax1.invert_yaxis()
            ax1.set_title("SF")

            heatmap_data = pivot_table(
                df,
                values="Stat",
                index=["eta"],
                columns="Energy [GeV]",
            )
            ax2 = heatmap(
                heatmap_data,
                annot=True,
                cmap="Spectral",
                # vmin=np.min(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
                # vmax=np.max(df[values].to_numpy()[np.isfinite(df[values].to_numpy())]),
                fmt=(".2g"),
                annot_kws=({"fontsize": "x-small"}),
                ax=ax2,
            )
            ax2.invert_yaxis()
            ax2.set_title("Stat")

            fig.savefig(f"{log_plot_dir}/{step_name}/{obj_name}_sf_stat.pdf")
            plt.close(fig)

    def log_legacy(self, step_name, obj_name):
        os.makedirs(f"{log_plot_dir}/{step_name}", exist_ok=True)

        fig, (ax1, ax2) = plt.subplots(1, 2)
        sf = self.sf.reshape((len(self.x_bins), -1))
        stat = self.stat.reshape((len(self.x_bins), -1))
        ax1.imshow(np.fliplr(sf.T))
        ax2.imshow(np.fliplr(stat.T))
        ax1.tick_params(axis="both", which="major", labelsize=6)
        ax1.tick_params(axis="both", which="minor", labelsize=0)
        ax2.tick_params(axis="both", which="major", labelsize=6)
        ax2.tick_params(axis="both", which="minor", labelsize=0)
        ax1.set_xticks(np.arange(-0.5, sf.shape[0], 1))
        ax2.set_xticks(np.arange(-0.5, stat.shape[0], 1))
        ax1.set_yticks(np.arange(-0.5, sf.shape[1], 1))
        ax2.set_yticks(np.arange(-0.5, stat.shape[1], 1))
        plt.savefig(f"{log_plot_dir}/{step_name}/{obj_name}_sf_stat.pdf")
        plt.close()

        # ax3.imshow(self.syst)
        # ax[0].imshow(np.array(self.sf.reshape((len(self.x_bins), -1)).T))
        # ax[1].imshow(self.stat.reshape((len(self.x_bins), -1)).T)
        # ax[2].imshow(self.syst)

    def rebinlowpt(self, pT_max=15000.0, rebinMethod="prebin"):

        # the input arrays
        SF, stat, syst, x, y = self.sf, self.stat, self.syst, self.x_bins, self.y_bins

        bin = 0
        for xbin in x:
            if xbin <= pT_max:
                bin = bin + 1

        SF_lo, stat_lo, syst_lo, x_lo, y_lo = (
            SF[: (bin * y.size)],
            stat[: (bin * y.size)],
            syst[: (bin * y.size)],
            x[:bin],
            y,
        )
        SF_hi, stat_hi, syst_hi, x_hi, y_hi = (
            SF[(bin * y.size) :],
            stat[(bin * y.size) :],
            syst[(bin * y.size) :],
            x[bin:],
            y,
        )
        Ziso_lo_rebin = []
        x_lo_rebin = []
        y_lo_rebin = []

        lo_method = MethodSF(SF_lo, stat_lo, syst_lo, x_lo, y_lo)

        hi_method = MethodSF(SF_hi, stat_hi, syst_hi, x_hi, y_hi)

        lo_method.syst_labels = self.syst_labels
        hi_method.syst_labels = self.syst_labels

        return (lo_method, hi_method)

    def rebinlowpt_BLUE(
        self, rebinMethod="prebin", pTbins=np.array([]), etabins=np.array([])
    ):

        pT_max = 15000.0

        # the input arrays
        SF, stat, syst, x, y = self.sf, self.stat, self.syst, self.x_bins, self.y_bins

        bin = 0
        for xbin in x:
            if xbin <= pT_max:
                bin = bin + 1

        SF_lo, stat_lo, syst_lo, x_lo, y_lo = (
            SF[: (bin * y.size)],
            stat[: (bin * y.size)],
            syst[: (bin * y.size)],
            x[:bin],
            y,
        )
        SF_hi, stat_hi, syst_hi, x_hi, y_hi = (
            SF[(bin * y.size) :],
            stat[(bin * y.size) :],
            syst[(bin * y.size) :],
            x[bin:],
            y,
        )
        Ziso_lo_rebin = []
        x_lo_rebin = []
        y_lo_rebin = []

        SF_new = np.zeros(len(pTbins) * len(etabins))
        stat_new = np.zeros(len(pTbins) * len(etabins))

        ## Just a trick if the systematics are empty
        num_syst = 1
        if syst_lo.ndim > 1:
            num_syst = syst_lo.shape[1]
        else:
            syst_lo = np.zeros_like(stat_lo)
            syst_lo = syst_lo[:, np.newaxis]

        syst_new = np.zeros([len(pTbins) * len(etabins), num_syst])

        xbin_new = 0
        ybin_new = len(etabins) - 1

        # determine the starting bin
        while pTbins[xbin_new] < x_lo[0]:
            xbin_new = xbin_new + 1

        SF_tmp = []
        stat_tmp = []
        syst_tmp = []

        for xbin in range(
            len(x_lo)
        ):  # pT has to go first because we're averaging over several eta bins
            for ybin in range(len(y_lo) - 1, -1, -1):

                if (
                    y_lo[ybin] < etabins[0]
                ):  # skip these cases because we're going to do an abs
                    continue

                # positive eta
                SF_tmp.append(SF_lo[xbin * len(y_lo) + ybin])
                stat_tmp.append(
                    stat_lo[xbin * len(y_lo) + ybin] * stat_lo[xbin * len(y_lo) + ybin]
                )
                syst_tmp.append(syst_lo[xbin * len(y_lo) + ybin, :])

                # negative eta
                SF_tmp.append(SF_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1])
                stat_tmp.append(
                    stat_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1]
                    * stat_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1]
                )
                syst_tmp.append(syst_lo[xbin * len(y_lo) + len(y_lo) - ybin - 1, :])

                # if the bin boundaries match, we'll start a new calculation
                if pTbins[xbin_new] == x_lo[xbin] and etabins[ybin_new] == y_lo[ybin]:
                    # if len(SF_tmp) == 2:
                    SF_tmp = np.array(SF_tmp)
                    stat_tmp = np.array(stat_tmp)
                    syst_tmp = np.array(syst_tmp)

                    #            # chi2 combination from https://cds.cern.ch/record/273386
                    # I GAVE UP ON THIS EVENTUALLY, SO DOESN'T WORK
                    #            cov = get_covariance( np.column_stack( (np.diag(stat_tmp),syst_tmp) ) )
                    #            from scipy.linalg import pinvh
                    #            invcov = pinvh( cov )
                    #
                    #            av = np.sum( invcov.dot(SF_tmp) ) / np.sum( invcov )
                    #            av_err = np.sqrt( 1./ np.sum( invcov ) )
                    #
                    #            av_stat = np.sum( invcov.dot(np.square(stat_tmp)) ) / np.sum( invcov ) / len(SF_tmp)
                    #            #            print ' shape ', np.square(syst_tmp[:,-1])
                    #            av_syst = np.sum( invcov.dot(np.square(syst_tmp)), axis=0 ) / np.sum( invcov ) / len(SF_tmp)
                    #            print 'stat ',av_stat
                    ##            print 'syst ',av_syst
                    #            #c_mat1 = np.tensordot(Q, a1, axes=([-1],[0]))
                    #            exit()

                    SF_tmp = np.array(SF_tmp, ndmin=2).T
                    stat_tmp = np.array(stat_tmp, ndmin=2).T
                    syst_tmp = np.array(syst_tmp, ndmin=3)
                    syst_tmp = np.swapaxes(syst_tmp, 0, 1)
                    print("Expected naive mean: ", np.mean(SF_tmp))
                    #            print SF_tmp.shape
                    #            print stat_tmp.shape
                    #            print syst_tmp.shape
                    average, averstat, aversyst = getAverage_BLUE(
                        SF_tmp, stat_tmp, syst_tmp
                    )
                    #            print average.shape
                    #            print averstat.shape
                    #            print aversyst.shape
                    print("BLUE mean: ", average)
                    print(averstat)
                    print(aversyst)
                    SF_new[xbin_new * len(etabins) + ybin_new] = average[0]
                    stat_new[xbin_new * len(etabins) + ybin_new] = averstat[0]
                    syst_new[xbin_new * len(etabins) + ybin_new] = aversyst[0].T

                    SF_tmp = []
                    stat_tmp = []
                    syst_tmp = []

                    ybin_new = ybin_new - 1

        rebin_method = MethodSF(SF_new, stat_new, syst_new, pTbins, etabins)
        lo_method = MethodSF(SF_lo, stat_lo, syst_lo, x_lo, y_lo)
        hi_method = MethodSF(SF_hi, stat_hi, syst_hi, x_hi, y_hi)
        rebin_method.syst_labels = self.syst_labels
        lo_method.syst_labels = self.syst_labels
        hi_method.syst_labels = self.syst_labels
        return (rebin_method, lo_method, hi_method)

    def decorrelateStat(self, method):
        SF_meth1, stat_meth1, syst_meth1 = self.sf, self.stat, self.syst
        SF_meth2, stat_meth2, syst_meth2 = method.sf, method.stat, method.syst

        consistent = np.allclose(self.x_bins, method.x_bins) and np.allclose(
            self.y_bins, method.y_bins
        )
        if not consistent:
            print("Methods are different shapes when trying to decorrelate statistics")
            raise TypeError

        SF_out = np.copy(SF_meth1)
        uncorr_out = np.zeros(SF_meth1.shape)
        corr_out = np.zeros(SF_meth1.shape)  # the syst here is the correlated stat

        for bin in range(0, len(SF_out)):
            stat_ref_rel = stat_meth1[bin] / SF_meth1[bin]
            uncorr_new_rel = stat_meth2[bin] / SF_meth2[bin]
            corr_new_rel = syst_meth2[bin] / SF_meth2[bin]
            
            rescale = stat_ref_rel / np.sqrt(uncorr_new_rel**2 + corr_new_rel**2)
            if uncorr_new_rel == 0.0 or np.isnan(uncorr_new_rel):
                rescale = 1.0
                stat_ref_rel = 0.0
                uncorr_new_rel = 0.0
                corr_new_rel = 0.0
                SF_out[bin] = 0.0
                syst_meth1[bin, :] = np.zeros(syst_meth1.shape[1])
            uncorr_out[bin] = uncorr_new_rel * rescale * SF_meth1[bin]
            corr_out[bin] = corr_new_rel * rescale * SF_meth1[bin]

        corr_out = np.diag(corr_out)

        self.sf = SF_out
        self.stat = uncorr_out
        self.syst = np.column_stack((corr_out, syst_meth1))

        decorr_labels = ["decorr_stat_" + str(i) for i in range(len(corr_out))]
        decorr_labels.extend(self.syst_labels)
        self.syst_labels = decorr_labels

    def undecorrelateStat(self, nCorr=-1):

        nStat = nCorr
        if nCorr < 0:
            nStat = self.sf.shape[0]
        SF_out = np.copy(self.sf)
        stat_out = np.sqrt(
            np.copy(self.stat) ** 2 + np.sum(self.syst[:, :nStat] ** 2, axis=1)
        )
        syst_out = np.copy(self.syst[:, nStat:])
        self.syst_labels = self.syst_labels[nStat:]
        self.sf, self.stat, self.syst = SF_out, stat_out, syst_out

    def removeDummyRows(self):
        # for Zcomb we have dummy rows of 0 to be able to combine with Jpsi
        self.sf = self.sf[self.sf != 0]
        self.stat = self.stat[self.stat != 0]
        self.syst = self.syst[~np.all(self.syst == 0, axis=1)]

    def addDummyRows(self, rows=0):

        self.sf = np.append(np.zeros((rows,)), self.sf)
        print(self.sf)
        self.stat = np.append(np.zeros((rows,)), self.stat)
        print(self.syst.shape)
        self.syst = np.vstack((np.zeros((rows, self.syst.shape[1])), self.syst))
        print(self.syst)

    def copy(self, method):
        self.sf = method.sf
        self.stat = method.stat
        self.syst = method.syst
        self.x_bins = method.x_bins
        self.y_bins = method.y_bins
        self.eta_bins = method.eta_bins
        self.pt_bins = method.pt_bins
        self.prebinned = method.prebinned
        self.energy_units = method.energy_units
        self.data_path = method.data_path
        self.working_point = method.working_point
        self.last_eta_bound = method.last_eta_bound
        self.last_pt_bound = method.last_pt_bound
        self.syst_labels = method.syst_labels
        self.year = method.year
        self.iso = method.iso

    def copy_binning(self, method):
        self.x_bins = method.x_bins
        self.y_bins = method.y_bins
        self.eta_bins = method.eta_bins
        self.pt_bins = method.pt_bins
        self.prebinned = method.prebinned
        self.energy_units = method.energy_units
        self.last_eta_bound = method.last_eta_bound
        self.last_pt_bound = method.last_pt_bound

    def invert(self):
        ########### LEGACY #############
        # for ientry in range(0, self.SF.shape[0]):

        #     oldSF = self.SF[ientry]

        #     self.SF[ientry] = 1.0 / oldSF
        #     self.stat[ientry] = 1.0 / oldSF / oldSF * self.stat[ientry]
        #     for isys in range(0, self.syst.shape[1]):
        #         self.syst[ientry, isys] = 1.0 / oldSF / oldSF * self.syst[ientry, isys]

        # Calculate the reciprocal of all scaling factors
        reciprocal_SF = 1.0 / self.SF

        # Update the scaling factors with their reciprocals
        self.SF = reciprocal_SF

        # Update statistical uncertainties by multiplying with the square of the reciprocals
        self.stat *= reciprocal_SF**2

        # Update systematic uncertainties by multiplying each systematic by the square of the reciprocals
        self.syst *= reciprocal_SF[:, np.newaxis] ** 2

        self.inverted = True

    def addFastSim(self, method2):

        left_df, right_df = self.to_df(), method2.to_df()

        print(right_df.columns)
        # Get sets of MultiIndex tuples
        left_indices = set(left_df.index)
        right_indices = set(right_df.index)

        # Check if all left_df indices are in right_df
        if not left_indices.issubset(right_indices):
            missing = left_indices - right_indices
            print(f"Missing MultiIndex entries in right_df: {missing}")
            sys.exit("Error: right_df is missing some MultiIndex entries from left_df.")

        # Merge right_df into left_df based on MultiIndex
        combined_df = left_df.join(right_df, how="left", rsuffix="_r")

        print("Combined DataFrame:")
        print(combined_df.head())

        combined_df["final_SF"] = combined_df["SF"] * combined_df["SF_r"]
        combined_df["final_Stat"] = combined_df["final_SF"] * np.sqrt(
            combined_df["Stat"] ** 2 / combined_df["SF"] ** 2
            + combined_df["Stat_r"] ** 2 / combined_df["SF_r"] ** 2
        )

        for label in self.syst_labels:
            combined_df[label] = combined_df[label] * combined_df["SF_r"]

        self.sf = combined_df["final_SF"].values
        self.stat = combined_df["final_Stat"].values
        self.syst = combined_df[self.syst_labels].values


@dataclass
class SimpleTPData(MethodSF):
    # This class is for the simple tag-and-probe scale factors
    # Where you have sf_CentralValues, sf_StatError, and sf_SystUp and sf_SystDown
    def read_data(self):

        SF_path = "sf_CentralValues"
        stat_path = "sf_StatError"

        # We will envelope these, to make it one variation
        syst_paths = [["sf_SystUp", "sf_SystDown"]]

        self.syst_labels = [
            "Total Sys",
        ]
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path, SF_path, stat_path=stat_path, syst_paths=syst_paths,dont_vary=True
        )
        self.sf = SF
        self.stat = stat
        self.syst = syst
        self.x_bins = x
        self.y_bins = y


@dataclass
class ZisoData(MethodSF):

    def read_data(self, removeNeg="no"):
        logger.info(f"Read in Ziso")

        ID = self.working_point
        print("self.data_path: ", self.data_path)
        print("ID: ", ID)
        # define the histograms holding the central value, and the systematic variations
        basename = "SF/SF_%s_%s_%s_%s_%s_%s_%s_%s_%s_DataDriven_Rel21_Smooth_vTest"

        # for the Ziso the templates and their syst variations is a complicated if-else thing
        # I'll loop a couple of templates and later stitch them together
        templates = [
            "TemplateVar1",  # pT<20 GeV
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, endcap
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, barrel
            "Template_ge1_sel_noloose",  # pT>25 GeV
        ]
        templates_syst = [
            "Template_ge1_sel_noloose",  # pT<20 GeV
            "Template6",  # 20<pT<25 GeV, endcap
            "TemplateVar1",  # 20<pT<25 GeV, barrel
            "Template6",  # pT>25 GeV
        ]
        templates_pTeta = [  # later we'll loop and fill values if pT>=1st entry and eta<=2nd entry. The order of these is therefore important
            [0.0, 99.0],  # pT<20 GeV
            [20.0, 99.0],  # 20<pT<25 GeV, endcap
            [20.0, 1.52],  # 20<pT<25 GeV, barrel
            [25.0, 99.0],  # pT>25 GeV
        ]

        # np arrays to hold the combined values
        SF = np.array([])
        stat = np.array([])
        syst = np.array([])
        x = np.array([])
        y = np.array([])

        for var in range(0, len(templates)):
            var_list = []
            var_list.append(
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # SF
            var_list.append(
                basename
                % (
                    "StatError",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )
            var_list.append(
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1AndTagIso",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # tag variation
            var_list.append(
                [
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll70110",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),  # mll variation
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll80100",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),
                ]
            )
            var_list.append(
                [
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p4",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),  # iso threshold
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p6",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),
                ]
            )
            var_list.append(
                basename
                % (
                    "CentralValue",
                    templates[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone40",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # discrim variable
            var_list.append(
                [
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor0.7",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),  # signal contamination
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.3",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    ),
                ]
            )
            var_list.append(
                [
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor0p7",
                        ID,
                    ),  # 2nd signal contamination
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p3",
                        ID,
                    ),
                ]
            )
            for t in range(0, var):
                var_list.append(
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    )
                )  # SF
            var_list.append(
                basename
                % (
                    "CentralValue",
                    templates_syst[var],
                    "Tag1",
                    "mll75105",
                    "ProbeIsoCut0p5",
                    "topoetcone30",
                    "doSubractMCeventsInTailScaleFactor1.0",
                    "RealElectronContaminationScalingFactor1p0",
                    ID,
                )
            )  # template variation
            for t in range(var + 1, len(templates)):
                var_list.append(
                    basename
                    % (
                        "CentralValue",
                        templates[var],
                        "Tag1",
                        "mll75105",
                        "ProbeIsoCut0p5",
                        "topoetcone30",
                        "doSubractMCeventsInTailScaleFactor1.0",
                        "RealElectronContaminationScalingFactor1p0",
                        ID,
                    )
                )  # SF

            # now we fill the histograms into arrays
            SF_var, stat_var, syst_var, x_var, y_var = getArray_SF_stat_syst(
                self.data_path, var_list
            )

            # stitching together the various template choices
            if var == 0:
                x = x_var
                y = y_var
                SF = np.zeros(SF_var.shape)
                stat = np.zeros(stat_var.shape)
                syst = np.zeros(syst_var.shape)

            def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
                arr_out = np.copy(arr_orig)
                if arr_orig.shape != arr_new.shape:
                    print(" in replaceValues: passing incompatible arrays")
                    return arr_out

                for ientry in range(0, arr_orig.shape[0]):
                    xbin = int(ientry / len(ybins))
                    ybin = ientry % len(ybins)

                    if (
                        xbins[xbin] >= cond_pT
                        and abs(ybins[ybin])
                        + np.sign(ybins[ybin]) * sys.float_info.epsilon
                        <= cond_eta
                    ):  # +sign*epsilon is there since the array only contains the lower bin boundary
                        #                if var ==2 :
                        #                    print( ' in var ', var, ' replacing ', xbins[xbin], ' ', ybins[ybin])
                        arr_out[ientry] = arr_new[ientry]

                return arr_out

            SF = replaceValues(
                SF, SF_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            stat = replaceValues(
                stat, stat_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            syst = replaceValues(
                syst, syst_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )

        # For the syst labels, we don't care about the template variations, just want one of the names
        for i in range(2, len(var_list)):
            if isinstance(var_list[i], list):
                print(var_list[i][0])
                self.syst_labels.append(var_list[i][0].split("/")[-1])
            else:
                print(var_list[i])
                self.syst_labels.append(var_list[i].split("/")[-1])

        self.syst_labels = [
            "Ziso: Tag Isolation",
            "Ziso: Zmass Window Var",
            "Ziso: Probe Isolation Var",
            "Ziso: Isolation Variable Change",
            "Ziso: Signal Contamination Bkg Norm",
            "Ziso: Signal Contamination Bkg Template",
            "Ziso: Nominal",
            "Ziso: Nominal",
            "Ziso: Nominal",
            "Ziso: Bkg Template Var",
        ]
        # now the SF, stat, syst, x, y arrays should correspond to what we want for the measurement

        # let's strip off the bins below 15 GeV
        SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Hanlin has NaNs at high Et where we do the measurement in abs(eta)
        SF = makeEtaAbs(SF, x, y, 150.0)
        stat = makeEtaAbs(stat, x, y, 150.0)
        syst = makeEtaAbs(syst, x, y, 150.0)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )

        arr_out = np.column_stack((SF, stat, syst))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        # Combining the Nominal variation and making it the SF
        new_central_values = []
        new_syst_labels = []
        for v in range(len(self.syst_labels)):
            if "Nominal" in self.syst_labels[v]:
                new_central_values.append(v)
            else:
                new_syst_labels.append(self.syst_labels[v])

        new_sf = np.zeros_like(arr_out[:, 0])
        for v in new_central_values:
            new_sf += arr_out[:, v]

        arr_out = arr_out[:, [0, 1, 2, 3, 4, 5, 6, 7, 11]]
        self.syst_labels = new_syst_labels
        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = np.array(y)


@dataclass
class ZisoDataV2(MethodSF):

    def read_data(self, removeNeg="no"):

        wp = "_" + self.working_point
        if not self.include_wp_names:
            wp = ""
        SF_path = "sf_Nominal_CentralValues" + wp
        stat_path = "sf_StatError" + wp

        syst_paths = [
            "sf_VAR_probe_variations" + wp,
            "sf_VAR_topoetcone_variations" + wp,
            "sf_VAR_mll_variations" + wp,
            "sf_VAR_tag_variations" + wp,
            "sf_VAR_subtrac_mc_in_tail_variations" + wp,
            "sf_VAR_real_elec_sf_variations" + wp,
            "sf_VAR_templ_variations" + wp,
        ]

        self.syst_labels = [
            "Ziso: Probe Isolation Var",
            "Ziso: Isolation Variable Change",
            "Ziso: Zmass Window Var",
            "Ziso: Tag Isolation",
            "Ziso: Signal Contamination Bkg Norm",
            "Ziso: Signal Contamination Bkg Template",
            "Ziso: Bkg Template Var",
        ]
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path, SF_path, stat_path=stat_path, syst_paths=syst_paths
        )
        # let's strip off the bins below 15 GeV
        SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        SF = makeEtaAbs(SF, x, y, 150.0)
        stat = makeEtaAbs(stat, x, y, 150.0)
        syst = makeEtaAbs(syst, x, y, 150.0)

        SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
        arr_out = np.column_stack((SF, stat, syst))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = y


@dataclass
class ZisoStatData(MethodSF):

    def read_data(self, removeNeg="no"):
        ID = self.working_point
        print("self.data_path: ", self.data_path)
        print("ID: ", ID)
        # define the histograms holding the central value, and the systematic variations
        basename = "%s_%s_%s_%s_%s_%s_%s_%s_DataDriven_Rel21_Smooth_vTest"

        # for the Ziso the templates and their syst variations is a complicated if-else thing
        # I'll loop a couple of templates and later stitch them together
        templates = [
            "TemplateVar1",  # pT<20 GeV
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, endcap
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, barrel
            "Template_ge1_sel_noloose",  # pT>25 GeV
        ]
        templates_pTeta = [  # later we'll loop and fill values if pT>=1st entry and eta<=2nd entry. The order of these is therefore important
            [0.0, 99.0],  # pT<20 GeV
            [20.0, 99.0],  # 20<pT<25 GeV, endcap
            [20.0, 1.52],  # 20<pT<25 GeV, barrel
            [25.0, 99.0],  # pT>25 GeV
        ]

        # np arrays to hold the combined values
        MC_ref = np.array([])
        Da_ref = np.array([])
        SF_ref = np.array([])
        stat_mc = np.array([])
        stat_da = np.array([])
        stat_sf = np.array([])
        bla_mc = np.array([])
        bla_da = np.array([])
        bla_sf = np.array([])
        x_mc = np.array([])
        x_da = np.array([])
        x = np.array([])
        y_mc = np.array([])
        y_da = np.array([])
        y = np.array([])

        def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
            arr_out = np.copy(arr_orig)
            if arr_orig.shape != arr_new.shape:
                print(" in replaceValues: passing incompatible arrays")
                return arr_out

            for ientry in range(0, arr_orig.shape[0]):
                xbin = int(ientry / len(ybins))
                ybin = ientry % len(ybins)

                if (
                    xbins[xbin] >= cond_pT
                    and abs(ybins[ybin]) + np.sign(ybins[ybin]) * sys.float_info.epsilon
                    <= cond_eta
                ):  # +sign*epsilon is there since the array only contains the lower bin boundary
                    arr_out[ientry] = arr_new[ientry]

            return arr_out

        for var in range(0, len(templates)):
            reference = [
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            ]

            # now we fill the histograms into arrays
            MC_var, stat_mc_var, syst_mc_var, x_mc, y_mc = getArray_SF_stat_syst(
                self.data_path,
                [
                    "EffMC/EffMC_" + basename % tuple(reference),
                    "EffMC/EffMC_StatError_" + basename % tuple(reference),
                ],
            )
            Da_var, stat_da_var, syst_da_var, x_da, y_da = getArray_SF_stat_syst(
                self.data_path, "EffData/EffData_" + basename % tuple(reference)
            )
            SF_var, stat_var, syst_var, x, y = getArray_SF_stat_syst(
                self.data_path, "SF/SF_CentralValue_" + basename % tuple(reference)
            )

            # stitching together the various template choices
            if var == 0:
                MC_ref = Da_ref = SF_ref = np.zeros(SF_var.shape)
                stat_mc = stat_da = stat_sf = np.zeros(stat_var.shape)
                bla_mc = bla_da = bla_sf = np.zeros(syst_var.shape)

            MC_ref = replaceValues(
                MC_ref,
                MC_var,
                x_mc,
                y_mc,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_mc = replaceValues(
                stat_mc,
                stat_mc_var,
                x_mc,
                y_mc,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

            Da_ref = replaceValues(
                Da_ref,
                Da_var,
                x_da,
                y_da,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_da = replaceValues(
                stat_da,
                stat_da_var,
                x_da,
                y_da,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

            SF_ref = replaceValues(
                SF_ref, SF_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            stat_sf = replaceValues(
                stat_sf,
                stat_var,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

        ###################################################
        # now the new stuff
        SF_new = np.array([])
        stat_new_corr = np.array([])
        stat_new_uncorr = np.array([])

        for var in range(0, len(templates)):
            reference = [
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            ]
            reflist = []
            reflist.append("EffData/EffData_" + basename % tuple(reference))
            reflist.append(
                "EffData/EffData_StatErrorCorr_" + basename % tuple(reference)
            )
            Da_corr, stat_corr, syst_corr, x_corr, y_corr = getArray_SF_stat_syst(
                self.data_path, reflist
            )
            reflist = []
            reflist.append("EffData/EffData_" + basename % tuple(reference))
            reflist.append(
                "EffData/EffData_StatErrorUncorr_" + basename % tuple(reference)
            )
            Da_uncorr, stat_uncorr, syst_uncorr, x_uncorr, y_uncorr = (
                getArray_SF_stat_syst(self.data_path, reflist)
            )

            SF_corr = np.zeros(Da_corr.shape)
            stat_sf_corr = np.zeros(Da_corr.shape)
            SF_uncorr = np.zeros(Da_uncorr.shape)
            stat_sf_uncorr = np.zeros(Da_uncorr.shape)
            for i in range(0, SF_corr.shape[0]):
                if MC_ref[i] != 0:
                    SF_corr[i] = Da_corr[i] / MC_ref[i]
                    SF_uncorr[i] = Da_uncorr[i] / MC_ref[i]
                    stat_sf_corr[i] = SF_corr[i] * np.sqrt(
                        (stat_corr[i] / Da_corr[i]) ** 2 + (stat_mc[i] / MC_ref[i]) ** 2
                    )
                    # print(np.sqrt( (stat_corr  [i]/Da_corr  [i])**2 + (stat_mc[i]/MC_ref  [i])**2 ),(stat_corr  [i]/Da_corr  [i]),(stat_mc[i]/MC_ref  [i]))
                    # print("\t",stat_corr  [i],Da_corr[i],stat_mc[i],MC_ref  [i])
                    stat_sf_uncorr[i] = SF_uncorr[i] * np.sqrt(
                        (stat_uncorr[i] / Da_uncorr[i]) ** 2
                    )
                    if stat_sf_corr[i] == 0 or stat_sf_uncorr[i] == 0:
                        # print(
                        #    " PHILIP found 0 uncertainties ",
                        #    stat_sf_corr[i],
                        #    " ",
                        #    stat_sf_uncorr[i],
                        # )
                        # print(
                        #    "  --> input is ",
                        #    SF_uncorr[i],
                        #    " ",
                        #    stat_uncorr[i],
                        #    " ",
                        #    Da_uncorr[i],
                        # )
                        pass
            # stitching together the various template choices
            if var == 0:
                SF_new = np.zeros(SF_corr.shape)
                stat_new_corr = np.zeros(stat_sf_corr.shape)
                stat_new_uncorr = np.zeros(stat_sf_uncorr.shape)

            SF_new = replaceValues(
                SF_new, SF_corr, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            stat_new_corr = replaceValues(
                stat_new_corr,
                stat_sf_corr,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_new_uncorr = replaceValues(
                stat_new_uncorr,
                stat_sf_uncorr,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

        x_tmp = np.array(x)
        y_tmp = np.array(y)

        # let's strip off the bins below 15 GeV
        MC_ref, stat_mc, bla_mc, x_mc, y_mc = stripFromArrayX(
            15.0, MC_ref, stat_mc, bla_mc, x_mc, y_mc
        )
        Da_ref, stat_da, bla_da, x_da, y_da = stripFromArrayX(
            15.0, Da_ref, stat_da, bla_da, x_da, y_da
        )
        SF_ref, stat_sf, bla_sf, x, y = stripFromArrayX(
            15.0, SF_ref, stat_sf, bla_sf, x, y
        )

        # eta abs
        MC_ref = makeEtaAbs(MC_ref, x, y, 150.0)
        stat_mc = makeEtaAbs(stat_mc, x, y, 150.0)

        Da_ref = makeEtaAbs(Da_ref, x, y, 150.0)
        stat_da = makeEtaAbs(stat_da, x, y, 150.0)

        SF_ref = makeEtaAbs(SF_ref, x, y, 150.0)
        stat_sf = makeEtaAbs(stat_sf, x, y, 150.0)

        # let's strip off the bins below 15 GeV
        SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp = stripFromArrayX(
            15.0, SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp
        )

        # eta abs
        SF_new = makeEtaAbs(SF_new, x, y, 150.0)
        stat_new_corr = makeEtaAbs(stat_new_corr, x, y, 150.0)
        stat_new_uncorr = makeEtaAbs(stat_new_uncorr, x, y, 150.0)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF_new, stat_new_uncorr, stat_new_corr, x, y )
        # writeArray_SF_stat_syst( outdir+'_ref', SF_ref, stat_sf, np.zeros(stat_sf.shape), x, y )

        arr_out = np.column_stack((SF_new, stat_new_uncorr, stat_new_corr))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = np.array(y)


@dataclass
class ZisoStatDataV2(MethodSF):

    def read_data(self, removeNeg="no"):
        ID = self.working_point
        print("self.data_path: ", self.data_path)
        print("ID: ", ID)
        # define the histograms holding the central value, and the systematic variations
        basename = "%s_%s_%s_%s_%s_%s_%s_%s"

        # for the Ziso the templates and their syst variations is a complicated if-else thing
        # I'll loop a couple of templates and later stitch them together
        templates = [
            "TemplateVar1",  # pT<20 GeV
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, endcap
            "Template_ge1_sel_noloose",  # 20<pT<25 GeV, barrel
            "Template_ge1_sel_noloose",  # pT>25 GeV
        ]
        templates_pTeta = [  # later we'll loop and fill values if pT>=1st entry and eta<=2nd entry. The order of these is therefore important
            [0.0, 99.0],  # pT<20 GeV
            [20.0, 99.0],  # 20<pT<25 GeV, endcap
            [20.0, 1.52],  # 20<pT<25 GeV, barrel
            [25.0, 99.0],  # pT>25 GeV
        ]

        # np arrays to hold the combined values
        MC_ref = np.array([])
        Da_ref = np.array([])
        SF_ref = np.array([])
        stat_mc = np.array([])
        stat_da = np.array([])
        stat_sf = np.array([])
        bla_mc = np.array([])
        bla_da = np.array([])
        bla_sf = np.array([])
        x_mc = np.array([])
        x_da = np.array([])
        x = np.array([])
        y_mc = np.array([])
        y_da = np.array([])
        y = np.array([])

        def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
            arr_out = np.copy(arr_orig)
            if arr_orig.shape != arr_new.shape:
                print(" in replaceValues: passing incompatible arrays")
                return arr_out

            for ientry in range(0, arr_orig.shape[0]):
                xbin = int(ientry / len(ybins))
                ybin = ientry % len(ybins)

                if (
                    xbins[xbin] >= cond_pT
                    and abs(ybins[ybin]) + np.sign(ybins[ybin]) * sys.float_info.epsilon
                    <= cond_eta
                ):  # +sign*epsilon is there since the array only contains the lower bin boundary
                    arr_out[ientry] = arr_new[ientry]

            return arr_out

        for var in range(0, len(templates)):
            reference = [
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            ]

            # now we fill the histograms into arrays
            Da_var, stat_da_var, syst_da_var, x_da, y_da = getArray_SF_stat_syst(
                self.data_path,
                "EffData_eff_CentralValues_" + basename % tuple(reference),
            )
            MC_var, stat_mc_var, syst_mc_var, x_mc, y_mc = getArray_SF_stat_syst(
                self.data_path,
                [
                    "EffMC_eff_CentralValues_" + basename % tuple(reference),
                    "EffMC_StatError_" + basename % tuple(reference),
                ],
            )

            SF_var, stat_var, syst_var, x, y = getArray_SF_stat_syst(
                self.data_path, "SF_sf_CentralValues_" + basename % tuple(reference)
            )

            # stitching together the various template choices
            if var == 0:
                MC_ref = Da_ref = SF_ref = np.zeros(SF_var.shape)
                stat_mc = stat_da = stat_sf = np.zeros(stat_var.shape)
                bla_mc = bla_da = bla_sf = np.zeros(syst_var.shape)

            MC_ref = replaceValues(
                MC_ref,
                MC_var,
                x_mc,
                y_mc,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_mc = replaceValues(
                stat_mc,
                stat_mc_var,
                x_mc,
                y_mc,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

            Da_ref = replaceValues(
                Da_ref,
                Da_var,
                x_da,
                y_da,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_da = replaceValues(
                stat_da,
                stat_da_var,
                x_da,
                y_da,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

            SF_ref = replaceValues(
                SF_ref, SF_var, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            stat_sf = replaceValues(
                stat_sf,
                stat_var,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

        ###################################################
        # now the new stuff
        SF_new = np.array([])
        stat_new_corr = np.array([])
        stat_new_uncorr = np.array([])

        for var in range(0, len(templates)):
            reference = [
                templates[var],
                "Tag1",
                "mll75105",
                "ProbeIsoCut0p5",
                "topoetcone30",
                "doSubractMCeventsInTailScaleFactor1.0",
                "RealElectronContaminationScalingFactor1p0",
                ID,
            ]
            reflist = []
            reflist.append("EffData_eff_CentralValues_" + basename % tuple(reference))
            reflist.append("EffData_StatErrorCorr_" + basename % tuple(reference))
            Da_corr, stat_corr, syst_corr, x_corr, y_corr = getArray_SF_stat_syst(
                self.data_path, reflist
            )
            reflist = []
            reflist.append("EffData_eff_CentralValues_" + basename % tuple(reference))
            reflist.append("EffData_StatErrorUncorr_" + basename % tuple(reference))
            Da_uncorr, stat_uncorr, syst_uncorr, x_uncorr, y_uncorr = (
                getArray_SF_stat_syst(self.data_path, reflist)
            )

            SF_corr = np.zeros(Da_corr.shape)
            stat_sf_corr = np.zeros(Da_corr.shape)
            SF_uncorr = np.zeros(Da_uncorr.shape)
            stat_sf_uncorr = np.zeros(Da_uncorr.shape)
            for i in range(0, SF_corr.shape[0]):
                if MC_ref[i] != 0:
                    SF_corr[i] = Da_corr[i] / MC_ref[i]
                    SF_uncorr[i] = Da_uncorr[i] / MC_ref[i]
                    stat_sf_corr[i] = SF_corr[i] * np.sqrt(
                        (stat_corr[i] / Da_corr[i]) ** 2 + (stat_mc[i] / MC_ref[i]) ** 2
                    )
                    # print(np.sqrt( (stat_corr  [i]/Da_corr  [i])**2 + (stat_mc[i]/MC_ref  [i])**2 ),(stat_corr  [i]/Da_corr  [i]),(stat_mc[i]/MC_ref  [i]))
                    # print("\t",stat_corr  [i],Da_corr[i],stat_mc[i],MC_ref  [i])
                    stat_sf_uncorr[i] = SF_uncorr[i] * np.sqrt(
                        (stat_uncorr[i] / Da_uncorr[i]) ** 2
                    )
                    if stat_sf_corr[i] == 0 or stat_sf_uncorr[i] == 0:
                        # print(
                        #    " PHILIP found 0 uncertainties ",
                        #    stat_sf_corr[i],
                        #    " ",
                        #    stat_sf_uncorr[i],
                        # )
                        # print(
                        #    "  --> input is ",
                        #    SF_uncorr[i],
                        #    " ",
                        #    stat_uncorr[i],
                        #    " ",
                        #    Da_uncorr[i],
                        # )
                        pass
            # stitching together the various template choices
            if var == 0:
                SF_new = np.zeros(SF_corr.shape)
                stat_new_corr = np.zeros(stat_sf_corr.shape)
                stat_new_uncorr = np.zeros(stat_sf_uncorr.shape)

            SF_new = replaceValues(
                SF_new, SF_corr, x, y, templates_pTeta[var][0], templates_pTeta[var][1]
            )
            stat_new_corr = replaceValues(
                stat_new_corr,
                stat_sf_corr,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )
            stat_new_uncorr = replaceValues(
                stat_new_uncorr,
                stat_sf_uncorr,
                x,
                y,
                templates_pTeta[var][0],
                templates_pTeta[var][1],
            )

        x_tmp = np.array(x)
        y_tmp = np.array(y)

        # let's strip off the bins below 15 GeV
        MC_ref, stat_mc, bla_mc, x_mc, y_mc = stripFromArrayX(
            15.0, MC_ref, stat_mc, bla_mc, x_mc, y_mc
        )
        Da_ref, stat_da, bla_da, x_da, y_da = stripFromArrayX(
            15.0, Da_ref, stat_da, bla_da, x_da, y_da
        )
        SF_ref, stat_sf, bla_sf, x, y = stripFromArrayX(
            15.0, SF_ref, stat_sf, bla_sf, x, y
        )

        # eta abs
        MC_ref = makeEtaAbs(MC_ref, x, y, 150.0)
        stat_mc = makeEtaAbs(stat_mc, x, y, 150.0)

        Da_ref = makeEtaAbs(Da_ref, x, y, 150.0)
        stat_da = makeEtaAbs(stat_da, x, y, 150.0)

        SF_ref = makeEtaAbs(SF_ref, x, y, 150.0)
        stat_sf = makeEtaAbs(stat_sf, x, y, 150.0)

        # let's strip off the bins below 15 GeV
        SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp = stripFromArrayX(
            15.0, SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp
        )

        # eta abs
        SF_new = makeEtaAbs(SF_new, x, y, 150.0)
        stat_new_corr = makeEtaAbs(stat_new_corr, x, y, 150.0)
        stat_new_uncorr = makeEtaAbs(stat_new_uncorr, x, y, 150.0)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF_new, stat_new_uncorr, stat_new_corr, x, y )
        # writeArray_SF_stat_syst( outdir+'_ref', SF_ref, stat_sf, np.zeros(stat_sf.shape), x, y )

        arr_out = np.column_stack((SF_new, stat_new_uncorr, stat_new_corr))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = np.array(y)


@dataclass
class ZmassData(MethodSF):

    def read_data(self, removeNeg="no"):

        wp = "_" + self.working_point
        if not self.include_wp_names:
            wp = ""
        SF_path = "sf_CentralValues_nominal" + wp
        stat_path = "StatError_nominal" + wp

        syst_paths = [
            "sf_CentralValues_int_70_110" + wp,
            "sf_CentralValues_bkg_fit_range_lo" + wp,
            "sf_CentralValues_bkg_fit_up" + wp,
            "sf_CentralValues_bkg_model_alt25" + wp,
            "sf_CentralValues_mc_weight_off" + wp,
            "sf_CentralValues_sig_cont_do" + wp,
        ]

        self.syst_labels = [
            "Zmass: Window Var",
            "Zmass: Bkg Est Low or High Mee Region",
            "Zmass: Bkg Norm Var",
            "Zmass: Alt Bkg Model 20-25GeV",
            "Zmass: Mee Reweighting off",
            "Zmass: Sig Contamination Var",
        ]
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path, SF_path, stat_path=stat_path, syst_paths=syst_paths
        )
        # let's strip off the bins below 15 GeV
        SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        SF = makeEtaAbs(SF, x, y, 150.0)
        stat = makeEtaAbs(stat, x, y, 150.0)
        syst = makeEtaAbs(syst, x, y, 150.0)

        SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
        arr_out = np.column_stack((SF, stat, syst))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = y


@dataclass
class ZmassStatData(MethodSF):

    def read_data(self, removeNeg="no"):
        filename = self.data_path
        ID = self.working_point
        infile = ROOT.TFile.Open(filename)
        # define the histograms holding the central value, and the systematic variations
        # basename = '%s_%s_%s_%s_%s_%s_%s_%s'

        list_var = ["nominal_" + ID]

        # now we fill the histograms into arrays
        MC_ref, stat_mc, bla_mc, x_mc, y_mc = getArray_SF_stat_syst(
            filename,
            ["EffMC_eff_CentralValues_nominal_" + ID, "EffMC_StatError_nominal_" + ID],
        )
        Da_ref, stat_da, bla_da, x_da, y_da = getArray_SF_stat_syst(
            filename,
            [
                "EffData_eff_CentralValues_nominal_" + ID,
                "EffData_StatError_nominal_" + ID,
            ],
        )
        SF_ref, stat_sf, bla_sf, x, y = getArray_SF_stat_syst(
            filename, ["sf_CentralValues_nominal_" + ID, "StatError_nominal_" + ID]
        )

        def replaceValues(arr_orig, arr_new, xbins, ybins, cond_pT, cond_eta):
            arr_out = np.copy(arr_orig)
            if arr_orig.shape != arr_new.shape:
                print(" in replaceValues: passing incompatible arrays")
                return arr_out

            for ientry in range(0, arr_orig.shape[0]):
                xbin = int(ientry / len(ybins))
                ybin = ientry % len(ybins)

                # if xbins[xbin]>=cond_pT and abs(ybins[ybin])+np.sign(ybins[ybin])*sys.float_info.epsilon<=cond_eta : # +sign*epsilon is there since the array only contains the lower bin boundary
                #    arr_out[ientry] = arr_new[ientry]

            return arr_out

        ###################################################
        # now the new stuff
        SF_new = np.array([])
        stat_new_corr = np.array([])
        stat_new_uncorr = np.array([])

        for var in range(0, len(list_var)):
            reference = list_var[var]
            reflist = []
            reflist.append("EffData_eff_CentralValues_" + reference)
            reflist.append("EffData_StatErrorCorr_" + reference)
            Da_corr, stat_corr, syst_corr, x_corr, y_corr = getArray_SF_stat_syst(
                filename, reflist
            )
            reflist = []
            reflist.append("EffData_eff_CentralValues_" + reference)
            reflist.append("EffData_StatErrorUncorr_" + reference)
            Da_uncorr, stat_uncorr, syst_uncorr, x_uncorr, y_uncorr = (
                getArray_SF_stat_syst(filename, reflist)
            )

            SF_corr = np.zeros(Da_corr.shape)
            stat_sf_corr = np.zeros(Da_corr.shape)
            SF_uncorr = np.zeros(Da_uncorr.shape)
            stat_sf_uncorr = np.zeros(Da_uncorr.shape)
            for i in range(0, SF_corr.shape[0]):
                if MC_ref[i] != 0:
                    SF_corr[i] = Da_corr[i] / MC_ref[i]
                    SF_uncorr[i] = Da_uncorr[i] / MC_ref[i]
                    stat_sf_corr[i] = SF_corr[i] * np.sqrt(
                        (stat_corr[i] / Da_corr[i]) ** 2 + (stat_mc[i] / MC_ref[i]) ** 2
                    )
                    stat_sf_uncorr[i] = SF_uncorr[i] * np.sqrt(
                        (stat_uncorr[i] / Da_uncorr[i]) ** 2
                    )
                    if stat_sf_corr[i] == 0 or stat_sf_uncorr[i] == 0:
                        print(
                            " PHILIP found 0 uncertainties ",
                            stat_sf_corr[i],
                            " ",
                            stat_sf_uncorr[i],
                        )
                        print(
                            "  --> input is ",
                            SF_uncorr[i],
                            " ",
                            stat_uncorr[i],
                            " ",
                            Da_uncorr[i],
                        )
            # stitching together the various template choices
            if var == 0:
                SF_new = np.zeros(SF_corr.shape)
                stat_new_corr = np.zeros(stat_sf_corr.shape)
                stat_new_uncorr = np.zeros(stat_sf_uncorr.shape)

            SF_new = SF_corr
            stat_new_corr = stat_sf_corr
            stat_new_uncorr = stat_sf_uncorr

        x_tmp = np.array(x)
        y_tmp = np.array(y)

        # let's strip off the bins below 15 GeV
        MC_ref, stat_mc, bla_mc, x_mc, y_mc = stripFromArrayX(
            15.0, MC_ref, stat_mc, bla_mc, x_mc, y_mc
        )
        Da_ref, stat_da, bla_da, x_da, y_da = stripFromArrayX(
            15.0, Da_ref, stat_da, bla_da, x_da, y_da
        )
        SF_ref, stat_sf, bla_sf, x, y = stripFromArrayX(
            15.0, SF_ref, stat_sf, bla_sf, x, y
        )

        # eta abs
        MC_ref = makeEtaAbs(MC_ref, x, y, 150.0)
        stat_mc = makeEtaAbs(stat_mc, x, y, 150.0)

        Da_ref = makeEtaAbs(Da_ref, x, y, 150.0)
        stat_da = makeEtaAbs(stat_da, x, y, 150.0)

        SF_ref = makeEtaAbs(SF_ref, x, y, 150.0)
        stat_sf = makeEtaAbs(stat_sf, x, y, 150.0)

        # let's strip off the bins below 15 GeV
        SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp = stripFromArrayX(
            15.0, SF_new, stat_new_uncorr, stat_new_corr, x_tmp, y_tmp
        )

        # eta abs
        SF_new = makeEtaAbs(SF_new, x, y, 150.0)
        stat_new_corr = makeEtaAbs(stat_new_corr, x, y, 150.0)
        stat_new_uncorr = makeEtaAbs(stat_new_uncorr, x, y, 150.0)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF_new, stat_new_uncorr, stat_new_corr, x, y )
        # writeArray_SF_stat_syst( outdir+'_ref', SF_ref, stat_sf, np.zeros(stat_sf.shape), x, y )

        arr_out = np.column_stack((SF_new, stat_new_uncorr, stat_new_corr))
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = np.array(y)


@dataclass
class JpsiData(MethodSF):

    def read_data(self, removeNeg="no"):
        infile = self.data_path
        ID = "_" + self.working_point
        if not self.include_wp_names:
            ID = ""
        # define the histograms holding the central value, and the systematic variations
        # jpsi_var_list = [
        #    "sf_CentralValues" + ID,  # this is the chosen nominal variation
        #    "sf_StatError" + ID,
        # ]
        jpsi_var_list = [
            "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_3_mll_2.7_3.4"
            + ID,
            "StatError_prompt0p2_tag_baseline_den_baseline_Cheb_3_mll_2.7_3.4" + ID,
        ]
        self.syst_labels = [
            "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_2_mll_2.7_3.4"
            + ID,
            "sf_CentralValues_prompt0p2_tag_baseline_den_baseline_Cheb_3_mll_2.8_3.3"
            + ID,
            "sf_CentralValues_prompt0p4_tag_baseline_den_baseline_Cheb_3_mll_2.7_3.4"
            + ID,
            "sf_CentralValues_prompt0p2_tag_iso0p15_den_baseline_Cheb_3_mll_2.7_3.4"
            + ID,
            "sf_CentralValues_prompt0p2_tag_baseline_den_ptcone0p15_Cheb_3_mll_2.7_3.4"
            + ID,
            "sf_CentralValues_prompt0p2_tag_baseline_den_ptcone0p02_Cheb_3_mll_2.7_3.4"
            + ID,
        ]

        jpsi_var_list.extend(self.syst_labels)
        self.syst_labels = [
            "J/Psi: Bkg Polynomial Deg",
            "J/Psi: Mll Window",
            "J/Psi: Liftime Cut",
            "J/Psi: Tag Isolation",
            "J/Psi: Probe Isolation Loose",
            "J/Psi: Probe Isolation Strict",
        ]
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst(infile, jpsi_var_list)

        SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        arr_out = np.column_stack((SF, stat, syst))

        pos_arr = []
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            for row_i in range(len(arr_out)):
                if row_i % (len(posy) * 2) >= len(posy):
                    pos_arr.append(arr_out[row_i, :])
            arr_out = np.array(pos_arr)

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = np.array(y)


@dataclass
class ZrecoData(MethodSF):

    def read_data(self, pTmin=15000.0, pTmax=99999999.0, removeNeg="no"):
        infile = self.data_path
        indir = ""
        x_unit_conversion = 1000
        match = ".+"
        print(match)
        # now we fill the histograms into arrays
        SF, self.stat, self.syst, self.x, self.y = self.getArray_SF_stat_syst(
            infile, indir, match, pTmin, pTmax, x_unit_conversion=x_unit_conversion
        )
        self.sf = makeEtaAbs(SF, self.x, self.y, 150.0)

    def getArray_SF_stat_syst(
        self, filename, path, match, pTmin=0.0, pTmax=99999999.0, x_unit_conversion=1
    ):
        # ------------------------------------------------------------------------------
        # this is the most important function for us
        #
        # it will return 5 np.arrays:
        # - the central values
        # - the stat uncertainty
        # - an array of systematic uncertainties, with columns of systematic sources
        # - the x-bins
        # - the y-bins
        #
        # it takes as input:
        # - a root file name
        # - the path in the root file
        # - a string and all histograms matching the name will be considered to get the mean and rms
        # ------------------------------------------------------------------------------

        # here is what we're returning
        SF = np.array([])
        stat = np.array([])
        syst = np.array([])
        xbins = np.array([])
        ybins = np.array([])

        infile = ROOT.TFile.Open(filename)
        if not infile.IsOpen():
            print(" in rootToArray: problem opening file ", filename)
            return SF, stat, syst, xbins, ybins

        listOfCentralValues = []
        listOfStatErrors = []

        directory = infile
        if len(path):
            directory = infile.Get(path)

        for hist in directory.GetListOfKeys():
            hname = hist.GetName()
            reg = re.compile(match)

            if not bool(reg.match(hname)):
                #           print(' skipping in hname ', hname , ' when looking for ', match)
                continue

            if "CentralValue" in hname:
                print(" adding hist central value ", hname)
                listOfCentralValues.append(directory.Get(hname))
            elif "StatError" in hname:
                print(" adding hist stat error ", hname)
                listOfStatErrors.append(directory.Get(hname))

        if not len(listOfCentralValues) or len(listOfCentralValues) != len(
            listOfStatErrors
        ):
            print(
                " in rootToArray: found no or inconsistent number of histograms ",
                len(listOfCentralValues),
                " ",
                len(listOfStatErrors),
            )
        else:
            print(" added ", len(listOfCentralValues), " histograms ")

        # get the binning from the first one
        xbins, ybins = self.getArray_binning(listOfCentralValues[0])
        print(xbins)
        xbins = xbins * x_unit_conversion
        print(xbins)
        # we need to cut some of the variations that are invalid
        cutoff_SF = 1.2
        cutoff_stat = 0.5
        cutoff_SF = 99999.0
        cutoff_stat = 1.5

        # now getting central values and stuff
        allSFs = []  # np.zeros( (xbins.size*ybins.size,0) )
        allStat = []  # np.zeros( (xbins.size*ybins.size,0) )
        for x in range(0, xbins.size):
            for y in range(0, ybins.size):
                var_val = []
                var_err = []
                histo = 0
                for cv in range(0, len(listOfCentralValues)):
                    val = listOfCentralValues[cv].GetBinContent(x + 1, y + 1)
                    err = listOfStatErrors[cv].GetBinContent(x + 1, y + 1)
                    var_val.append(val)
                    var_err.append(err)

                allSFs.append(np.array(var_val))
                allStat.append(np.array(var_err))

        SF = np.zeros(len(allSFs))
        stat = np.zeros(len(allStat))
        cov = np.zeros((len(allSFs), len(allSFs)))
        for i in range(0, len(allSFs)):
            mean = 0.0
            mean_stat = 0.0
            counter = 0.0
            for var in range(0, len(allSFs[i])):
                val = allSFs[i][var]
                err = allStat[i][var]

                if (
                    val > 20.0 and val < 100.0
                ):  # it's Andrey's damn Jpsi efficiency that is in percent
                    val = val / 100.0
                    err = err / 100.0

                if (
                    val > 0.0 and abs(val - 1) < cutoff_SF and err < cutoff_stat
                ):  # skip a bunch of SF variations
                    mean += val
                    mean_stat += err
                    counter += 1
                #                if i==185 :
                #                    print ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' adding mean ', val, ' stat ', err
                #                    print ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' mean ', mean, ' stat ', mean_stat, ' counter ', counter
                elif mean != 0:
                    print(
                        " skipping ",
                        val,
                        " +- ",
                        err,
                        " in bins ",
                        xbins[int(i / len(ybins))],
                        " / ",
                        ("%1.2f" % ybins[i % len(ybins)]),
                    )
            if counter > 0.0:
                SF[i] = mean / counter
                stat[i] = mean_stat / counter
        for i in range(0, len(allSFs)):
            for j in range(0, len(allSFs)):
                corr = 0.0
                counter = 0.0
                for var in range(0, len(allSFs[i])):
                    vali = allSFs[i][var]
                    valj = allSFs[j][var]
                    erri = allStat[i][var]
                    errj = allStat[j][var]
                    if (
                        vali > 0.0
                        and valj > 0.0
                        and abs(vali - 1) < cutoff_SF
                        and erri < cutoff_stat
                        and abs(valj - 1) < cutoff_SF
                        and errj < cutoff_stat
                    ):  # skip a bunch of SF variations
                        corr += (vali - SF[i]) * (valj - SF[j])
                        counter += 1.0
                #                elif vali != 0 and valj != 0 and erri<99999999999. and errj<99999999999. :
                #                    print ' skipping 2 ', vali, ' +- ', erri, '  ', valj, ' +- ', errj, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]), ' in bins ', xbins[int(j/len(ybins))], ' / ', ('%1.2f' % ybins[j%len(ybins)])
                if counter > 0.0:
                    cov[i, j] = corr / counter
        #            else :
        #                print ' problem counting cov in bins ' ,i, ' ',j, ' ', counter

        SF, stat, cov, xbins, ybins = self.stripFromArrayX(
            pTmin, SF, stat, cov, xbins, ybins
        )
        SF, stat, cov, xbins, ybins = self.stripFromArrayX_max(
            pTmax, SF, stat, cov, xbins, ybins
        )

        # --------------------------------------
        # let's run an eigenvalue decomposition
        # --------------------------------------
        val_eig, vec_eig = np.linalg.eigh(cov)

        # remove complex entries, in general all this can be complex but for positive definite matrix is real
        val_eig = np.flipud(val_eig)
        vec_eig = np.fliplr(vec_eig)
        for iv in range(0, len(val_eig)):  # if there are neg eigenvalues, set them to 0
            if val_eig[iv] < 0.0:
                val_eig[iv] = 0

        eigsum = np.sum(val_eig)
        if eigsum == 0:
            print(
                " all eigenvalues are zero so the syst uncertainty must be zero, too "
            )
            return SF, stat, np.zeros(stat.shape), xbins, ybins

        # eigenvalues are already sorted by size, let's find the largest 99%
        counteig = 0
        currentsum = 0
        for eig in val_eig:

            currentsum += eig
            counteig += 1
            if currentsum / eigsum > 0.99:
                break

        print(" number of eig ", counteig, val_eig[:counteig])
        # the gamma matrix contains the sqrt( eigenvalue ) * eigenvectors
        # --> these are the uncorrelated sources of uncertainty
        gamma = vec_eig[:, :counteig].dot(np.diag(np.sqrt(val_eig[:counteig])))
        rest = vec_eig[:, counteig:].dot(np.diag(np.sqrt(val_eig[counteig:])))
        rest = np.sqrt(np.sum(rest**2, axis=1))

        # copy the syst and ignore for now the uncorr
        syst = gamma
        # add the uncorr to stats
        stat = np.sqrt(np.sum(np.column_stack((stat**2, rest**2)), axis=1))

        infile.Close()
        return SF, stat, syst, xbins, ybins

    def getArray_binning(self, histogram):

        # returns arrays with bin boundaries in x and y, excluding the upper edge of the last bin
        if not histogram:
            print(" in rootToArray:  no histogram ", histogram)
            return 0

        nxbins = histogram.GetXaxis().GetNbins()
        nybins = histogram.GetYaxis().GetNbins()

        xbins = np.array(
            [histogram.GetXaxis().GetBinLowEdge(x + 1) for x in range(0, nxbins)]
        )  # I don't want the upper edge for now
        ybins = np.array(
            [histogram.GetYaxis().GetBinLowEdge(y + 1) for y in range(0, nybins)]
        )

        return xbins, ybins

    def stripFromArrayX(self, threshold, SF, stat, cov, xbins, ybins):

        # not all our methods cover the full pT range, so let's strip some of the zeros
        bin = 0
        for x in xbins:
            if x < threshold:
                bin = bin + 1

        return (
            SF[(bin * ybins.size) :],
            stat[(bin * ybins.size) :],
            cov[(bin * ybins.size) :, (bin * ybins.size) :],
            xbins[bin:],
            ybins,
        )

    def stripFromArrayX_max(self, threshold, SF, stat, cov, xbins, ybins):

        # not all our methods cover the full pT range, so let's strip some of the zeros
        bin = 0
        for x in xbins:
            if x < threshold:
                bin = bin + 1

        return (
            SF[: (bin * ybins.size)],
            stat[: (bin * ybins.size)],
            cov[: (bin * ybins.size), : (bin * ybins.size)],
            xbins[:bin],
            ybins,
        )
