# EID Combination

This repository holds the code for Electron Identification Combination. The directory structure is meant to emulate that of the Tag&Probe analysis_packs. An example of how the code could be run with the Gitlab CI/CD pipelines is given. The combinations are computed in the compute_stage and uploaded to eos in the finalize_stage. The actual computation of the combination is done with Heraverager. This explains the python2.7 folder as the dependecies for this software live here.

## Methodology

In an attempt to make the process of the combination simpler a structure of dataclasses and combination recipe was developed. All of the dataclasses are defined in the dataclass.py file and the recipe is in the combinate file.

## Heraverager

For Run 2 Release 22 the EID group wanted to replicate the same process done in Run 2 Release 22 so we use the same averaging software, Heraverager. This project is built to run in python2 and has fortran dependencies. Since, we wanted to have the project written in python 3 we must execute the Heraverager step in a seperate script run in a docker container with python 2 configured. Then the paths to the dependencies are set to point to the "python2.7" directory. To be able to perform computation, the data massed be written to a file so it can be read inside the docker container and then again written out. This is done in the "data/" folder. 

## Results

All the results are written to the "results/" directory and take the form of root file with the scalefactors, statistics, and systematics. However, the systematics are in the form of its most important eigenvectors written to all the variables corr_n. The remaining eigenvectors are summed in quadrature per kinematic bin and put into the uncorr variable. The files who have high pt and low pt contributions reflect that in the root file. All of the variables belonging to low pt have an extra string "_LowPt".

## Pipeline stages

### compute_stage

For each working point, perform the combination. This involves, reading in zmass and ziso, dealing with correlations, combination between z methods, reading in jpsi, and performing the final combination.

### finalize_stage

For each working point, perform eigenvector decomposition, write to root files, and write these root files to eos.

## Electron Identification Combination

To perform a combination of electron identification scalefactors, you can follow the example file `combine_Tight_2018.py`. This can be run by running in the main project directory:
```
source setup.sh
source set_paths.sh
python -m examples.combine_Tight_2018
```

However, the instructions for the combination must follow the procedure that you wish to perform. 

First, you must set the years and working points you wish to perform the combinations for.

Then must set the paths for your data in python/config.py. Which holds dictionaries pointing to the path of the input data you have for each year and working point.

In iteration of these years and working points, you now must read-in the measurements you have into the dataclass objects so the code can perform operations on the measurements. There is a base class for this called MethodSF in the python/dataclass.py file. There are also 6 other pre-defined dataclasses. Ziso, ZisoStat (used to decorrelate the statistics in Ziso), Zmass, ZmassStat (used to decorrelate the statistics in Ziso), JPsi, and ZReco (used for inplace decomposition where no combination with other measurements is done).

Once you have read-in the measurements, the default procedure is to decorrelate the statistics in Ziso and Zmass. This will make a diagonal matrix in each measurement where each column is the statistical uncertainty in each kinematic bin. This is done to include the statistics in the combination with each kinematic bin statistical uncertainty not affecting the neighboring bin.

We also need to decorrelate systematic uncertainties if they are not correlated in your measurements. This function simply makes sure that each column index is only populated by one measurement at a time. This will make it so during the combination, the systematics are not allowed to affect each other in that kinematic bin.

Then you would combine the Z methods with the combineZmethods function. This returns a new dataclass object with the combined measurements. 

After combination, we want to remove all of the statistcal uncertainty columns so we `undecorrelateStat` this collapes the decorrelated statistics matrix. We will don't need to do this with the systematics because they should remain in their own column.

Now, we start to work with JPsi. It is read-in by making the JPsi dataclass.

It is important to note that the Zmethods are split in the default binning for Z for high pt and the binning for Jpsi for low pt. This was done because the rebinning step used heraverager and it was failing due to inconsistent measurements in the bins that were getting merged. The fix was to split the Z measurements in pt and put the section that will be combined with JPsi, to match JPsi's binning.

We then need to repeat the process of decorrelating the systematics except now with the combined Z measurements and JPsi.

Then we can combine JPsi and the Z methods in the pt bin where the measurements overlap. This is done by invoking the `combineHera` function. This calls a script that uses the heraverager library, written in FORTRAN. This will again return a new dataclass object with the combined measurements. 

Now that we have the final measurements, we can perform the eigendecomposition to reduce the number of systematic uncertainties passed to analyses.

Before we do that, it is nice to understand the contribution of each systematic uncertainty in the eigenvectors. This can be done with the `studyEigenMapping` function. It will plot the eigenvector matrix with the columns labeled with the corresponding eigenvector number and the rows labeled with the corresponding systematic uncertainty. This will show how much each systematic uncertainty contributes to each eigenvector. 

In the `writeRootFiles_decomp` function, we conclude our combination. Here, the whole pt spectrum for the scalefactors, statistical uncertainties, and systematic variations are concatenated, eigendecomposed, and finally written to a file. This function first takes the year for the combination and finds the range of run numbers for this year. It then makes a directory in the root file where it can write the various measurements. For high and low pt, you will have the scalefactors, statistical uncertainty, and the 16 most important eigenvectors. the rest of the eigenvectors are summed by kinematic bin and put into a final variable called uncorr.

The file looks like:
```
| Run Number
|-- SF
|-- Stat
|-- Corr_0
|-- Corr_1
|-- Corr_N
|-- UnCorr
|-- LowpT_SF
|-- LowpT_Stat
|-- LowpT_Corr_0
|-- LowpT_Corr_1
|-- LowpT_Corr_N
|-- LowpT_UnCorr
```
It is also useful to see what is going on at each step of the combination. Therefore, there are different logging functions you can use see what is happening. You can use MethodSF.log() to see the Scalefactors and statistical uncertainty as a heatmap. You can use MethodSF.log_syst() to do the same but only for the systematic variations. Finally, you can use `plot_scale_factors_at_pt` to do just as it says where the horizontal error bars are the bin size and the vertical error bars are the stat + syst uncertainty.

## Simple Decomposition

To perform a simple decomposition in the case of the Zreco method, you will make sure you are in the main directory of the repository then run
```
source setup.sh
source set_paths.sh
python -m examples.simple_decomp
```
Make sure to modify the path for the input files as is defined in the simple_decom.py file and you will then see the final file in the results directory.