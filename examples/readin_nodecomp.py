from SFCombination import *
import argparse
import numpy as np
import os

# Define the path where the variations live
data_path = "/afs/cern.ch/user/n/nibrahim/public/forSully/scalefactor_e26_e60_e140_e300_LooseBLLLH_isolLoose_VarRad_central_value.root"

method = SimpleTPData(data_path=data_path, last_pt_bound=50000)
method.convertGeVtoMeV()
method.log("readin", "simple")
method.log_syst("readin", "simple")

plot_all_scale_factors(
    [method],
    ["EL EFF ID"],
    colors=["black"],
    name="run3",
)
# write to an eGamma root file
writeRoot([2023], [method], "results/run3.root", doCorr=False)
