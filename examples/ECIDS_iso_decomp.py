from SFCombination import ZrecoData, writeRoot
import argparse
import numpy as np
import os

# Specify the years you want to work with
years = [2015, 2016, 2017, 2018]

working_points = ["loose", "medium", "tight"]
ECIDS = ["ECIDS_Tight_VarRad", "Tight_VarRad_est"]

for wp in working_points:
    for ecids in ECIDS:
        if (ecids == "Tight_VarRad_est") and (wp == "loose"):
            continue
        if wp == "tight":
            continue
        zrecos = []
        for year in years:

            path = f"/afs/cern.ch/user/c/cbilling/tNp/eid-combination/data/ECIDS/tp-scalefactor-zmass-efficiency_rel22_{wp}_F_data{year-2000}_ECIDS_{wp}/scalefactor_{ecids}_variations.root"
            print(wp, year)
            zreco = ZrecoData(data_path=path)
            zrecos.append(zreco)

        # write to an eGamma root file, the decomposition is also performed in this function
        writeRoot(years, zrecos, f"results/ecids/{wp}_{ecids}_decomp.root")

years = [20152016, 2017, 2018]

correctedYear_dict = {20152016: "1516_a", 2017: "17_d", 2018: "18_e"}
working_points = ["loose", "medium", "tight"]
ECIDS = [
    "HighPtCaloOnly",
    "Loose_VarRad",
    "Tight_VarRad",
    "Tight_VarRad_est",
    "TightTrackOnly_FixedRad",
    "TightTrackOnly_VarRad",
]

for wp in working_points:
    for ecids in ECIDS:
        zrecos = []
        for year in years:
            correctedYear = correctedYear_dict[year]
            path = f"/afs/cern.ch/user/c/cbilling/tNp/eid-combination/data/iso/tp-scalefactor-zmass-efficiency_rel22_{wp}_F_{correctedYear}/scalefactor_{ecids}_variations.root"
            print(wp, year)
            zreco = ZrecoData(data_path=path)
            zrecos.append(zreco)

        # write to an eGamma root file, the decomposition is also performed in this function
        writeRoot(years, zrecos, f"results/iso/{wp}_{ecids}_decomp.root")
