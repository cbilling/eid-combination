from SFCombination import (
    MethodSF,
    JpsiData,
    ZmassData,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    writeRoot,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
    getArray_SF_stat_syst_refactored,
)
from dataclasses import dataclass
import numpy as np
import os

# from .tools import (
#    getArray_SF_stat_syst,
#    stripFromArrayX,
#    makeEtaAbs,
#    check_SF_stat_syst,
#    getArray_SF_stat_syst_refactored,
# )


@dataclass
class MatrixMethod(MethodSF):
    def read_data(self, removeNeg="yes", include_wp_names=False):

        SF_path = "MM_sf_Base"
        stat_path = "MM_sf_unc_stat"

        syst_path = [
            "MM_sf_unc_MM_correction",
            "MM_sf_unc_MM_iteration",
            "MM_sf_unc_MM_mc_stat",
            "MM_sf_unc_MM_isolation",
        ]

        self.syst_labels = [
            "MatrixMethod: correction",
            "MatrixMethod: iteration",
            "MatrixMethod: mc stat",
            "MatrixMethod: isolation",
        ]

        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path, SF_path, stat_path=stat_path, syst_paths=syst_path
        )

        # let's strip off the bins below 15 GeV
        # SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        # SF = makeEtaAbs(SF, x, y, 150.0)
        # stat = makeEtaAbs(stat, x, y, 150.0)
        # syst = makeEtaAbs(syst, x, y, 150.0)

        SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
        arr_out = np.column_stack((SF, stat, syst))

        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = y


@dataclass
class RadiativeZ(MethodSF):
    def read_data(self, removeNeg="yes", include_wp_names=False):

        SF_path = "RZ_sf_Base"
        stat_path = "RZ_sf_unc_stat"

        syst_path = [
            "RZ_sf_unc RZ_bkg1",
            "RZ_sf_unc RZ_bkg2",
        ]

        self.syst_labels = [
            "MatrixMethod: Background 1",
            "MatrixMethod: Background 2",
        ]

        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path, SF_path, stat_path=stat_path, syst_paths=syst_path
        )

        # let's strip off the bins below 15 GeV
        # SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        # SF = makeEtaAbs(SF, x, y, 150.0)
        # stat = makeEtaAbs(stat, x, y, 150.0)
        # syst = makeEtaAbs(syst, x, y, 150.0)

        SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
        arr_out = np.column_stack((SF, stat, syst))

        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = y


@dataclass
class PhotonComb(MethodSF):
    def read_data(self, removeNeg="yes", include_wp_names=False):

        SF_path = "FullSim_sf"
        stat_path = "stat_Combined"
        """
        if (Run == "Run2_r21"):

            syst_path = [
                "closure_Combined",
                "EE_conversion_Combined",
                "RZ_generator_Combined",
                "isolation_Combined",
                "fragmentation_Combined",
                "ff_Combined",
                "mc_stat_Combined",
                "MM_correction_Combined",
                "MM_interation_Combined",
                "RZ_background_Combined",
                "RZ_leakage_Combined",
            ]
            
            self.syst_labels = [
                "Combined: closure",
                "Combined: EE_conversion",
                "Combined: RZ_generator",
                "Combined: isolation",
                "Combined: fragmentation",
                "Combined: ff",
                "Combined: mc_stat",
                "Combined: MM_correction",
                "Combined: MM_interation",
                "Combined: RZ_background",
                "Combined: RZ_leakage",
            ]




        if (Run == "Run3_r22"):
        """
        syst_path = [
            "closure_Combined",
            "ff_Combined",
            "run2_run3_Combined",
            "MM_correction_Combined",
            "MM_iteration_Combined",
            "MM_mc_stat_Combined",
            "MM_isolation_Combined",
            "RZ_bkg1_Combined",
            "RZ_bkg2_Combined",
        ]

        self.syst_labels = [
            "Combined: closure",
            "Combined: ff",
            "Combined: run2_run3",
            "Combined: MM_correction",
            "Combined: MM_iteration",
            "Combined: MM_mc_stat",
            "Combined: MM_isolation",
            "Combined: RZ_bkg1",
            "Combined: RZ_bkg2",
        ]

        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path,
            SF_path,
            stat_path=stat_path,
            syst_paths=syst_path,
            dont_vary=True,
        )

        # let's strip off the bins below 15 GeV
        # SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        # SF = makeEtaAbs(SF, x, y, 150.0)
        # stat = makeEtaAbs(stat, x, y, 150.0)
        # syst = makeEtaAbs(syst, x, y, 150.0)

        # SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )

        arr_out = np.column_stack((SF, stat, syst))

        arr_out2 = arr_out
        adder = 0

        # remove the problematic crack region
        for i in range(0, len(arr_out)):
            if i % 5 == 2:
                print(arr_out[i])
                arr_out2 = np.delete(arr_out2, i - adder, axis=0)
                adder += 1

        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        print(type(y))
        new_y = y
        new_y = np.delete(new_y, 3, axis=0)
        print(new_y)

        """
        self.sf = arr_out[:, 0]      
        self.stat = arr_out[:, 1]    
        self.syst = arr_out[:, 2:]   
        self.x_bins = x              
        self.y_bins = y 
        """
        self.sf = arr_out2[:, 0]
        self.stat = arr_out2[:, 1]
        self.syst = arr_out2[:, 2:]
        self.x_bins = x
        self.y_bins = new_y


"""    

logger.info("Performing combination")

mmeth = MatrixMethod(data_path="/afs/cern.ch/work/r/rhulsken/private/PhotonId/PhotonEfficiencyCombination/Out_c.root")
mmeth.log("readin", "mmeth")
mmeth.log_syst("readin", "zrad")
zrad = RadiativeZ(data_path="/afs/cern.ch/work/r/rhulsken/private/PhotonId/PhotonEfficiencyCombination/Out_c.root")
zrad.log("readin", "mmeth")
zrad.log_syst("readin", "zrad")

mmeth.convertGeVtoMeV()
zrad.convertGeVtoMeV()

pTbins = np.array([15000.00])
etabins = np.array([0.00, 0.10, 0.80, 1.37, 1.52, 2.01, 2.37])


# Total combination

# we want the high zcomb to be decorrelated the same way when we go to decompose
zmasshi.syst, _ = decorrelateSyst(zmasshi, jpsi, nCorr=0) 
zmassRebin.syst, jpsi.syst = decorrelateSyst(zmassRebin, jpsi, nCorr=0)

zmassRebin.removeDummyRows()

# jpsi.log("afterlastdecorr", "jpsi_vloose")
# zmassRebin.log("afterlastdecorr", "zmassRebin_vloose")

zjpsicomb = combineHera(
    zmassRebin.stacked_array(),
    jpsi.stacked_array(last_nrows=7),
    zmassRebin.x_bins,
    zmassRebin.y_bins,
    working_point,
    year,
)



zmasshi.syst_labels = combineSysLabels(zmassRebin, jpsi)
zjpsicomb.syst_labels = combineSysLabels(zmassRebin, jpsi)
"""


### Load the info

naming = ["c"]

for name in naming:
    photComb = PhotonComb(
        data_path="/afs/cern.ch/work/r/rhulsken/public/For_Sully/Out_Run3_r22_"
        + name
        + ".root"
    )
    photComb.log("readin", "photComb_" + name)
    photComb.log_syst("readin", "photComb_" + name)

    studyEigenMapping(
        indirs=[photComb],
        years=[2018],
        plots_dir="eigenmap_" + name,
        stat_in_decomp=False,
    )

    writeRootFiles_decomp(
        indirs=[photComb],
        years=[2018],
        outfname=f"results/efficiencySF_offline_photon_" + name + ".root",
        eig_threshold=0.8,
        use_exact_eig_num=True,
        max_sig_eig=9,
    )
