from SFCombination import (
    MethodSF,
    ZisoData,
    ZisoStatData,
    ZmassData,
    ZmassStatData,
    JpsiData,
    config,
    test_config,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
)
import argparse
import numpy as np
import os

parser = argparse.ArgumentParser()
parser.add_argument("-test", action="store_true")
parser.add_argument("--stat_in_decomp", action="store_true")
args = parser.parse_args()

test = args.test
stat_in_decomp = args.stat_in_decomp
working_point = "TightLLH_d0z0"
years = [2018]
logger.info(f"Performing combination on {working_point} {years}")
data_config = config
if test:
    print("TEST")
    data_config = test_config

for year in years:

    # Ziso
    ziso = ZisoData(data_path=data_config.ziso_data_path[working_point][year])
    # ziso.log("readin", "ziso")
    ziso.log_syst("readin", "ziso")
    exit()
    # Ziso data that is binned to fit Jpsi because Heraverager was not doing a good job
    # rebinning the zmethods
    zisoPrebin = ZisoData(
        prebinned=True,
        data_path=data_config.zisoPrebin_data_path[working_point][year],
    )
    zisoPrebin.log_syst("readin", "zisoPrebin")
    # Rename to correlated stats or some shit like that
    zisoStat = ZisoStatData(data_path=data_config.ziso_data_path[working_point][year])
    zisoPrebinStat = ZisoStatData(
        prebinned=True,
        data_path=data_config.zisoPrebin_data_path[working_point][year],
    )

    for meas in [ziso, zisoPrebin, zisoStat, zisoPrebinStat]:
        meas.convertGeVtoMeV()

    ziso.decorrelateStat(zisoStat)
    zisoPrebin.decorrelateStat(zisoPrebinStat)
    zisoPrebin.log("decorrStat", "zisoPrebin")
    zisoPrebin.log_syst("decorrStat", "zisoPrebin")

    # Zmass

    zmass = ZmassData(data_path=data_config.zmass_data_path[working_point][year])
    zmass.log_syst("readin", "zmass")
    # Zmass data that is binned to fit Jpsi because Heraverager was not doing a good job
    # rebinning the zmethods
    zmassPrebin = ZmassData(
        prebinned=True,
        data_path=data_config.zmassPrebin_data_path[working_point][year],
    )
    zmassPrebin.log("readin", "zmassPrebin")
    zmassPrebin.log_syst("readin", "zmassPrebin")
    # Rename to correlated stats or some shit like that
    zmassStat = ZmassStatData(
        data_path=data_config.zmass_data_path[working_point][year]
    )
    zmassPrebinStat = ZmassStatData(
        prebinned=True,
        data_path=data_config.zmassPrebin_data_path[working_point][year],
    )

    for meas in [zmass, zmassPrebin, zmassStat, zmassPrebinStat]:
        meas.convertGeVtoMeV()

    zmass.decorrelateStat(zmassStat)
    zmassPrebin.decorrelateStat(zmassPrebinStat)
    zmassPrebin.log("decorrStat", "zmassPrebin")
    zmassPrebin.log_syst("decorrStat", "zmassPrebin")
    # Z combination
    ziso.syst, zmass.syst = decorrelateSyst(ziso, zmass)
    zisoPrebin.syst, zmassPrebin.syst = decorrelateSyst(
        zisoPrebin, zmassPrebin, nCorr=7
    )
    print(combineSysLabels(zmass, ziso))
    zisoPrebin.log_syst("decorrSyst", "zisoPrebin")
    zmassPrebin.log_syst("decorrSyst", "zmassPrebin")
    zisolo, zisohi = ziso.rebinlowpt()
    zmasslo, zmasshi = zmass.rebinlowpt()

    zcomblo = combinemethods_BLUE(zmasslo, zisolo)
    zcombhi = combinemethods_BLUE(zmasshi, zisohi)
    zcombPrebin = combinemethods_BLUE(zmassPrebin, zisoPrebin)
    zcombPrebin.log("aftercomb", "zcombPrebin")
    zcombPrebin.log_syst("aftercomb", "zcombPrebin")

    zcomblo.undecorrelateStat(nCorr=ziso.sf.shape[0])
    zcombhi.undecorrelateStat(nCorr=ziso.sf.shape[0])
    zcombPrebin.undecorrelateStat()
    zcombPrebin.log_syst("undecorr", "zcombPrebin")

    orig_zcombPrebin = MethodSF()
    orig_zcombPrebin.copy(zcombPrebin)

    # plot_scale_factors_at_pt(
    #     [zcombhi, zmasshi, zisohi],
    #     ["Z Combined", "Z Mass", "Z Iso"],
    #     name="zcombhi",
    # )

    # plot_scale_factors_at_pt(
    #     [zcomblo, zmasslo, zisolo],
    #     ["Z Combined", "Z Mass", "Z Iso"],
    #     name="zcomblo",
    # )

    # plot_scale_factors_at_pt(
    #     [zcombPrebin, zmassPrebin, zisoPrebin],
    #     ["Z Combined", "Z Mass", "Z Iso"],
    #     name="zcombPrebin",
    # )
    # Jpsi

    jpsi = JpsiData(data_path=data_config.jpsi_data_path[working_point][year])
    jpsi.convertGeVtoMeV()
    jpsi.log_syst("readin", "jpsi")

    # Total combination

    zcombPrebin.removeDummyRows()
    zcombPrebin.log("beforelastdecorr", "zcombPrebin")
    # we want the high zcomb to be decorrelated the same way when we go to decompose
    zcombhi.log("beforelastdecorr", "zcombhi")
    zcombhi.syst, _ = decorrelateSyst(zcombhi, jpsi, nCorr=0)
    zcombPrebin.syst, jpsi.syst = decorrelateSyst(zcombPrebin, jpsi, nCorr=0)
    zcombPrebin.removeDummyRows()
    jpsi.log("afterlastdecorr", "jpsi")
    zcombPrebin.log("afterlastdecorr", "zcombPrebin")
    zjpsicomb = combineHera(
        zcombPrebin.stacked_array(),
        jpsi.stacked_array(last_nrows=7),
        zcombPrebin.x_bins,
        zcombPrebin.y_bins,
        working_point,
        year,
    )
    zcombhi.syst_labels = combineSysLabels(zcombPrebin, jpsi)
    zjpsicomb.syst_labels = combineSysLabels(zcombPrebin, jpsi)
    zjpsicomb.convertGeVtoMeV()
    zjpsicomb.log("preprepending", "zjpsicomb")

    zjpsicomb.prepend(jpsi)
    zjpsicomb.log("final", "zjpsicomb")
    zjpsicomb.log_syst("final", "zjpsicomb")
    zcombhi.log("final", "zcombhi")
    zcombhi.log_syst("final", "zcombhi")
    zcombPrebin.addDummyRows(rows=len(jpsi.sf) - len(zcombPrebin.sf))
    zcombPrebin.copy_binning(jpsi)
    plot_scale_factors_at_pt(
        [zjpsicomb, jpsi, zcombPrebin],
        ["Z JPsi Combined", "Jpsi", "Z Combined"],
        name="total_comb",
    )
os.makedirs("results", exist_ok=True)

studyEigenMapping(
    indirs=[zcombhi, zjpsicomb],
    years=[2018, 2018],
    plots_dir="eigenmap",
    stat_in_decomp=False,
)
writeRootFiles_decomp(
    indirs=[zcombhi, zjpsicomb],
    years=[2018, 2018],
    outfname=f"results/efficiencySF.offline.{working_point}.fillwithstat.root",
    stat_in_decomp=True,
)

writeRootFiles_decomp(
    indirs=[zcombhi, zjpsicomb],
    years=[2018, 2018],
    outfname=f"results/efficiencySF.offline.{working_point}.full.root",
    stat_in_decomp=False,
)

# npreduction(indirs=[zcombhi, zjpsicomb], years=[2018, 2018])
