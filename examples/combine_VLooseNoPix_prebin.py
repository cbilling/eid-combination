from SFCombination import (
    MethodSF,
    JpsiData,
    ZmassData,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
    getArray_SF_stat_syst_refactored,
)
from dataclasses import dataclass
import numpy as np
import os


##### THIS METHOD DOES NOT HAVE THE PROPER STATISTICS #####
### STATISTICS NEED TO BE ADDED TO THE CORRECTIONS FILE ###
@dataclass
class CorrectionsData(MethodSF):

    def read_data(self, removeNeg="no"):

        mc_campaigns = {2015: "mc20a", 2016: "mc20a", 2017: "mc20d", 2018: "mc20e"}

        SF_path = f"h_SF_{mc_campaigns[self.year]}_{self.working_point}_{self.iso}"
        print(SF_path)
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(self.data_path, SF_path)

        y = np.array([round(i, 2) for i in y])

        for j in range(len(x)):
            if x[j] == 4:
                x[j] = 4.5
                print("Corrected")
        self.sf = SF
        self.stat = stat
        self.syst = syst
        self.x_bins = x
        self.y_bins = y


working_point = "VeryLooseNoPixLH"
years = [2015]  # , 2016, 2017, 2018]
logger.info(f"Performing combination on {working_point} {years}")
data_config = {
    "jpsi": {
        "VeryLooseNoPixLH": {
            2015: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/JPsi_taucut_07012025/tp-scalefactor-taucut-eff_taucut_R22_oldLH_15/scalefactor_VeryLooseNoPix_variations.root",
            2016: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/JPsi_taucut_07012025/tp-scalefactor-taucut-eff_taucut_R22_oldLH_16/scalefactor_VeryLooseNoPix_variations.root",
            2017: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/JPsi_taucut_07012025/tp-scalefactor-taucut-eff_taucut_R22_oldLH_17/scalefactor_VeryLooseNoPix_variations.root",
            2018: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/JPsi_taucut_07012025/tp-scalefactor-taucut-eff_taucut_R22_oldLH_17/scalefactor_VeryLooseNoPix_variations.root",
        }
    },
    "zmass": {
        "VeryLooseNoPixLH": {
            2015: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_updatedEnScale/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2015s/scalefactor_VeryLooseNoPix_variations.root",
            2016: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_updatedEnScale/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2016s/scalefactor_VeryLooseNoPix_variations.root",
            2017: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_updatedEnScale/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2017/scalefactor_VeryLooseNoPix_variations.root",
            2018: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_updatedEnScale/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2018/scalefactor_VeryLooseNoPix_variations.root",
        }
    },
    "zmassRebin": {
        "VeryLooseNoPixLH": {
            2015: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_JPsi_binning/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2015s/scalefactor_VeryLooseNoPix_variations.root",
            2016: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_JPsi_binning/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2016s/scalefactor_VeryLooseNoPix_variations.root",
            2017: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_JPsi_binning/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2017/scalefactor_VeryLooseNoPix_variations.root",
            2018: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_JPsi_binning/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2018/scalefactor_VeryLooseNoPix_variations.root",
        }
    },
    "corrections": {
        "VeryLooseNoPixLH": {
            2015: "/afs/cern.ch/user/a/akvam/public/VLNP_FastSim/output_sf.root",
            2016: "/afs/cern.ch/user/a/akvam/public/VLNP_FastSim/output_sf.root",
            2017: "/afs/cern.ch/user/a/akvam/public/VLNP_FastSim/output_sf.root",
            2018: "/afs/cern.ch/user/a/akvam/public/VLNP_FastSim/output_sf.root",
        }
    },
}


finalSF_towrite = []
finalSF_corrected_towrite = []
years_for_eachsf = []
for year in years:

    zmass = ZmassData(data_path=data_config["zmass"][working_point][year])
    zmass.log("readin", "zmass_vloose")
    zmass.log_syst("readin", "zmass_vloose")

    zmass.convertGeVtoMeV()

    zmasslo, zmasshi = zmass.rebinlowpt()
    zmasshi.log("readin", "zmasshi_vloose")
    zmasshi.log_syst("readin", "zmasshi_vloose")
    zmassRebin_start = ZmassData(
        data_path=data_config["zmassRebin"][working_point][year]
    )

    # this prebinned data actually has more ptbins than necessary, so we can just split it
    zmassRebin, _ = zmassRebin_start.rebinlowpt(pT_max=15.0)
    zmassRebin.log("readin", "rebin_zmass_vloose")
    zmassRebin.log_syst("readin", "rebin_zmass_vloose")
    zmassRebin.convertGeVtoMeV()
    # Jpsi

    jpsi = JpsiData(data_path=data_config["jpsi"][working_point][year])
    jpsi.log("readin", "jpsi_vloose")
    jpsi.log_syst("readin", "jpsi_vloose")
    jpsi.convertGeVtoMeV()

    # Total combination

    # we want the high zcomb to be decorrelated the same way when we go to decompose
    zmasshi, _ = decorrelateSyst(zmasshi, jpsi, nCorr=0)
    jpsi.log("testdecorr", "jpsi_vloose")
    jpsi.log_syst("testdecorr", "jpsi_vloose")

    zmassRebin, jpsi = decorrelateSyst(zmassRebin, jpsi, nCorr=0)

    jpsi.log("afterlastdecorr", "jpsi_vloose")
    zmassRebin.log("afterlastdecorr", "zmassRebin_vloose")
    jpsi.log_syst("afterlastdecorr", "jpsi_vloose")
    zmassRebin.log_syst("afterlastdecorr", "zmassRebin_vloose")

    zjpsicomb = combineHera(
        # we need only the first 7 bins corresponding to the overlap
        zmassRebin.stacked_array(),
        # The last 7 bins here correspond to overlap
        jpsi.stacked_array(last_nrows=7),
        # We only want to combine in the first pT bin while maintaining an array
        zmassRebin.x_bins,
        zmassRebin.y_bins,
        working_point,
        year,
    )

    zjpsicomb.syst_labels = combineSysLabels(zmassRebin, jpsi)
    jpsi.syst_labels = combineSysLabels(zmassRebin, jpsi)
    zmasshi.syst_labels = combineSysLabels(zmassRebin, jpsi)

    zjpsicomb.log("preprepending", "zjpsicomb_vloose")

    zjpsicomb.prepend(jpsi)

    zjpsicomb.log("aftercomb", "zjpsicomb_vloose")
    zjpsicomb.log_syst("aftercomb", "zjpsicomb_vloose")
    zmasshi.log("aftercomb", "zmasshi_vloose")
    zmasshi.log_syst("aftercomb", "zmasshi_vloose")

    # plot_scale_factors_at_pt(
    #    [zjpsicomb, jpsi, zmassRebin],
    #    ["Z JPsi Combined", "Jpsi", "Z Combined"],
    #    name="VLooseNoPix",
    # )

    # Read in mc-to-mc fast to full corrections
    corrections = CorrectionsData(
        data_path=data_config["corrections"][working_point][year],
        year=year,
        working_point=working_point,
    )
    corrections.convertGeVtoMeV()

    zmasshi_corrected = MethodSF()
    zmasshi_corrected.copy(zmasshi)
    zmasshi_corrected.addFastSim(corrections)

    corrections.rebinlowpt_BLUE(
        pTbins=np.array([4500, 7000, 10000, 15000]),
        etabins=np.array([0, 0.8, 1.37, 1.52, 2.01, 2.37, 2.47]),
    )
    zjpsicomb_corrected = MethodSF()
    zjpsicomb_corrected.copy(zjpsicomb)
    zjpsicomb_corrected.addFastSim(corrections)

    finalSF_towrite.append(zmasshi)
    finalSF_towrite.append(zjpsicomb)
    finalSF_corrected_towrite.append(zmasshi_corrected)
    finalSF_corrected_towrite.append(zjpsicomb_corrected)
    years_for_eachsf.append(year)
    years_for_eachsf.append(year)

os.makedirs("results", exist_ok=True)

studyEigenMapping(
    indirs=finalSF_towrite,
    years=years_for_eachsf,
    plots_dir="eigenmap",
    stat_in_decomp=False,
)

outfile = f"results/efficiencySF.offline.{working_point}.VLooseNoPix_Prebin_af2.root"
writeRootFiles_decomp(
    indirs=finalSF_towrite,
    years=years_for_eachsf,
    outfname=outfile,
    stat_in_decomp=False,
)

# Now let's write the AtlFast
writeRootFiles_decomp(
    indirs=finalSF_corrected_towrite,
    years=years_for_eachsf,
    outfname=outfile,
    stat_in_decomp=False,
    sflabel="AtlFast",
)
# npreduction(indirs=[zcombhi, zjpsicomb], years=[2018, 2018])
