from SFCombination import ZrecoData, writeRoot
import argparse
import numpy as np
import os

# Specify the years you want to work with
years = [20152016, 2017, 2018]

# Define the path where the variations live
zreco_data_paths = {
    20152016: "/afs/cern.ch/user/c/cbilling/zreco_data/tp-scalefactor-zreco1516-release22/scalefactor_reco_variations.root",
    2017: "/afs/cern.ch/user/c/cbilling/zreco_data/tp-scalefactor-zreco17-release22/scalefactor_reco_variations.root",
    2018: "/afs/cern.ch/user/c/cbilling/zreco_data/tp-scalefactor-zreco18-release22/scalefactor_reco_variations.root",
}

# Read in the data from each year
zrecos = []
for year in years:
    zreco = ZrecoData(data_path=zreco_data_paths[year])
    zrecos.append(zreco)

# write to an eGamma root file, the decomposition is also performed in this function
writeRoot(years, zrecos, "results/zreco.root")
