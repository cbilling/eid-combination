from SFCombination import combine_sfs_run2_rel22
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-test", action="store_true")
args = parser.parse_args()

combine_sfs_run2_rel22(
    years=[2018],
    working_point="TightLLH_d0z0",
    test=args.test,
    np_red_study=True,
    write_eg=False,
)
