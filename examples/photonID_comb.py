from SFCombination import (
    MethodSF,
    ZisoData,
    ZisoStatData,
    ZmassData,
    ZmassStatData,
    JpsiData,
    config,
    test_config,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
    getArray_SF_stat_syst_refactored,
)
import argparse
import numpy as np
import os
from dataclasses import dataclass


@dataclass
class FinalPhotonData(MethodSF):

    def read_data(self, removeNeg="yes", include_wp_names=False):

        wp = "_" + self.working_point
        if not include_wp_names:
            wp = ""
        SF_path = "FullSim_sf" + wp
        stat_path = "stat_Combined" + wp

        syst_paths = [
            "ff_Combined" + wp,
        ]

        self.syst_labels = [
            "Fudge",
        ]
        # now we fill the histograms into arrays
        SF, stat, syst, x, y = getArray_SF_stat_syst_refactored(
            self.data_path,
            SF_path,
            stat_path=stat_path,
            syst_paths=syst_paths,
            dont_vary=True,
        )
        # let's strip off the bins below 15 GeV
        # SF, stat, syst, x, y = stripFromArrayX(15.0, SF, stat, syst, x, y)

        # Elias has zeros at high Et where we do the measurement in abs(eta)
        # SF = makeEtaAbs(SF, x, y, 150.0)
        # stat = makeEtaAbs(stat, x, y, 150.0)
        # syst = makeEtaAbs(syst, x, y, 150.0)

        # SF, stat, syst = check_SF_stat_syst(SF, stat, syst)

        # and we're done! This stuff we can now store
        # writeArray_SF_stat_syst( outdir, SF, stat, syst, x, y )
        arr_out = np.column_stack((SF, stat, syst))
        print("removeNeg: ", removeNeg)
        if removeNeg != "no":
            posy = []
            for b in y:
                if b >= 0:
                    posy.append(b)
            y = posy
            arr_out = arr_out[len(posy) :, :]

        self.sf = arr_out[:, 0]
        self.stat = arr_out[:, 1]
        self.syst = arr_out[:, 2:]
        self.x_bins = x
        self.y_bins = y


finalMeth = FinalPhotonData(
    data_path="/afs/cern.ch/user/c/cbilling/tNp/eid-combination/Out_Run3_r22_c.root"
)
finalMeth.log_syst("readin", "photon")

# studyEigenMapping(
#     indirs=[finalMeth],
#     years=[2018],
#     plots_dir="eigenmap",
#     stat_in_decomp=False,
# )

# writeRootFiles_decomp(
#     indirs=[finalMeth],
#     years=[2018],
#     outfname=f"results/mm.root",
#     stat_in_decomp=False,
# )
