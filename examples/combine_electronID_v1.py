from SFCombination import (
    MethodSF,
    ZisoData,
    ZisoStatData,
    ZmassData,
    ZmassStatData,
    JpsiData,
    config,
    test_config,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
)
import argparse
import numpy as np
import os

ziso_data_path = {
    "TightLLH_d0z0": {
        2015: "/eos/user/c/cbilling/eid-comb-inputs/ziso/inclusive/scalefactors_ziso_for_comb_2015.root",
        2016: "/eos/user/c/cbilling/eid-comb-inputs/ziso/inclusive/scalefactors_ziso_for_comb_2016.root",
        2017: "/eos/user/c/cbilling/eid-comb-inputs/ziso/inclusive/scalefactors_ziso_for_comb_2017.root",
        2018: "/eos/user/c/cbilling/eid-comb-inputs/ziso/inclusive/scalefactors_ziso_for_comb_2018.root",
    }
}

zisoPrebin_data_path = {
    "TightLLH_d0z0": {
        2015: "/eos/user/c/cbilling/eid-comb-inputs/ziso/lowPtExtraBin/scalefactors_ziso_for_comb_2015.root",
        2016: "/eos/user/c/cbilling/eid-comb-inputs/ziso/lowPtExtraBin/scalefactors_ziso_for_comb_2016.root",
        2017: "/eos/user/c/cbilling/eid-comb-inputs/ziso/lowPtExtraBin/scalefactors_ziso_for_comb_2017.root",
        2018: "/eos/user/c/cbilling/eid-comb-inputs/ziso/lowPtExtraBin/scalefactors_ziso_for_comb_2018.root",
    }
}

zmass_data_path = {
    "TightLLH_d0z0": {
        2015: "/eos/user/c/cbilling/eid-comb-inputs/zmass/inclusive/scalefactors_zmass_for_comb_2015.root",
        2016: "/eos/user/c/cbilling/eid-comb-inputs/zmass/inclusive/scalefactors_zmass_for_comb_2016.root",
        2017: "/eos/user/c/cbilling/eid-comb-inputs/zmass/inclusive/scalefactors_zmass_for_comb_2017.root",
        2018: "/eos/user/c/cbilling/eid-comb-inputs/zmass/inclusive/scalefactors_zmass_for_comb_2018.root",
    }
}

zmassPrebin_data_path = {
    "TightLLH_d0z0": {
        2015: "/eos/user/c/cbilling/eid-comb-inputs/zmass/lowPtExtraBin/scalefactors_zmass_for_comb_2015.root",
        2016: "/eos/user/c/cbilling/eid-comb-inputs/zmass/lowPtExtraBin/scalefactors_zmass_for_comb_2016.root",
        2017: "/eos/user/c/cbilling/eid-comb-inputs/zmass/lowPtExtraBin/scalefactors_zmass_for_comb_2017.root",
        2018: "/eos/user/c/cbilling/eid-comb-inputs/zmass/lowPtExtraBin/scalefactors_zmass_for_comb_2018.root",
    }
}

jpsi_data_path = {
    "TightLLH_d0z0": {
        2015: "/eos/user/c/cbilling/eid-comb-inputs/jpsi/lowPtExtraBin/scalefactors_jpsi_for_comb_2015.root",
        2016: "/eos/user/c/cbilling/eid-comb-inputs/jpsi/lowPtExtraBin/scalefactors_jpsi_for_comb_2016.root",
        2017: "/eos/user/c/cbilling/eid-comb-inputs/jpsi/lowPtExtraBin/scalefactors_jpsi_for_comb_2017.root",
        2018: "/eos/user/c/cbilling/eid-comb-inputs/jpsi/lowPtExtraBin/scalefactors_jpsi_for_comb_2018.root",
    }
}

working_point = "TightLLH_d0z0"
years = [2018]
logger.info(f"Performing combination on {working_point} {years}")

methods = []
years_for_writing = []
for year in years:

    # Ziso
    ziso = ZisoData(data_path=ziso_data_path[working_point][year])
    # ziso.log("readin", "ziso")
    ziso.log_syst("readin", "ziso")
    # Ziso data that is binned to fit Jpsi because Heraverager was not doing a good job
    # rebinning the zmethods
    zisoPrebin = ZisoData(
        prebinned=True,
        data_path=zisoPrebin_data_path[working_point][year],
    )
    zisoPrebin.log_syst("readin", "zisoPrebin")
    # Rename to correlated stats or some shit like that
    zisoStat = ZisoStatData(data_path=ziso_data_path[working_point][year])
    zisoPrebinStat = ZisoStatData(
        prebinned=True,
        data_path=zisoPrebin_data_path[working_point][year],
    )

    for meas in [ziso, zisoPrebin, zisoStat, zisoPrebinStat]:
        meas.convertGeVtoMeV()

    ziso.decorrelateStat(zisoStat)
    zisoPrebin.decorrelateStat(zisoPrebinStat)
    zisoPrebin.log("decorrStat", "zisoPrebin")
    zisoPrebin.log_syst("decorrStat", "zisoPrebin")

    # Zmass
    print("reading in ZMASS")
    zmass = ZmassData(
        data_path=zmass_data_path[working_point][year], include_wp_names=True
    )
    zmass.log_syst("readin", "zmass")
    # Zmass data that is binned to fit Jpsi because Heraverager was not doing a good job
    # rebinning the zmethods
    zmassPrebin = ZmassData(
        prebinned=True,
        data_path=zmassPrebin_data_path[working_point][year],
        include_wp_names=True,
    )
    zmassPrebin.log("readin", "zmassPrebin")
    zmassPrebin.log_syst("readin", "zmassPrebin")
    # Rename to correlated stats or some shit like that
    zmassStat = ZmassStatData(data_path=zmass_data_path[working_point][year])
    zmassPrebinStat = ZmassStatData(
        prebinned=True,
        data_path=zmassPrebin_data_path[working_point][year],
    )

    for meas in [zmass, zmassPrebin, zmassStat, zmassPrebinStat]:
        meas.convertGeVtoMeV()

    zmass.decorrelateStat(zmassStat)
    zmassPrebin.decorrelateStat(zmassPrebinStat)
    zmassPrebin.log("decorrStat", "zmassPrebin")
    zmassPrebin.log_syst("decorrStat", "zmassPrebin")
    # Z combination
    ziso, zmass = decorrelateSyst(ziso, zmass)
    zisoPrebin, zmassPrebin = decorrelateSyst(zisoPrebin, zmassPrebin, nCorr=7)
    zisoPrebin.log_syst("decorrSyst", "zisoPrebin")
    zmassPrebin.log_syst("decorrSyst", "zmassPrebin")
    zisolo, zisohi = ziso.rebinlowpt()
    zmasslo, zmasshi = zmass.rebinlowpt()

    zcomblo = combinemethods_BLUE(zmasslo, zisolo)
    print(1)
    zcombhi = combinemethods_BLUE(zmasshi, zisohi)
    print(2)
    zcombPrebin = combinemethods_BLUE(zmassPrebin, zisoPrebin)
    print(3)
    zcombPrebin.log("aftercomb", "zcombPrebin")
    zcombPrebin.log_syst("aftercomb", "zcombPrebin")

    zcomblo.undecorrelateStat(nCorr=ziso.sf.shape[0])
    zcombhi.undecorrelateStat(nCorr=ziso.sf.shape[0])
    zcombPrebin.undecorrelateStat()
    zcombPrebin.log_syst("undecorr", "zcombPrebin")

    orig_zcombPrebin = MethodSF()
    orig_zcombPrebin.copy(zcombPrebin)

    plot_scale_factors_at_pt(
        [zcombhi, zmasshi, zisohi],
        ["Z Combined", "Z Mass", "Z Iso"],
        name="zcombhi",
    )

    plot_scale_factors_at_pt(
        [zcomblo, zmasslo, zisolo],
        ["Z Combined", "Z Mass", "Z Iso"],
        name="zcomblo",
    )

    plot_scale_factors_at_pt(
        [zcombPrebin, zmassPrebin, zisoPrebin],
        ["Z Combined", "Z Mass", "Z Iso"],
        name="zcombPrebin",
    )
    # Jpsi

    jpsi = JpsiData(
        data_path=jpsi_data_path[working_point][year], include_wp_names=True
    )
    jpsi.convertGeVtoMeV()
    jpsi.log_syst("readin", "jpsi")

    # Total combination

    zcombPrebin.removeDummyRows()
    zcombPrebin.log("beforelastdecorr", "zcombPrebin")
    # we want the high zcomb to be decorrelated the same way when we go to decompose
    zcombhi.log("beforelastdecorr", "zcombhi")
    zcombhi, _ = decorrelateSyst(zcombhi, jpsi, nCorr=0)
    zcombPrebin, jpsi = decorrelateSyst(zcombPrebin, jpsi, nCorr=0)
    zcombPrebin.removeDummyRows()
    jpsi.log("afterlastdecorr", "jpsi")
    zcombPrebin.log("afterlastdecorr", "zcombPrebin")
    zjpsicomb = combineHera(
        zcombPrebin.stacked_array(),
        jpsi.stacked_array(last_nrows=7),
        np.array(zcombPrebin.x_bins),
        np.array(zcombPrebin.y_bins),
        working_point,
        year,
    )
    zcombhi.syst_labels = combineSysLabels(zcombPrebin, jpsi)
    zjpsicomb.syst_labels = combineSysLabels(zcombPrebin, jpsi)
    zjpsicomb.convertGeVtoMeV()
    zjpsicomb.log("preprepending", "zjpsicomb")

    zjpsicomb.prepend(jpsi)
    zjpsicomb.log("final", "zjpsicomb")
    zjpsicomb.log_syst("final", "zjpsicomb")
    zcombhi.log("final", "zcombhi")
    zcombhi.log_syst("final", "zcombhi")
    zcombPrebin.addDummyRows(rows=len(jpsi.sf) - len(zcombPrebin.sf))
    zcombPrebin.copy_binning(jpsi)
    plot_scale_factors_at_pt(
        [zjpsicomb, jpsi, zcombPrebin],
        ["Z JPsi Combined", "Jpsi", "Z Combined"],
        name="total_comb",
    )
    methods.append(zcombhi)
    methods.append(zjpsicomb)
    years_for_writing.append(year)
    years_for_writing.append(year)
os.makedirs("results", exist_ok=True)

studyEigenMapping(
    indirs=[zcombhi, zjpsicomb],
    years=[2018, 2018],
    plots_dir="eigenmap",
    stat_in_decomp=False,
)

writeRootFiles_decomp(
    indirs=methods,
    years=years_for_writing,
    outfname=f"results/efficiencySF.offline.{working_point}.full_v1.root",
    stat_in_decomp=False,
)

# npreduction(indirs=[zcombhi, zjpsicomb], years=[2018, 2018])
