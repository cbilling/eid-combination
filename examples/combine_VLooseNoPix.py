from SFCombination import (
    MethodSF,
    JpsiData,
    ZmassData,
    decorrelateSyst,
    combineHera,
    combinemethods_BLUE,
    combineSysLabels,
    writeRootFiles_decomp,
    npreduction,
    plot_scale_factors_at_pt,
    logger,
    studyEigenMapping,
)
from dataclasses import dataclass
import numpy as np
import os


working_point = "VeryLooseNoPix"
years = [2017]
logger.info(f"Performing combination on {working_point} {years}")
data_config = {
    "jpsi": {
        "VeryLooseNoPix": {
            2017: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/JPsi_taucut_17122024/tp-scalefactor-taucut-eff_taucut_R22_oldLH_17/scalefactor_VeryLooseNoPix_variations.root"
        }
    },
    "zmass": {
        "VeryLooseNoPix": {
            2017: "/eos/user/a/alguida/egamma/2024-08-run2_rel22_vlnp/veryloose_sf/zmass_updatedEnScale/tp-scalefactor-zmass-efficiency_r22_oldLH_final_2017/scalefactor_VeryLooseNoPix_variations.root"
        }
    },
}


for year in years:

    zmass = ZmassData(data_path=data_config["zmass"][working_point][year])
    zmass.log("readin", "zmass_vloose")
    zmass.log_syst("readin", "zmass_vloose")

    zmass.convertGeVtoMeV()

    pTbins = np.array([15000.00])
    etabins = np.array([0.00, 0.10, 0.80, 1.37, 1.52, 2.01, 2.37])

    zmassRebin, zmasslo, zmasshi = zmass.rebinlowpt_BLUE(pTbins=pTbins, etabins=etabins)
    # zmassRebin.log("rebin", "zmassRebin")
    # zmassRebin.log_syst("rebin", "zmassRebin")

    # plot_scale_factors_at_pt(
    #     [zcombhi, zmasshi, zisohi],
    #     ["Z Combined", "Z Mass", "Z Iso"],
    #     name="zcombhi",
    # )

    # Jpsi

    jpsi = JpsiData(data_path=data_config["jpsi"][working_point][year])
    jpsi.convertGeVtoMeV()
    jpsi.log("readin", "jpsi_vloose")
    jpsi.log_syst("readin", "jpsi_vloose")

    # Total combination

    # we want the high zcomb to be decorrelated the same way when we go to decompose
    zmasshi, _ = decorrelateSyst(zmasshi, jpsi, nCorr=0)
    zmassRebin, jpsi = decorrelateSyst(zmassRebin, jpsi, nCorr=0)

    jpsi.log("afterlastdecorr", "jpsi_vloose")
    zmassRebin.log("afterlastdecorr", "zmassRebin_vloose")
    jpsi.log_syst("afterlastdecorr", "jpsi_vloose")
    zmassRebin.log_syst("afterlastdecorr", "zmassRebin_vloose")

    zjpsicomb = combineHera(
        # we need only the first 7 bins corresponding to the overlap
        zmassRebin.stacked_array()[:7],
        # The last 7 bins here correspond to overlap
        jpsi.stacked_array(last_nrows=7),
        # We only want to combine in the first pT bin
        zmassRebin.x_bins[0],
        zmassRebin.y_bins,
        working_point,
        year,
    )

    zjpsicomb.syst_labels = combineSysLabels(zmassRebin, jpsi)

    zjpsicomb.convertGeVtoMeV()
    zjpsicomb.log("preprepending", "zjpsicomb_vloose")

    zjpsicomb.prepend(jpsi)

    zjpsicomb.log("final", "zjpsicomb_vloose")
    zjpsicomb.log_syst("final", "zjpsicomb_vloose")
    zmasshi.log("final", "zmasshi_vloose")
    zmasshi.log_syst("final", "zmasshi_vloose")

    plot_scale_factors_at_pt(
        [zjpsicomb, jpsi, zmassRebin],
        ["Z JPsi Combined", "Jpsi", "Z Combined"],
        name="VLooseNoPix",
    )
os.makedirs("results", exist_ok=True)

studyEigenMapping(
    indirs=[zmasshi, zjpsicomb],
    years=[2018, 2018],
    plots_dir="eigenmap",
    stat_in_decomp=False,
)

writeRootFiles_decomp(
    indirs=[zmasshi, zjpsicomb],
    years=[2018, 2018],
    outfname=f"results/efficiencySF.offline.{working_point}.VLooseNoPix.root",
    stat_in_decomp=False,
)

# npreduction(indirs=[zcombhi, zjpsicomb], years=[2018, 2018])
