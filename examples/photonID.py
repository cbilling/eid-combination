from SFCombination import writeRoot, MethodSF
from dataclasses import dataclass
import argparse
import numpy as np
import os
import ROOT
import re

# Specify the years you want to work with
years = [2018]

# Define the path where the variations live
zreco_data_paths = {
    2018: "/afs/cern.ch/user/c/cbilling/tNp/eid-combination/data/combined_results_c.root",
}


@dataclass
class ZrecoData(MethodSF):

    def read_data(self, pTmin=15000.0, pTmax=99999999.0, removeNeg="no"):
        infile = self.data_path
        indir = ""
        x_unit_conversion = 1000
        match = ".+"
        print(match)
        # now we fill the histograms into arrays
        self.SF, self.stat, self.syst, self.x, self.y = self.getArray_SF_stat_syst(
            infile, indir, match, pTmin, pTmax, x_unit_conversion=x_unit_conversion
        )

    def getArray_SF_stat_syst(
        self, filename, path, match, pTmin=0.0, pTmax=99999999.0, x_unit_conversion=1
    ):
        # ------------------------------------------------------------------------------
        # this is the most important function for us
        #
        # it will return 5 np.arrays:
        # - the central values
        # - the stat uncertainty
        # - an array of systematic uncertainties, with columns of systematic sources
        # - the x-bins
        # - the y-bins
        #
        # it takes as input:
        # - a root file name
        # - the path in the root file
        # - a string and all histograms matching the name will be considered to get the mean and rms
        # ------------------------------------------------------------------------------

        # here is what we're returning
        SF = np.array([])
        stat = np.array([])
        syst = np.array([])
        xbins = np.array([])
        ybins = np.array([])

        infile = ROOT.TFile.Open(filename)
        if not infile.IsOpen():
            print(" in rootToArray: problem opening file ", filename)
            return SF, stat, syst, xbins, ybins

        listOfCentralValues = []
        listOfStatErrors = []

        directory = infile
        if len(path):
            directory = infile.Get(path)

        for hist in directory.GetListOfKeys():
            hname = hist.GetName()
            reg = re.compile(match)

            if not bool(reg.match(hname)):
                #           print(' skipping in hname ', hname , ' when looking for ', match)
                continue

            if ("CentralValue" in hname) or ("FullSim_sf" in hname):
                print(" adding hist central value ", hname)
                listOfCentralValues.append(directory.Get(hname))
            elif "StatError" in hname:
                print(" adding hist stat error ", hname)
                listOfStatErrors.append(directory.Get(hname))

        if not len(listOfCentralValues) or len(listOfCentralValues) != len(
            listOfStatErrors
        ):
            listOfStatErrors.append(directory.Get("FullSim_sf"))
            print(
                " in rootToArray: found no or inconsistent number of histograms ",
                len(listOfCentralValues),
                " ",
                len(listOfStatErrors),
            )
        else:
            print(" added ", len(listOfCentralValues), " histograms ")

        # get the binning from the first one
        xbins, ybins = self.getArray_binning(listOfCentralValues[0])
        print(xbins)
        xbins = xbins * x_unit_conversion
        print(xbins)
        # we need to cut some of the variations that are invalid
        cutoff_SF = 1.2
        cutoff_stat = 0.5
        cutoff_SF = 99999.0
        cutoff_stat = 1.5

        # now getting central values and stuff
        allSFs = []  # np.zeros( (xbins.size*ybins.size,0) )
        allStat = []  # np.zeros( (xbins.size*ybins.size,0) )
        for x in range(0, xbins.size):
            for y in range(0, ybins.size):
                var_val = []
                var_err = []
                histo = 0
                for cv in range(0, len(listOfCentralValues)):
                    val = listOfCentralValues[cv].GetBinContent(x + 1, y + 1)
                    err = listOfStatErrors[cv].GetBinContent(x + 1, y + 1)
                    var_val.append(val)
                    var_err.append(err)

                allSFs.append(np.array(var_val))
                allStat.append(np.array(var_err))

        SF = np.zeros(len(allSFs))
        stat = np.zeros(len(allStat))
        cov = np.zeros((len(allSFs), len(allSFs)))
        for i in range(0, len(allSFs)):
            mean = 0.0
            mean_stat = 0.0
            counter = 0.0
            for var in range(0, len(allSFs[i])):
                val = allSFs[i][var]
                err = allStat[i][var]

                if (
                    val > 20.0 and val < 100.0
                ):  # it's Andrey's damn Jpsi efficiency that is in percent
                    val = val / 100.0
                    err = err / 100.0

                if (
                    val > 0.0 and abs(val - 1) < cutoff_SF and err < cutoff_stat
                ):  # skip a bunch of SF variations
                    mean += val
                    mean_stat += err
                    counter += 1
                #                if i==185 :
                #                    print ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' adding mean ', val, ' stat ', err
                #                    print ' for pT ', xbins[int(i/len(ybins))], ' eta ', ('%1.2f' % ybins[i%len(ybins)]), ' mean ', mean, ' stat ', mean_stat, ' counter ', counter
                elif mean != 0:
                    print(
                        " skipping ",
                        val,
                        " +- ",
                        err,
                        " in bins ",
                        xbins[int(i / len(ybins))],
                        " / ",
                        ("%1.2f" % ybins[i % len(ybins)]),
                    )
            if counter > 0.0:
                SF[i] = mean / counter
                stat[i] = mean_stat / counter
        for i in range(0, len(allSFs)):
            for j in range(0, len(allSFs)):
                corr = 0.0
                counter = 0.0
                for var in range(0, len(allSFs[i])):
                    vali = allSFs[i][var]
                    valj = allSFs[j][var]
                    erri = allStat[i][var]
                    errj = allStat[j][var]
                    if (
                        vali > 0.0
                        and valj > 0.0
                        and abs(vali - 1) < cutoff_SF
                        and erri < cutoff_stat
                        and abs(valj - 1) < cutoff_SF
                        and errj < cutoff_stat
                    ):  # skip a bunch of SF variations
                        corr += (vali - SF[i]) * (valj - SF[j])
                        counter += 1.0
                #                elif vali != 0 and valj != 0 and erri<99999999999. and errj<99999999999. :
                #                    print ' skipping 2 ', vali, ' +- ', erri, '  ', valj, ' +- ', errj, ' in bins ', xbins[int(i/len(ybins))], ' / ', ('%1.2f' % ybins[i%len(ybins)]), ' in bins ', xbins[int(j/len(ybins))], ' / ', ('%1.2f' % ybins[j%len(ybins)])
                if counter > 0.0:
                    cov[i, j] = corr / counter
        #            else :
        #                print ' problem counting cov in bins ' ,i, ' ',j, ' ', counter

        SF, stat, cov, xbins, ybins = self.stripFromArrayX(
            pTmin, SF, stat, cov, xbins, ybins
        )
        SF, stat, cov, xbins, ybins = self.stripFromArrayX_max(
            pTmax, SF, stat, cov, xbins, ybins
        )

        # --------------------------------------
        # let's run an eigenvalue decomposition
        # --------------------------------------
        val_eig, vec_eig = np.linalg.eigh(cov)

        # remove complex entries, in general all this can be complex but for positive definite matrix is real
        val_eig = np.flipud(val_eig)
        vec_eig = np.fliplr(vec_eig)
        for iv in range(0, len(val_eig)):  # if there are neg eigenvalues, set them to 0
            if val_eig[iv] < 0.0:
                val_eig[iv] = 0

        eigsum = np.sum(val_eig)
        if eigsum == 0:
            print(
                " all eigenvalues are zero so the syst uncertainty must be zero, too "
            )
            return SF, stat, np.zeros(stat.shape), xbins, ybins

        # eigenvalues are already sorted by size, let's find the largest 99%
        counteig = 0
        currentsum = 0
        for eig in val_eig:

            currentsum += eig
            counteig += 1
            if currentsum / eigsum > 0.99:
                break

        print(" number of eig ", counteig, val_eig[:counteig])
        # the gamma matrix contains the sqrt( eigenvalue ) * eigenvectors
        # --> these are the uncorrelated sources of uncertainty
        gamma = vec_eig[:, :counteig].dot(np.diag(np.sqrt(val_eig[:counteig])))
        rest = vec_eig[:, counteig:].dot(np.diag(np.sqrt(val_eig[counteig:])))
        rest = np.sqrt(np.sum(rest**2, axis=1))

        # copy the syst and ignore for now the uncorr
        syst = gamma
        # add the uncorr to stats
        stat = np.sqrt(np.sum(np.column_stack((stat**2, rest**2)), axis=1))

        infile.Close()
        return SF, stat, syst, xbins, ybins

    def getArray_binning(self, histogram):

        # returns arrays with bin boundaries in x and y, excluding the upper edge of the last bin
        if not histogram:
            print(" in rootToArray:  no histogram ", histogram)
            return 0

        nxbins = histogram.GetXaxis().GetNbins()
        nybins = histogram.GetYaxis().GetNbins()

        xbins = np.array(
            [histogram.GetXaxis().GetBinLowEdge(x + 1) for x in range(0, nxbins)]
        )  # I don't want the upper edge for now
        ybins = np.array(
            [histogram.GetYaxis().GetBinLowEdge(y + 1) for y in range(0, nybins)]
        )

        return xbins, ybins

    def stripFromArrayX(self, threshold, SF, stat, cov, xbins, ybins):

        # not all our methods cover the full pT range, so let's strip some of the zeros
        bin = 0
        for x in xbins:
            if x < threshold:
                bin = bin + 1

        return (
            SF[(bin * ybins.size) :],
            stat[(bin * ybins.size) :],
            cov[(bin * ybins.size) :, (bin * ybins.size) :],
            xbins[bin:],
            ybins,
        )

    def stripFromArrayX_max(self, threshold, SF, stat, cov, xbins, ybins):

        # not all our methods cover the full pT range, so let's strip some of the zeros
        bin = 0
        for x in xbins:
            if x < threshold:
                bin = bin + 1

        return (
            SF[: (bin * ybins.size)],
            stat[: (bin * ybins.size)],
            cov[: (bin * ybins.size), : (bin * ybins.size)],
            xbins[:bin],
            ybins,
        )


# Read in the data from each year
zrecos = []
for year in years:
    zreco = ZrecoData(data_path=zreco_data_paths[year])
    zrecos.append(zreco)

# write to an eGamma root file, the decomposition is also performed in this function
writeRoot(years, zrecos, "results/zreco.root")
